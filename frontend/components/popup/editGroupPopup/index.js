import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import axios from 'axios';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';

export function EditGroupPopup(props) {
  const [groupName, setGroupName] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const group_data = {
      name: groupName,
    }
    try {
      const response = await fetch(`http://localhost:1337/api/guest-groups/${props.groupId}`, {
        method: 'PUT',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: group_data })

      })
      props.mutate()
    } catch (error) {
      console.log(error)
    }
    props.closePopup()
  };
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h4>Edit the name of the group</h4>
            <section>
              <InputForm type='text' defaultValue={props.groupname} onChange={(e) => { setGroupName(e.target.value) }} />
            </section>

            <div className={styles.submitClose}>
              <DashboardSolidButton type="submit">Save</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
