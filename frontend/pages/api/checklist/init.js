import axios from 'axios';
import { data } from './../checklist/checklist_init.js'


export default async (req, res) => {

  data.forEach(element => {
    console.log("THE DATA title  " + element.title)
    const wedding = new Date(req.body.period);
    var year = wedding.getFullYear();
    var month = wedding.getMonth();
    var day = wedding.getDate();

    axios.post(`http://localhost:1337/api/checklists`, {
      data:
      {
        user_id: req.body.id,
        title: element.title,
        tag: element.tag,
        period: new Date(year + element.year, month + element.month, day + element.day)
      }
    }).then(response => {
      console.log('init checklist', response.data)

      res.status(200).end();
    })
      .catch(function (err) {
        if (err.response) {
          console.log(err);
          console.log(err.response.data.error.name);
          res.status(400).send(err);
        }
      });
  })
}