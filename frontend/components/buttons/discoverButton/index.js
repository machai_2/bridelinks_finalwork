import styles from './style.module.scss';

export function DiscoverButton() {
  return <button className={styles.discover_btn}>Discover</button>;
}
