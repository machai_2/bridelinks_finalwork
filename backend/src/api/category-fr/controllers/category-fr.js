'use strict';

/**
 *  category-fr controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::category-fr.category-fr');
