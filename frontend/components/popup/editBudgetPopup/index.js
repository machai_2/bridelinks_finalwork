import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { signOut, useSession } from 'next-auth/react';
import { useRouter } from 'next/router'

export function EditBudgetPopup(props) {
  const [budget, setBudget] = useState("");
  const router = useRouter()
  const { data: session } = useSession();

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log('the budgeet here ' + budget)
    const user_data = {
      name: budget,
    }
    try {
      const response = await fetch(`http://localhost:1337/api/weddings/${props.id}`, {
        method: 'PUT',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: user_data })

      })
      console.log('the respnse her e  ' + response)
      props.mutate()
    } catch (error) {
      console.log(error)
    }
    props.closePopup()
  };
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h3>Set your maximum budget</h3>
            <section>
              <InputForm type='text' placeholder="maximum budget" defaultValue={`€ ${props.value}`} onChange={(e) => { setBudget(e.target.value) }} />
              <div className={styles.submitClose}>
                <DashboardSolidButton type="submit">Save</DashboardSolidButton>
              </div>
            </section>
          </form>
        </div>
      </div>
    </div>

  );
}
