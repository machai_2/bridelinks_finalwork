import axios from 'axios';
const sendtwilio = require("../sendtwilio");



export default async (req, res) => {
  console.log('in method sms  ' + JSON.stringify(req.body))
  try {
    let email = {
      txt: req.body.txt,
      number: req.body.number,
    };
    sendtwilio(email)
    res.status(200).end();
  } catch (err) {
    console.error(err);
    res.status(400).send(err);

  }
};