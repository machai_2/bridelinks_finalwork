import styles from './style.module.scss';

export function DashboardOutlineButton(props) {
  return <button className={styles.dashboard_button} onClick={props.onClick}>{props.children}</button>;
}
