import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import React from 'react';
import { InputStyle } from "./../inputStyle"
import styles from './style.module.scss';

export function InputSearchVendors(props) {
  const classes = InputStyle();
  return (
    <div className={styles.container}>
      <Autocomplete
        options={props.list}
        disableListWrap
        autoHighlight={true}
        inputValue={props.inputValue}
        onChange={props.onchange}
        autoComplete={true}
        value={props.value}
        id="combo-box-demo"
        popupIcon={<FontAwesomeIcon size="2xs" icon="fa-solid fa-chevron-down" />}
        renderInput={(params) => (
          <div className={styles.wrapper}>
            <TextField
              {...params}
              className={classes.root}
              ref={params.InputProps.ref}
              placeholder={props.children}
              fullWidth
              variant="standard"
            />
          </div>
        )}
      />
    </div>
  );
}
