import axios from 'axios'

export default async (req, res) => {

  const { password, email } = req.body;
  const role_type = req.body.user_type == "couple" ? 3 : 4;
  console.log('the role of the user ' + req.body.user_type)
  await axios
    .post('http://localhost:1337/api/auth/local/register', {
      email,
      password,
      role: role_type
    })
    .then((response) => {
      console.log('\x1b[33m', "[REGISTER]", "user succesfully registered. " + response.data.user);
      res.status(200).end();
    })
    .catch((error) => {
      if (error.response) {
        console.log("error data  " + JSON.stringify(error.response.data));
        console.log("error details  " + JSON.stringify(error.response.data.error.details.path));
        console.log("error status  " + error.response.status);
        console.log("error headers  " + JSON.stringify(error.response.headers));
        res.status(400).send(error.response.data);

      } else if (error.request) {
        console.log('error request :')
        res.status(400).send(error.request);
      } else {
        console.log('error content : ' + error.response)
      }
    });

}