'use strict';

/**
 * my-vendor router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::my-vendor.my-vendor');
