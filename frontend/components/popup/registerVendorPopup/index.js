import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import { ForgotContent } from '../forgotContent';
import { RegisterContentVendor } from '../registerContentVendor';
import { LoginContentVendor } from '../loginContentVendor';


export function RegisterVendorPopup(props) {
  let comp;
  const router = useRouter();

  if (router.query.mode == 'forgot') {
    comp = <ForgotContent />
  }
  if (router.query.mode == 'forgot') {
    comp = <ForgotContent />
  } else if (router.query.mode == 'register-vendor') {
    comp = <RegisterContentVendor />
  } else if (router.query.mode == 'login-vendor') {
    comp = <LoginContentVendor />
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            {comp}
          </div>
        </div>
      </div>
    </>
  );
}
