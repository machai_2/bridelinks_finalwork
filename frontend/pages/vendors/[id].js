import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Rating from '@mui/material/Rating';
import React, { useState, useEffect } from 'react';
import Footer from '../../components/footer';
import Header from '../../components/header';
import { fetchAPI } from '../../lib/api';
import styles from './style.module.scss';
import SimpleImageSlider from 'react-simple-image-slider';
import { HiredPopup } from '../../components/popup/HiredPopup';
import useSWR from 'swr'
import { useSession } from 'next-auth/react';
import { FavoritePopup } from '../../components/popup/favoritePopup';
import FeatherIcon from 'feather-icons-react';
import { IoIosHeart, IoIosHeartEmpty } from "react-icons/io";
import { InputFilter, InputForm } from '../../components/inputs';
import { DashboardSolidButton } from '../../components/buttons';

export default function DetailVendor({ vendor }) {
  const [OpenHired, setOpenHired] = useState(false);
  const [OpenLiked, setOpenLiked] = useState(false);
  const [isHired, setIsHired] = useState(false);
  const [isLiked, setIsLiked] = useState(false);
  const [isRating, setIsRating] = useState(false);
  const [rating, setRating] = useState(0);
  const [error, setError] = useState("");
  const [comment, setComment] = useState("");

  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [body, setBody] = useState("");


  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?user_id=${session.id}` : null,
    fetcher)
  const { data: myVendors, error: myVendorError } = useSWR(`http://localhost:1337/api/my-vendors?filters[id_link]=${vendor.data.attributes.id_link}`, fetcher)
  const { data: Reviews, error: ReviewsError, mutate: mutateReviews } = useSWR(`http://localhost:1337/api/reviews?filters[id_link]=${vendor.data.attributes.id_link}&populate=*`, fetcher)

  if (Reviews) {
    console.log('the data reviews ' + JSON.stringify(Reviews, null, 2))
  }
  const images = [
    { url: `https://www.mariage.be${vendor.data.attributes.image_profile}` },
    { url: `https://www.mariage.be${vendor.data.attributes.image_profile}` },
    { url: `https://www.mariage.be${vendor.data.attributes.image_profile}` },
    { url: `https://www.mariage.be${vendor.data.attributes.image_profile}` },
  ];

  const showHiredPopup = () => {
    setOpenHired(!OpenHired);
    setIsHired(true)
  };
  const showRating = () => {
    if (session) {
      setIsRating(true);
    } else {
      setError('you have to login first to write a review')
    }
  };

  const sendFormEmail = (e) => {
    // e.preventDefault();
    // if (vendor.data.attributes.email&&email) {
    //   try {

    //     const response = await fetch('http://localhost:3000/api/vendor/sendmail', {
    //       method: 'POST',
    //       mode: 'cors',
    //       headers: { "Content-Type": "application/json" },
    //       body: JSON.stringify({ name: `${firstname} ${lastname}`, email: email, urlToken: `http://localhost:3000/rsvp?token=${guestToken.token}` })

    //     })
    //     console.log(response);
    //   } catch (error) {
    //     console.log("send an email error " + JSON.stringify(error))
    //   }
    // }else{

    // }




  }

  const showFavoritePopup = () => {
    setOpenLiked(!OpenLiked);
    setIsLiked(true)
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const user_data = {
      comment: comment,
      id_link: vendor.data.attributes.id_link,
      user_id: session.id,
      rating: rating,
      reviewDate: new Date()
    }
    console.log('user data ' + JSON.stringify(user_data, null, 2) + "   " + comment)
    try {
      const response = await fetch('http://localhost:1337/api/reviews', {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: user_data })

      })
      mutateReviews()
    } catch (error) {
      console.log(error)
    }
    setIsRating(false)
  }

  const convertDate = (date) => {
    let dateWed = new Date(date)
    var datestring = dateWed.getDate() + "/" + dateWed.getMonth() + "/" + dateWed.getFullYear();
    return datestring;
  };

  useEffect(() => {
    myVendorList()
  }, []);
  const myVendorList = () => {
    if (myVendors === undefined) {
      setIsHired(false)
      setIsLiked(false)
    } else {

      if (myVendors.data[0]?.attributes.favorite === true) {
        setIsLiked(true)
      } else {
        setIsLiked(false)
      }
      if (myVendors.data[0]?.attributes.hired === true) {
        setIsHired(true)
      } else {
        setIsHired(false)
      }
    };
  }
  return (
    <div className={styles.container_id}>
      <Header />
      <div className={styles.content_id}>
        <div className={styles.vendor_detail}>
          <div className={styles.name}>
            <span>{vendor.data.attributes.name}</span>


            {/* {isLiked ?
              <Image
                width={15}
                height={15}
                id={styles.heartFilled}
                src="/Bridelinks/heart-solid_ctp92n.svg"
                alt="heartLiked"
              />
              : (
                <div onClick={showFavoritePopup}>
                  <FontAwesomeIcon icon="fa-regular fa-heart" />
                </div>
              )
            } */}

            {isLiked ?
              <div className={styles.favorite}>
                <IoIosHeart size={25}></IoIosHeart>
              </div>
              : (
                <div className={styles.favorite}>
                  <IoIosHeartEmpty size={25} onClick={showFavoritePopup}></IoIosHeartEmpty>
                </div>
              )
            }

          </div>


          <div className={styles.location}>
            <span>{vendor.data.attributes.location}</span>
          </div>
          <div className={styles.stars}>
            <div className={styles.last}>
              <div><Rating name="read-only" value={3} sx={{ color: '#AF4720' }} readOnly /></div>
              {isHired ?
                <div className={styles.handshake}>
                  <FontAwesomeIcon icon="fa-solid fa-circle-check" />
                  <span>Hired</span>
                </div>
                : (
                  <div className={styles.notHandshake} onClick={showHiredPopup}>
                    <FontAwesomeIcon icon="fa-regular fa-handshake" size={"lg"} />
                    <span>Hire ?</span>
                  </div>
                )}

            </div>
          </div>

        </div>

        <div className={styles.split}>
          <div className={styles.carousel_area}>
            <SimpleImageSlider
              width={960}
              height={380}
              images={images}
              navSize={25}
              showNavs={true}
              bgColor={'#e8e2dc'}
              navStyle={2}
              // slideDuration={30}
              autoPlay={false}
              useGPURender={true}
              priority
            />
          </div>
          <div className={styles.right_box}>
            <div className={styles.contact}>
              <h4>Contact us</h4>
              <div><FontAwesomeIcon icon="fa-solid fa-location-dot" size="xs" /> <span>{vendor.data.attributes.location}</span> </div>
              {vendor.data.attributes.home_phone ? <div><FontAwesomeIcon icon="fa-solid fa-phone" size="xs" /> <span>{vendor.data.attributes.home_phone}</span></div> : null}
              {vendor.data.attributes.mobile_phone ? <div><FontAwesomeIcon icon="fa-solid fa-phone" size="xs" /><span>{vendor.data.attributes.mobile_phone}</span></div> : null}
              {vendor.data.attributes.email ? <div><FontAwesomeIcon icon="fa-solid fa-envelope" size="xs" /><span>{vendor.data.attributes.email}</span></div> : null}
              {vendor.data.attributes.website ? <div><FeatherIcon icon="globe" size={'1rem'} color="#AF5520" /><span>{vendor.data.attributes.website}</span></div> : null}
            </div>
            <div className={styles.message}>
              <h4>Message the vendor</h4>
              <div className={styles.messageForm}>
                <form onSubmit={sendFormEmail}>
                  <div><InputFilter placeholder='Firstname*' onchange={(event, value) => setFirstname(value)}></InputFilter></div>
                  <div><InputFilter placeholder='Lastname*' onchange={(event, value) => setLastname(value)}></InputFilter></div>
                  <div><InputFilter placeholder='Email*' onchange={(event, value) => setEmail(value)}></InputFilter></div>
                  <div><InputFilter type="comment" placeholder='Your message*' rows={6} onchange={(event, value) => setBody(value)}></InputFilter></div>
                  <div className={styles.send}><DashboardSolidButton type='submit'>Send your message</DashboardSolidButton></div>
                </form>
              </div>
            </div>
          </div>
        </div>


        <div className={styles.about}>
          <h3 className={styles.subTitle}>About us</h3>
          <div className={styles.description}>{vendor.data.attributes.description}</div>
        </div>
        <div className={styles.reviews}>
          <h3 className={styles.subTitle}>Couple reviews and ratings</h3>
          <div className={styles.headerReview}>
            <div className={styles.averageRating}>
              <span className={styles.score}>ratings ({Reviews?.data?.length})</span>
              <div><Rating name="read-only" value={3} sx={{ color: '#AF4720' }} readOnly /></div>
            </div>
            <div>
              <DashboardSolidButton onClick={showRating}>Add a review</DashboardSolidButton>
              <span>{error}</span>
            </div>
          </div>
          <hr />
        </div>


        {isRating ?
          <div className={styles.postReview}>
            <form onSubmit={handleSubmit}>
              <Rating sx={{ color: '#AF4720' }} onChange={(event, value) => { setRating(value); }} />
              <InputFilter type='comment' placeholder='write a review' rows={10} value={comment} onChange={e => setComment(e.target.value)}></InputFilter>
              <DashboardSolidButton type="submit">Publish review</DashboardSolidButton>
            </form>
          </div> : null}

        <div className={styles.review_cards}>
          {
            Reviews && Reviews.data.map((item, index) => (
              <div className={styles.review_item} key={index}>
                <div className={styles.content_review}>
                  <div className={styles.avatar}>
                    <img
                      src="https://res.cloudinary.com/dmj6tqnkw/image/upload/ar_1:1,b_rgb:f9f9f8,bo_5px_solid_rgb:ffffff,c_fill,co_rgb:ffffff,g_auto,r_max,w_1000/v1661733965/Bridelinks/blanc_user_ixqtht.webp"
                      alt="avatar profile"
                    />
                  </div>
                  <div className={styles.userRating}>
                    <span>{item.attributes.user_id.data?.attributes.firstname ? `${item.attributes.user_id.data.attributes.firstname} ${item.attributes.user_id.data.attributes.lastname}` : 'John Doe'} </span>
                    <div><Rating name="read-only" value={item.attributes.rating} sx={{ color: '#AF4720' }} readOnly /></div>
                    <div>{item.attributes.comment}</div>
                  </div>
                  <div className={styles.dateReview}>{convertDate(item.attributes.reviewDate)}</div>
                </div>
              </div>
            ))
          }
        </div>
      </div>
      {OpenHired ? <HiredPopup vendor={vendor} closePopup={showHiredPopup} weddingId={wedding.data[0].id} /> : null}
      {OpenLiked ? <FavoritePopup vendor={vendor} closePopup={showFavoritePopup} weddingId={wedding.data[0].id} /> : null}

      <Footer />
    </div >
  );
}


export const getStaticProps = async (context) => {
  const id = context.params.id;
  const data = await fetchAPI(`/vendor-nls/${id}`)
  return {
    props: { vendor: data }
  }
}

export const getStaticPaths = async () => {
  const data = await fetchAPI(`/vendor-nls?populate=*`)
  const paths = data.data.map(item => {
    return {
      params: { id: item.id.toString() }
    }
  })

  return {
    paths,
    fallback: true
  }
}