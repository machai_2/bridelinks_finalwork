import styles from './style.module.scss';

export function SliderButtons(props) {
  return <button className={styles.slider_button} onClick={props.onClick} disabled={props.disabled}>{props.children}</button>
}
