import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSession } from 'next-auth/react';
import styles from './../style.module.scss';
import { useState, useEffect } from 'react';
import useSWR from 'swr'
import { DeleteGuestPopup, AddGroupPopup, AddGuestPopup, EditGuestPopup } from '../../../components/popup';
import { EditDropdowns } from '../../../components/dropdowns/editDropdown';
import { InputSearchGuests } from '../../inputs';
import { DashboardSolidButton } from '../../buttons';
import { SpinnerCircularFixed } from 'spinners-react';
import axios from 'axios';


export default function GroupGuests() {

  const [OpenGuestPopup, setOpenGuestPopup] = useState(false);
  const [OpenGroupPopup, setOpenGroupPopup] = useState(false);
  const [deleteGuestPopup, setDeleteGuestPopup] = useState(false);
  const [editGuestPopup, setEditGuestPopup] = useState(false);
  const [filterInvite, setFilterInvite] = useState([]);
  const [filterTables, setFilterTables] = useState([]);
  const [loading, setLoading] = useState(false);
  const [idItem, setIdItem] = useState();
  const [searchQuery, setSearchQuery] = useState(null);

  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}` : null,
    fetcher)
  const { data: Groups, error: GroupsError, mutate: mutateGroups } = useSWR(session ? `http://localhost:1337/api/guest-groups?filters[user_id]=${session.id}` : null,
    fetcher)

  const { data: guests, error: GuestsError, mutate: mutateGuests } = useSWR(wedding ? `http://localhost:1337/api/guest-lists?filters[wedding_id]=${wedding.data[0].id}&populate=*` : null,
    fetcher, { refreshInterval: 3000 })




  const filteredGuests = guests ? guests.data?.filter((el) => {
    if (searchQuery == null) {
      return el;
    }
    else {
      return el.attributes.firstname.toLowerCase().includes(searchQuery);
    }
  }) : [];


  if (GuestsError || GuestsError) <p>Loading failed...</p>;
  if (Groups || guests) <h1>Loading...</h1>;

  const showGuest = () => {
    setOpenGuestPopup(!OpenGuestPopup);
  };

  const showGroup = () => {
    setOpenGroupPopup(!OpenGroupPopup);
  };

  const showDeleteGuest = (id) => {
    setIdItem(id)
    setDeleteGuestPopup(!deleteGuestPopup);
  };
  const showEditGuest = (id) => {
    setIdItem(id)
    setEditGuestPopup(!editGuestPopup);
  };
  let inputHandler = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    setSearchQuery(lowerCase);
  };

  useEffect(() => {
    // GuestStatus()
    getTableGuest()
  }, []);



  const promiseGuest = guests?.data?.map(async (item) => {
    const response = await fetch(`http://localhost:1337/api/invitations?filters[guest_id]=${item.id}&populate=*`)
    let data = await response.json();
    return data.data[0]
  })

  const promiseTable = guests?.data?.map(async (item) => {
    let response = await fetch(`http://localhost:1337/api/table-guests?filters[guest_id]=${item.id}&populate=*`);
    let data = await response.json();
    return data.data[0]
  })
  async function GuestStatus() {
    setLoading(true);
    const temp1 = guests && await Promise.all(promiseGuest).then((results) => {
      setFilterInvite(results)
    })
    setLoading(false);
  }

  const getTableGuest = async () => {
    setLoading(true);
    guests && guests?.data?.map(async (item) => {
      const data = await axios.get(`http://localhost:1337/api/table-guests?filters[guest_id]=${item.id}&populate=*`);
      console.log('the info ' + JSON.stringify(data.data.data))
      setFilterTables([...filterTables, data.data.data[0]]);
    });
    setLoading(false);
  }


  if (loading) return <div className={styles.spinner}><SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} /></div>


  return (
    <div className={styles.container}>
      <div className={styles.header}>

        <div className={styles.group_search}>
          <div className={styles.search}>
            <InputSearchGuests onChange={inputHandler} value={searchQuery}>Search your guests here</InputSearchGuests>
          </div>
        </div>
        <div className={styles.buttonsGroup}>
          <DashboardSolidButton onClick={showGroup}>add a group</DashboardSolidButton>
          {OpenGroupPopup ? <AddGroupPopup id={session.id} mutate={mutateGroups} closePopup={showGroup} /> : null}

          <DashboardSolidButton onClick={showGuest}>add a guest</DashboardSolidButton>
          {OpenGuestPopup ? <AddGuestPopup id={wedding.data[0].id} mutate={mutateGuests} list={Groups} closePopup={showGuest} /> : null}

        </div>
      </div>
      <section className={styles.main_content}>
        {Groups &&
          Groups.data?.map((items, i) => (

            <div className={styles.main_body} key={i}>
              <div>
                <div className={styles.title_category}>
                  <div>
                    {items.attributes.name}
                    <span>{filteredGuests?.filter(el => el.attributes.group_id.data?.id == items.id).length}</span>
                  </div>
                  <EditDropdowns groupname={items.attributes.name} groupId={items.id} mutate={mutateGroups} />
                </div>
                <div className={styles.labels}>
                  <div className={styles.label_item}>
                    <span>NAME</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>
                    <span>STATUS</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>
                    <span>CONTACT</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>
                    <span>TABLE</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>
                    <span>DIET</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                </div>
              </div>
              {filteredGuests
                && filteredGuests?.filter(el => el.attributes.group_id.data?.id == items.id).map((item, index) => (
                  <div className={styles.guest_item} key={index} >
                    <div className={styles.edit_item} onClick={() => showEditGuest(item.id)}>
                      <div className={styles.person_data}>{item.attributes.firstname} {item.attributes.lastname}</div>
                      <div className={styles.person_data}>
                        {filterInvite.filter(el => el.attributes.guest_id.data.id == item.id)
                          .map((item, index) => item.attributes.status)
                        }
                      </div>
                      <div className={styles.person_data}>
                        <FontAwesomeIcon icon="fa-solid fa-phone" className={item.attributes.phone_number ? `${styles.active}` : `${styles.disabled}`} />
                        <FontAwesomeIcon icon="fa-solid fa-envelope" className={item.attributes.email ? `${styles.active}` : `${styles.disabled}`} />
                        <FontAwesomeIcon icon="fa-solid fa-home" className={item.attributes.street ? `${styles.active}` : `${styles.disabled}`} />
                      </div>
                      <div className={styles.person_data}>
                        {console.log('filter tables in map ' + JSON.stringify(filterTables))}
                        {

                          // filterTables.map((item, index) => { return item.attributes.table_id.data.attributes.table_number })
                        }
                      </div>
                      <div className={styles.person_data}>{item.attributes.diet ? `${item.attributes.diet}` : `none`}</div>
                    </div>
                    <div className={styles.delete_item}>
                      <button onClick={() => showDeleteGuest(item.id)} className={styles.deleteGuest}>
                        <FontAwesomeIcon icon="fa-solid fa-trash" />

                      </button>
                    </div>
                    {deleteGuestPopup ? <DeleteGuestPopup mutate={mutateGuests} id={idItem} closePopup={showDeleteGuest} /> : null}
                    {editGuestPopup ? <EditGuestPopup list={Groups} mutate={mutateGuests} id={idItem} closePopup={showEditGuest} /> : null}
                  </div>
                ))}
            </div>
          ))}
      </section>
    </div>
  );
}



