import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { signIn } from 'next-auth/react';
import { PrimaryButton, SocialsButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useState, useRef } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import Link from 'next/link'
import { SpinnerCircularFixed } from 'spinners-react';

export function RegisterContent() {

  const router = useRouter();


  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .required('Email is required')
      .email('Email is invalid'),
    password: Yup.string()
      .min(6, 'Password must be at least 6 characters')
      .required('Password is required'),
  });


  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const [errors, setError] = useState(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  async function handleSignIn(em, pass) {
    setLoading(true);
    const res = await signIn('credentials', {
      redirect: false,
      email: em,
      password: pass,
      url: `${window.location.origin}`,
    });
    if (res?.error) {
      console.log('register errors ' + res.error)
      setError(res.error);
    } else {
      setError(null);
    }
    setLoading(false);
  }


  async function onsubmit(e) {
    setLoading(true);
    e.preventDefault();
    const user_type = "couple";
    try {
      await axios.post('/api/auth/register', { email, password, user_type });
      handleSignIn(email, password)
      router.replace("/dashboard/questions");
      setLoading(false);
    } catch (err) {
      console.log('the reg error ' + JSON.stringify(err.response.data))
    }
  }


  function handleEmailChange(event) { setEmail(event.target.value); }

  function handlePasswordChange(event) { setPassword(event.target.value); }

  if (loading) return <SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} />

  return (
    <>
      <div>
        <div>
          <h2>Create your new account</h2>
        </div>

        <div className={styles.buttonsSocials}>
          <SocialsButton className={styles.button_items} icon="fa-brands fa-google" size="xl" onClick={() => signIn('google')}>
            Log in with Google
          </SocialsButton>
          <SocialsButton className={styles.button_items} icon="fa-brands fa-facebook-square" size="xl" onClick={() => signIn('facebook')}>
            Log in with Facebook
          </SocialsButton>
        </div>
        <div className={styles.text_OR}>
          <span>OR</span>
        </div>

        <form className={styles.form} onSubmit={(e) => onsubmit(e)}>
          <label>
            <InputForm type='email' {...register('email')} onChange={handleEmailChange} placeholder="email" />
          </label>
          <label>
            <InputForm name="password" type="password" {...register('password')} onChange={handlePasswordChange} placeholder="password" />
          </label>
          <div className={styles.form_button}>
            <PrimaryButton type="submit">Register</PrimaryButton>
          </div>
        </form>
        <div className={styles.footer}>
          <span>Do you already have an account?
            <Link href={{ pathname: '/', query: { mode: 'login' } }}>
              <a>
                <span className={styles.linkPopup}>Already have an account?</span>
              </a>
            </Link>
          </span>
        </div>
      </div>
    </>
  );
}
