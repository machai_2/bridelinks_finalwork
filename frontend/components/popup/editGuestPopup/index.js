import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputCheckbox, InputForm } from '../../inputs';
import { SelectDashboard, SelectGroups } from '../../selects';
import styles from './style.module.scss';
import { useSession } from 'next-auth/react';
import { CollapseDashboard } from '../../Collapse';
import useSWR, { mutate } from 'swr'
import { useRouter } from 'next/router';



export function EditGuestPopup(props) {

  const age = [
    {
      label: "Adult",
      value: "adult",
    },
    {
      label: "Child",
      value: "child",
    },
    {
      label: "Baby",
      value: "baby",
    },
  ];


  const [email, setEmail] = useState("");
  const [number, setNumber] = useState("");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [city, setCity] = useState("");
  const [zipcode, setZipcode] = useState("");
  const [address, setAdress] = useState("");
  const [sendEmail, setSendEmail] = useState(false);
  const [sendPhone, setSendPhone] = useState(false);
  const [ageGroup, setAgeGroup] = useState(age[0].value);
  const [guestGroup, setGuestGroup] = useState(props.list.data[0].attributes.name);
  const [titleGuest, setTitleGuest] = useState("Mrs");
  const [diet, setDiet] = useState("None");

  const { data: session } = useSession();
  const router = useRouter()

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: guests, error: error } = useSWR(props.id ? `http://localhost:1337/api/guest-lists/${props.id}` : null,
    fetcher)

  if (error) <p>Loading failed...</p>;
  if (guests) <h1>Loading...</h1>;

  useEffect(() => {
    if (guests?.data) {
      console.log('the guest here ' + JSON.stringify(guests?.data) + "  " + props.id)
      setAdress(guests.data.attributes.street);
      setZipcode(guests.data.attributes.zipcode);
      setCity(guests.data.attributes.city);
      setAgeGroup(guests.data.attributes.age);
      setTitleGuest(guests.data.attributes.title);
      setEmail(guests.data.attributes.email);
      setNumber(guests.data.attributes.phone_number);
      setLastname(guests.data.attributes.firstname);
      setFirstname(guests.data.attributes.lastname);
      setGuestGroup(guests.data.attributes.group);
      setDiet(guests.data.attributes.diet)
    }
  }
    , [guests]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const user_data = {
      guest_id: props.id,
      user_id: session.id,
      wedding_id: props.id,
      firstname,
      lastname,
      zipcode,
      address,
      diet,
      city,
      phone_number: number,
      title: titleGuest,
      email,
      age: ageGroup,
      group: guestGroup,
      sendEmail,
      sendPhone
    }
    axios.put(`http://localhost:1337/api/invitation/update`, {
      data: { user_data }
    }).catch(function (err) {
      if (err.response) {
        console.log(err);
      }
    });
    props.mutate(`http://localhost:3000/api/invitation/update`)

    props.closePopup();
  };
  const diets = [
    {
      label: "None",
      value: "None",
    },
    {
      label: "Vegetarian",
      value: "Vegetarian",
    },
    {
      label: "Pescatarian",

      value: "Pescatarian",
    },
    {
      value: "Hallal",
      label: "Hallal",

    },
    {
      label: "Vegan",
      value: "Vegan",
    },
    {
      label: "Kosher",
      value: "Kosher",
    }
  ]

  const title = [
    {
      label: "Mrs",
      value: "Mrs",
    },
    {
      label: "Mr",
      value: "Mr",
    },
    {
      label: "Ms",
      value: "Ms",
    },
  ]

  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body} key={`${firstname} ${lastname}`}>
          <form onSubmit={handleSubmit}>
            <h3>Edit {firstname} {lastname} Guests</h3>
            <section className={styles.content} >
              <div className={styles.short_items}>
                <div className={styles.item}>
                  <label>Title</label>
                  <div className={styles.input}>
                    <SelectDashboard options={title} defaultValue={title} onChange={(e) => { setTitleGuest(e.target.value) }}></SelectDashboard>
                  </div>
                </div>
                <div className={styles.item}>
                  <label>Firstname</label>
                  <div className={styles.input}>
                    <InputForm type='text' placeholder="firstname" defaultValue={firstname} id={firstname} onChange={(e) => { setFirstname(e.target.value) }} />
                  </div>
                </div>
                <div className={styles.item}>
                  <label>Lastname</label>
                  <div className={styles.input}>
                    <InputForm type='text' placeholder="lastname" defaultValue={lastname} id={lastname} onChange={(e) => { setLastname(e.target.value) }} />
                  </div>
                </div>
              </div>

              <div className={styles.short_items}>
                <div className={styles.item}>
                  <label>Age group</label>
                  <SelectDashboard options={age} defaultValue={age} onChange={(e) => { setAgeGroup(e.target.value) }}></SelectDashboard>
                </div>
                <div className={styles.item}>
                  <label>Relationship</label>
                  <SelectGroups options={props.list.data} defaultValue={guestGroup} onChange={(e) => { setGuestGroup(e.target.value) }}></SelectGroups>
                </div>
                <div className={styles.item}>
                  <label>Diet</label>
                  <SelectDashboard options={diets} defaultValue={diet} onChange={(e) => { setDiet(e.target.value) }}></SelectDashboard>
                </div>
              </div>

              <div className={styles.short_items}>
                <div className={styles.item}> <label>Email</label>
                  <div className={styles.input}>

                    <InputForm type="email" placeholder="Email" defaultValue={email} onChange={(e) => { setEmail(e.target.value) }} />
                  </div>
                </div>
                <div className={styles.item}> <label>Phone number</label><InputForm type="text" defaultValue={number} placeholder="Phone number" onChange={(e) => { setNumber(e.target.value) }} /></div>
              </div>

              <div className={styles.short_items}>
                <div className={styles.item}> <label>Adress</label>                  <div className={styles.input}>
                  <InputForm placeholder="Adress" defaultValue={address} onChange={(e) => { setAdress(e.target.value) }} /></div></div>
                <div className={styles.item}> <label>City</label>                  <div className={styles.input}>
                  <InputForm placeholder="City" defaultValue={city} onChange={(e) => { setCity(e.target.value) }} /></div></div>
                <div className={styles.item}> <label>Zipcode</label>                  <div className={styles.zipcode}>
                  <InputForm placeholder="Zipcode" defaultValue={zipcode} onChange={(e) => { setZipcode(e.target.value) }} />
                </div></div>
              </div>
              <div className={styles.checkboxes}>
                <span>
                  Send an invitation by ?
                </span>
                <div>
                  <div className={styles.check}>
                    <label htmlFor='num1'>Email</label>
                    <InputCheckbox id={"email"} onChange={(e) => { setSendEmail(e.target.checked) }} />

                  </div>
                  <div className={styles.check}>
                    <label htmlFor='num2'>Phone</label>
                    <InputCheckbox id={"Phone"} onChange={(e) => { setSendPhone(e.target.checked) }} />
                  </div>                </div>
              </div>
              <div className={styles.submitClose}>
                <DashboardSolidButton type="submit">Submit & close</DashboardSolidButton>
              </div>
            </section>
          </form>
        </div>
      </div>
    </div>
  );
}
