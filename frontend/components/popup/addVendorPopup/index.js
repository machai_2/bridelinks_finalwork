import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState, useEffect } from 'react';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputFilter, InputForm } from '../../inputs';
import styles from './style.module.scss';
import useSWR from 'swr'
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';



export function AddVendorPopup(props) {
  const [selectedVendor, setSelectedVendor] = useState("")
  const { data: session } = useSession();
  const router = useRouter()

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}` : null,
    fetcher)
  const { data: vendors, error: error } = useSWR(`http://localhost:1337/api/vendor-nls`, fetcher)
  const filter_vendors = vendors?.data?.map(item => item.attributes.name);

  console.log('the filter vendors ' + filter_vendors)
  if (error) <p>Loading failed...</p>;
  if (vendors) <h1>Loading...</h1>;


  const AddVendor = async () => {
    const itemId = 0;
    console.log('the selected ' + selectedVendor)
    vendors?.data?.filter(e => e.attributes.name == selectedVendor).map(item => {
      itemId = item.attributes.id_link
      console.log('the item here ' + JSON.stringify(item) + "   " + wedding.data[0].id)
    });
    const objectData = {
      hired: true,
      wedding_id: wedding.data[0].id,
      id_link: itemId
    }
    console.log('ITEM ID ' + itemId)
    try {
      const response = await fetch('http://localhost:1337/api/my-vendors/', {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: objectData })
      })
      console.log(response);
    } catch (error) {
      console.log(error)
    }
    router.reload(window.location.pathname)

    props.closePopup()
  }

  const SearchCatalog = async () => {
    router.push({ pathname: '/vendors' })
  }
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <h3>Add your vendor to your list</h3>
          <div className={styles.popup_content}>

            <div className={styles.input}>
              <InputFilter type="text" list={filter_vendors} placeholder="Search a vendor by name" onchange={(event, value) => setSelectedVendor(value)} />
            </div>
            <div className={styles.saveButton}>
              <DashboardSolidButton onClick={AddVendor}>Save</DashboardSolidButton>
            </div>
          </div>
          <div>
            <div className={styles.notChoice}>

              <div>Not made your choice yet?</div> <DashboardOutlineButton onClick={SearchCatalog}>Search our catalog</DashboardOutlineButton>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
