import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';

import styles from './style.module.scss';

export function CollapseDashboard({ children, name }) {
  const [OpenDropdown, setOpenDropdown] = useState(true);
  
  const showDropdown = () => {
    setOpenDropdown(!OpenDropdown);
  };

  return (
    <div>
      <button className={styles.containerButton} onClick={showDropdown}>
          <span className={styles.titleDropdown}>{name}</span>
          <FontAwesomeIcon
          className={`${OpenDropdown ? styles.ArrowUp : styles.ArrowDown}`}
          size="xs"
          icon="fa-solid fa-chevron-down"
        />
      </button>

      {OpenDropdown ? <div className={styles.dropdownMenu}>{children}</div> : null}
    </div>
  );
}
