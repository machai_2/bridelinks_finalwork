import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState, useEffect } from 'react';
import { useSession } from 'next-auth/react';
import { DashboardSolidButton } from '../../../components/buttons';
import DashboardLayout from '../../../components/layout/dashboard';
import styles from './style.module.scss';
import useSWR from 'swr'
import { AddGuestTable, AddTablePopup, DeleteTablePopup } from '../../../components/popup';
import { SpinnerCircularFixed } from 'spinners-react';
import axios from 'axios';



export default function Table() {
  const { data: session } = useSession();
  const [selectedTable, setSelectedTable] = useState(null);
  const [guests, setGuests] = useState()
  const [OpenTablePopup, setOpenTablePopup] = useState(false);
  const [OpenGuestPopup, setOpenGuestPopup] = useState(false);
  const [loading, setLoading] = useState(false);

  const [OpenDeleteTablePopup, setOpenDeleteTablePopup] = useState(false);

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?user_id=${session.id}` : null,
    fetcher)
  const { data: tables, error: tablesErr, mutate: mutateList } = useSWR(wedding ? `http://localhost:1337/api/tables?filters[wedding_id]=${wedding.data[0].id}&populate=*` : null,
    fetcher)

  useEffect(() => {
    if (selectedTable == null && tables) {
      setSelectedTable(tables?.data[0]);
      fetchGuests(tables?.data[0]?.id)
    }
  }, [tables]);


  const showAddTable = () => {
    setOpenTablePopup(!OpenTablePopup);
  };

  const showAddGuest = () => {
    setOpenGuestPopup(!OpenGuestPopup);
  };
  const showDeleteTable = () => {
    setOpenDeleteTablePopup(!OpenDeleteTablePopup);
  };
  const handleClick = (table) => {
    setSelectedTable(table);
    fetchGuests(table.id);
  };


  if (tables && tablesErr) <p>Loading failed...</p>;
  if (tablesErr) return <div>Failed to load</div>
  if (!tables) return <SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} />

  const fetchGuests = async (id) => {
    setLoading(true);
    const res = await fetch(`http://localhost:1337/api/table-guests?filters[table_id]=${id}&populate=*`);
    const data = await res.json()
    setGuests(data)
    setLoading(false);
  };

  const deleteGuest = (item) => {
    document.getElementById(`id-${item.id}`).remove();
    console.log('the geust item +' + JSON.stringify(item))
    axios.delete(`http://localhost:1337/api/table-guests/${item.id}`).then(
    ).catch(function (error) {
      console.log(error);
    });
    mutateList(`http://localhost:1337/api/table-guests/${item.id}`)

  }

  return (
    <div className={styles.container}>
      <div className={styles.table_overview}>
        <div id={styles.container_title}>
          <div className={styles.title_dashboard}><span>TABLE PLAN</span></div>
          <div className={styles.button_group}>
            <DashboardSolidButton onClick={showAddTable}>add a table</DashboardSolidButton>
            {OpenTablePopup ? <AddTablePopup weddingId={wedding.data[0].id} closePopup={showAddTable} mutate={mutateList} /> : null}
          </div>
        </div>
        <div className={styles.tables}>
          {tables ?
            tables?.data?.map((item, index) => (
              <div key={index} onClick={() => handleClick(item)}>
                <span className={selectedTable?.id == item.id ? `${styles.active_table_content}` : `${styles.table_content}`} key={index}>
                  {item.attributes.table_number}
                </span>
              </div>
            ))
            : <div>
              <h3>The Tables is empty</h3>
              <p>Add some tables first</p>
            </div>}
        </div>
      </div>
      <div className={styles.table_detail}>
        <div className={styles.title_dashboard}>
          <span>TABLE NUMBER {selectedTable?.attributes?.table_number}</span>
          <button className={styles.deleteTable} onClick={showDeleteTable}>
            <FontAwesomeIcon icon="fa-solid fa-trash" />
          </button>
        </div>

        {OpenDeleteTablePopup ? <DeleteTablePopup closePopup={showDeleteTable} id={selectedTable.id} mutate={mutateList} /> : null}

        <div className={styles.detail_content}>
          <div>
            <div className={styles.number}>
              <span>Number of seats</span>
              <span>{selectedTable?.attributes?.seats}</span>
            </div>
            <div className={styles.number}>
              <span>Number of guests</span>
              <span>{guests ? guests?.data?.length : ""}</span>
            </div>
          </div>
          {guests && guests?.data?.map((items, i) => (
            items.attributes.guest_id.data ?
              (<div className={styles.tag_container} key={i} onClick={() => deleteGuest(items)}>
                <div className={styles.tag} id={`id-${items.id}`}>
                  <span>{items.attributes.guest_id.data != null ? `${items.attributes.guest_id.data.attributes.firstname} ${items.attributes.guest_id.data.attributes.lastname}` : 'add a guests '}</span>
                  <FontAwesomeIcon icon="fa-solid fa-xmark" size="xl" />
                </div>
              </div>) : null
          ))}
          <div className={styles.add_button}>
            <DashboardSolidButton onClick={showAddGuest}>add a guest</DashboardSolidButton>
            {OpenGuestPopup ? <AddGuestTable tableId={selectedTable.id} weddingId={wedding.data[0].id} closePopup={showAddGuest} mutate={mutateList} /> : null}
          </div>
        </div>
      </div>
    </div>

  );
}

Table.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
