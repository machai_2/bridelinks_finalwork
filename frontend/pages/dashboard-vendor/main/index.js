import { useSession, getSession } from 'next-auth/react';
import { useEffect, useState } from "react";
import VendorLayout from './../../../components/layout/vendor';
import styles from './style.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FeatherIcon from 'feather-icons-react';
export default function Main() {
  return (
    <div className={styles.container}>
      <section>
        <div className={styles.cover}>
          <div className={styles.cover_content}>
            <div className={styles.card}>
              <div className={styles.icon}> <FontAwesomeIcon size="lg" icon="fa-shop" /></div>
              <h2 className={styles.card_title}>Set up shop</h2>
              <span>Customize your shop so it's more memorable and engaging. Make it truly unique.</span>
              <div className={styles.checklist}>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /> <li>Add a cover image</li></div>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /><li>Add a social link</li></div>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /><li>Add a description</li></div>
              </div>
            </div>
            <div className={styles.card}>
              <div className={styles.icon}><FontAwesomeIcon size="lg" icon="fa-pen" /></div>
              <h2 className={styles.card_title}>General information</h2>
              <span>Confirm your account details and change them here.</span>
              <div className={styles.checklist}>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /><li>Add your name and address</li></div>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /><li>Add a social link</li></div>
              </div>
            </div>
            <div className={styles.card}>
              <div className={styles.icon}>      <FeatherIcon icon="image" size={'2rem'} />
              </div>
              <h2 className={styles.card_title}>Setup Gallery</h2>
              <span>Add more pictures to make your listing more eyecatching</span>
              <div className={styles.checklist}>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /><li>Add a thumbnail cover photo</li></div>
                <div><FontAwesomeIcon icon="fa-solid fa-chevron-right" size="2xs" /><li>Add a secondary pictures</li></div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div >
  )



}


Main.getLayout = function getLayout(page) {
  return <VendorLayout>{page}</VendorLayout>;
};
