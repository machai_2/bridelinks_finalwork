
import styles from './style.module.scss';
import React, { useState } from 'react';
import { InputSearchVendors } from './../../../inputs';


export function Page4({ data, setData, list }) {
      return (
            <div className={styles.container}>
                  <h2>Where is you business located ?</h2>
                  <div>
                        <div className={styles.content_container}>
                              The location is
                              <div>
                                    <InputSearchVendors list={list} onchange={(event, value) => setData({ ...data, location: value })}>City</InputSearchVendors>
                              </div>
                        </div>
                  </div>
            </div>
      )
}
