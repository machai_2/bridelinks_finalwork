import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { DeleteButton, CancelButton } from '../../buttons';
import { useRouter } from 'next/router';

export function DeleteTablePopup(props) {
  const router = useRouter();

  const deleteTable = () => {
    fetch(`http://localhost:1337/api/tables/${props.id}`, {  method: 'DELETE',  }).then(res => res.json())
     .then(res => console.log(res))
    router.reload(window.location.pathname)
    props.closePopup()
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            <h4>Are you sure you want the table?</h4>
            <span>This cannot be undone</span>
            <div className={styles.buttonGroup}>
              <CancelButton onClick={props.closePopup}>Cancel</CancelButton>
              <DeleteButton id={props.id} onClick={deleteTable}>Delete</DeleteButton>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
