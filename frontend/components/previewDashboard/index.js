import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FeatherIcon from 'feather-icons-react';
import Link from 'next/link';
import { useEffect, useState } from "react";
import CountdownTimer from './../countdown';
import styles from './style.module.scss';
import useSWR from 'swr'
import { useSession } from 'next-auth/react';


export function PreviewDashboard() {
  const { data: session } = useSession();

  const [weddingDate, setWeddingDate] = useState(null);
  const [taskDone, setTaskDone] = useState(0);
  const [taskOngoing, setTaskOngoing] = useState(0);
  const [weddingBudget, setWeddingBudget] = useState(0);
  const [estimatedTotal, setEstimatedTotal] = useState(0);
  const [costTotal, setCostTotal] = useState(0);
  const [attending, setAttending] = useState(0);
  const [partnerFirstname, setPartnerFirstname] = useState(0);
  const [partnerLastname, setPartnerLastname] = useState(0);
  const [user, setUser] = useState(null);
  const [favorite, setFavorite] = useState(0);
  const [hired, setHired] = useState(0);
  const [convertedDate, setConvertedDate] = useState(0);

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}` : null,
    fetcher)
  const { data: budgets, error: budgetErr } = useSWR(wedding ? `http://localhost:1337/api/budgets?filters[wedding_id]=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: checklist, error: checklistErr } = useSWR(session ? `http://localhost:1337/api/checklists?filters[user_id]=${session.id}&populate=*` : null,
    fetcher)
  const { data: Guests, error: guestErr } = useSWR(wedding ? `http://localhost:1337/api/guest-lists?wedding_id=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: invitations, error: inviteError } = useSWR(wedding ? `http://localhost:1337/api/invitations?filters[wedding_id]=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: myVendors, error: myVendorsErr } = useSWR(wedding ? `http://localhost:1337/api/my-vendors?wedding_id=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  if (budgetErr || inviteError || checklistErr || weddingError || guestErr) {
    return <div>'loading error ...'</div>
  }
  if (budgets || wedding || checklist || invitations) <h1>Loading...</h1>;


  const fetchUser = async () => {
    const res = await fetch(`http://localhost:3000/api/auth/restricted`);
    const data = await res.json();
    setUser(data);
  };

  useEffect(() => {
    fetchUser();
  });

  const convertDate = (date) => {
    let dateWed = new Date(date)
    var datestring = dateWed.getDate() + "." + dateWed.getMonth() + "." + dateWed.getFullYear();
    setConvertedDate(datestring)
  };

  useEffect(() => {
    if (wedding) {
      const weddingDate = new Date(wedding.data[0]?.attributes.date_wedding).getTime();

      setWeddingDate(wedding.data[0]?.attributes.date_wedding)
      convertDate(wedding.data[0]?.attributes.date_wedding)
      setPartnerFirstname(wedding.data[0]?.attributes.partner_firstname)
      setPartnerLastname(wedding.data[0]?.attributes.partner_lastname)
      setWeddingBudget(parseFloat(wedding.data[0]?.attributes.budget))
    }

    if (budgets) {
      const total = budgets.data?.reduce((accumulator, object) => { return accumulator + object.attributes.estimated_cost; }, 0);
      const costs = budgets.data?.reduce((accumulator, object) => { return accumulator + object.attributes.amount_paid; }, 0);
      setEstimatedTotal(parseFloat(total))
      setCostTotal(parseFloat(costs))
    }
    if (checklist) {
      const notDone = checklist?.data?.filter((item) => { return item.attributes.done == false })
      const isDone = checklist?.data?.filter((item) => { return item.attributes.done == true })
      setTaskOngoing(notDone.length)
      setTaskDone(isDone.length)
    }
    if (invitations) {
      const attending = invitations?.data?.filter((item) => {
        return item.attributes.status == "accepted"
      }).length
      setAttending(attending)
    }
    if (myVendors) {
      const favorite = myVendors?.data?.filter((item) => { return item.attributes.favorite == true })
      const hired = myVendors?.data?.filter((item) => { return item.attributes.hired == true })
      setFavorite(favorite.length)
      setHired(hired.length)
    }
  }, [wedding, budgets, checklist, invitations, myVendors])


  return (
    <section className={styles.container}>
      <h2>Welcome back {user?.firstname === undefined ? `${session.user.name}` : `${user?.firstname} ${user?.lastname}`} & {partnerFirstname} {partnerLastname}</h2>
      <div className={styles.preview_container}>
        <div className={styles.wedding_content}>
          <span className={styles.nameCouple}>{user?.firstname === undefined ? `${session.user.name}` : `${user?.firstname} ${user?.lastname}`} & {partnerFirstname} {partnerLastname}</span>
          <span>{convertedDate}</span>
          <div className={styles.countdown}>
            <CountdownTimer targetDate={weddingDate} type="1" />
          </div>
        </div>
        <div className={styles.preview_body}>
          <div className={styles.preview_header}>
            <div className={styles.header_title}>Planning progress</div>
            <div className={styles.header_button}>
              <Link href="/dashboard/main" >
                <a>
                  My Wedding <FontAwesomeIcon icon="fa-solid fa-chevron-right" />
                </a>
              </Link>
            </div>
          </div>

          <div className={styles.groupItems}>
            <div className={styles.item}>
              <FeatherIcon icon="shopping-cart" size={'3rem'} color="#AF5520" />
              <div className={styles.item_title}>
                <span>{hired} out of {favorite}</span>
              </div>
              <span className={styles.description}>vendors hired</span>
            </div>
            <div className={styles.item} >
              <FeatherIcon icon='check-circle' size={'3rem'} color="#AF5520" />
              <div className={styles.item_title}>
                <span>{taskDone} out of {checklist?.data?.length}</span>
              </div>
              <span className={styles.description}>tasks completed</span>
            </div>
            <div className={styles.item} >
              <FeatherIcon icon='users' size={'3rem'} color="#AF5520" />
              <div className={styles.item_title}>
                <span>{attending} out of {Guests?.data?.length}</span>
              </div>
              <span className={styles.description}>guests attending</span>
            </div>
            <div className={styles.item} >
              <FeatherIcon icon='dollar-sign' size={'3rem'} color="#AF5520" />
              <div className={styles.item_title}>
                <span>{costTotal.toFixed(2)}€ out of {weddingBudget.toFixed(2)}€</span>
              </div>
              <span className={styles.description}>Budget spent</span>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
