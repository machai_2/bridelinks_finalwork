import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { DashboardSolidButton } from '../../buttons';
import { InputCheckbox, InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';

export function AddTaskPopup(props) {

  const tags = ["engagement", "pre-wedding", "post-wedding", "paperwork"]
  const router = useRouter();

  const [titleTask, setTitleTask] = useState();
  const [description, setDescription] = useState();
  const [date, setDate] = useState();
  const [done, setDone] = useState(false);
  const { data: session } = useSession();
  const [category, setCategory] = useState('engagement');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      title: titleTask,
      done,
      tag: category,
      period: date,
      user_id: session.id,
      description
    }

    try {
      const response = await fetch('http://localhost:1337/api/checklists/', {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data })

      })
    } catch (error) {
      console.log(error)
    }
    router.reload(window.location.pathname)
    props.closePopup()
  };


  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h3>Create a new task</h3>
            <section className={styles.content}>
              <div className={styles.item}>
                <label>Title of the task</label>
                <div className={styles.input}>
                  <InputForm type='text' placeholder='name your task' onChange={(e) => { setTitleTask(e.target.value) }}></InputForm>
                </div>
              </div>
              <div className={styles.item}>
                <label>Description of the task</label>
                <div className={styles.input}>
                  <InputForm type='text' placeholder='add a note or description of the task' onChange={(e) => { setDescription(e.target.value) }}></InputForm>
                </div>
              </div>
              <div className={styles.short_items}>
                <div className={styles.date_item}>
                  <label>Due date</label>
                  <div className={styles.input}>
                    <InputForm type='date' placeholder='date' onChange={(e) => { setDate(e.target.value) }}></InputForm>
                  </div>
                </div>
                <div className={styles.category_item}>
                  <label>Category</label>
                  <div className={styles.input}>
                    <select name="categories" onChange={(e) => { setCategory(e.target.value) }}>
                      {tags && tags.map((item, index) => (<option value={item}>{item}</option>))}
                    </select>                  </div>
                </div>
              </div>
              <div className={styles.mark}>
                <label>Mark as done</label>
                <div className={styles.input}>
                  <InputCheckbox onChange={(e) => { setDone(e.target.checked) }} />
                </div>
              </div>
            </section>
            <div className={styles.saveTask}>
              <DashboardSolidButton type="submit">Create</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
