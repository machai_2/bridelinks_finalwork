'use strict';

/**
 * plus-one service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::plus-one.plus-one');
