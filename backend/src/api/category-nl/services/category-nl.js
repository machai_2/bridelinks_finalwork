'use strict';

/**
 * category-nl service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::category-nl.category-nl');
