"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/auth/[...nextauth]";
exports.ids = ["pages/api/auth/[...nextauth]"];
exports.modules = {

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ "next-auth":
/*!****************************!*\
  !*** external "next-auth" ***!
  \****************************/
/***/ ((module) => {

module.exports = require("next-auth");

/***/ }),

/***/ "next-auth/providers/credentials":
/*!**************************************************!*\
  !*** external "next-auth/providers/credentials" ***!
  \**************************************************/
/***/ ((module) => {

module.exports = require("next-auth/providers/credentials");

/***/ }),

/***/ "next-auth/providers/facebook":
/*!***********************************************!*\
  !*** external "next-auth/providers/facebook" ***!
  \***********************************************/
/***/ ((module) => {

module.exports = require("next-auth/providers/facebook");

/***/ }),

/***/ "next-auth/providers/google":
/*!*********************************************!*\
  !*** external "next-auth/providers/google" ***!
  \*********************************************/
/***/ ((module) => {

module.exports = require("next-auth/providers/google");

/***/ }),

/***/ "(api)/./pages/api/auth/[...nextauth].js":
/*!*****************************************!*\
  !*** ./pages/api/auth/[...nextauth].js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var next_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next-auth */ \"next-auth\");\n/* harmony import */ var next_auth__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_auth__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_auth_providers_credentials__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next-auth/providers/credentials */ \"next-auth/providers/credentials\");\n/* harmony import */ var next_auth_providers_credentials__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_auth_providers_credentials__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_auth_providers_facebook__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next-auth/providers/facebook */ \"next-auth/providers/facebook\");\n/* harmony import */ var next_auth_providers_facebook__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_auth_providers_facebook__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_auth_providers_google__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-auth/providers/google */ \"next-auth/providers/google\");\n/* harmony import */ var next_auth_providers_google__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_auth_providers_google__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);\n\n\n\n\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (next_auth__WEBPACK_IMPORTED_MODULE_0___default()({\n    providers: [\n        next_auth_providers_google__WEBPACK_IMPORTED_MODULE_3___default()({\n            clientId: process.env.GOOGLE_ID,\n            clientSecret: process.env.GOOGLE_SECRET\n        }),\n        next_auth_providers_facebook__WEBPACK_IMPORTED_MODULE_2___default()({\n            clientId: process.env.FACEBOOK_ID,\n            clientSecret: process.env.FACEBOOK_SECRET\n        }),\n        next_auth_providers_credentials__WEBPACK_IMPORTED_MODULE_1___default()({\n            name: \"credentials\",\n            async authorize (credentials) {\n                try {\n                    const { data  } = await axios__WEBPACK_IMPORTED_MODULE_4___default().post(\"http://localhost:1337/api/auth/local\", {\n                        identifier: credentials.email,\n                        password: credentials.password\n                    });\n                    const user = {\n                        ...data.user,\n                        jwt: data.jwt\n                    };\n                    if (user) {\n                        return user;\n                    } else {\n                        return null;\n                    }\n                } catch (e) {\n                    return null;\n                }\n            }\n        }), \n    ],\n    database: \"postgres://strapi:strapi@localhost:5432/strapi?synchronize=true\",\n    session: {\n        jwt: true,\n        maxAge: 30 * 24 * 60 * 60\n    },\n    callbacks: {\n        async redirect ({ url , baseUrl  }) {\n            return baseUrl;\n        },\n        async session ({ session , token , user  }) {\n            session.jwt = token.jwt;\n            session.id = token.id;\n            return Promise.resolve(session);\n        },\n        async jwt ({ token , user , account , profile , isNewUser  }) {\n            if (user) {\n                if (user.provider == \"local\") {\n                    token.jwt = user.jwt;\n                    token.id = user.id;\n                } else {\n                    try {\n                        const response = await fetch(`${\"http://localhost:1337\"}/api/auth/${account.provider}/callback?access_token=${account?.access_token}`);\n                        const data = await response.json();\n                        console.log(\"THE DATA \" + JSON.stringify(data));\n                        token.jwt = data.jwt;\n                        token.id = data.user.id;\n                    } catch (err) {\n                        console.log(\"THE ERROR HERE \" + err);\n                    }\n                }\n            }\n            return Promise.resolve(token);\n        }\n    }\n}));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9wYWdlcy9hcGkvYXV0aC9bLi4ubmV4dGF1dGhdLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQWlDO0FBQ2lDO0FBQ047QUFDSjtBQUM5QjtBQUcxQixpRUFBZUEsZ0RBQVEsQ0FBQztJQUN0QkssU0FBUyxFQUFFO1FBQ1RGLGlFQUFjLENBQUM7WUFDYkcsUUFBUSxFQUFFQyxPQUFPLENBQUNDLEdBQUcsQ0FBQ0MsU0FBUztZQUMvQkMsWUFBWSxFQUFFSCxPQUFPLENBQUNDLEdBQUcsQ0FBQ0csYUFBYTtTQUN4QyxDQUFDO1FBQ0ZULG1FQUFnQixDQUFDO1lBQ2ZJLFFBQVEsRUFBRUMsT0FBTyxDQUFDQyxHQUFHLENBQUNJLFdBQVc7WUFDakNGLFlBQVksRUFBRUgsT0FBTyxDQUFDQyxHQUFHLENBQUNLLGVBQWU7U0FDMUMsQ0FBQztRQUVGWixzRUFBbUIsQ0FBQztZQUNsQmEsSUFBSSxFQUFFLGFBQWE7WUFDbkIsTUFBTUMsU0FBUyxFQUFDQyxXQUFXLEVBQUU7Z0JBQzNCLElBQUk7b0JBQ0YsTUFBTSxFQUFFQyxJQUFJLEdBQUUsR0FBRyxNQUFNYixpREFBVSxDQUFDLHNDQUFzQyxFQUFFO3dCQUN4RWUsVUFBVSxFQUFFSCxXQUFXLENBQUNJLEtBQUs7d0JBQzdCQyxRQUFRLEVBQUVMLFdBQVcsQ0FBQ0ssUUFBUTtxQkFDL0IsQ0FBQztvQkFDRixNQUFNQyxJQUFJLEdBQUc7d0JBQUUsR0FBR0wsSUFBSSxDQUFDSyxJQUFJO3dCQUFFQyxHQUFHLEVBQUVOLElBQUksQ0FBQ00sR0FBRztxQkFBRTtvQkFFNUMsSUFBSUQsSUFBSSxFQUFFO3dCQUNSLE9BQU9BLElBQUksQ0FBQztxQkFDYixNQUNJO3dCQUNILE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGLENBQUMsT0FBT0UsQ0FBQyxFQUFFO29CQUNWLE9BQU8sSUFBSTtpQkFDWjthQUNGO1NBQ0YsQ0FBQztLQUNIO0lBQ0RDLFFBQVEsRUFBRWxCLGlFQUFvQztJQUM5Q29CLE9BQU8sRUFBRTtRQUNQSixHQUFHLEVBQUUsSUFBSTtRQUNUSyxNQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRTtLQUMxQjtJQUNEQyxTQUFTLEVBQUU7UUFDVCxNQUFNQyxRQUFRLEVBQUMsRUFBRUMsR0FBRyxHQUFFQyxPQUFPLEdBQUUsRUFBRTtZQUMvQixPQUFPQSxPQUFPLENBQUM7U0FDaEI7UUFDRCxNQUFNTCxPQUFPLEVBQUMsRUFBRUEsT0FBTyxHQUFFTSxLQUFLLEdBQUVYLElBQUksR0FBRSxFQUFFO1lBQ3RDSyxPQUFPLENBQUNKLEdBQUcsR0FBR1UsS0FBSyxDQUFDVixHQUFHLENBQUM7WUFDeEJJLE9BQU8sQ0FBQ08sRUFBRSxHQUFHRCxLQUFLLENBQUNDLEVBQUUsQ0FBQztZQUN0QixPQUFPQyxPQUFPLENBQUNDLE9BQU8sQ0FBQ1QsT0FBTyxDQUFDLENBQUM7U0FDakM7UUFDRCxNQUFNSixHQUFHLEVBQUMsRUFBRVUsS0FBSyxHQUFFWCxJQUFJLEdBQUVlLE9BQU8sR0FBRUMsT0FBTyxHQUFFQyxTQUFTLEdBQUUsRUFBRTtZQUN0RCxJQUFJakIsSUFBSSxFQUFFO2dCQUNSLElBQUlBLElBQUksQ0FBQ2tCLFFBQVEsSUFBSSxPQUFPLEVBQUU7b0JBQzVCUCxLQUFLLENBQUNWLEdBQUcsR0FBR0QsSUFBSSxDQUFDQyxHQUFHLENBQUM7b0JBQ3JCVSxLQUFLLENBQUNDLEVBQUUsR0FBR1osSUFBSSxDQUFDWSxFQUFFLENBQUM7aUJBQ3BCLE1BQU07b0JBQ0wsSUFBSTt3QkFDRixNQUFNTyxRQUFRLEdBQUcsTUFBTUMsS0FBSyxDQUFDLENBQUMsRUFBRW5DLHVCQUErQixDQUFDLFVBQVUsRUFBRThCLE9BQU8sQ0FBQ0csUUFBUSxDQUFDLHVCQUF1QixFQUFFSCxPQUFPLEVBQUVPLFlBQVksQ0FBQyxDQUFDLENBQzVJO3dCQUNELE1BQU0zQixJQUFJLEdBQUcsTUFBTXdCLFFBQVEsQ0FBQ0ksSUFBSSxFQUFFO3dCQUNsQ0MsT0FBTyxDQUFDQyxHQUFHLENBQUMsV0FBVyxHQUFHQyxJQUFJLENBQUNDLFNBQVMsQ0FBQ2hDLElBQUksQ0FBQyxDQUFDO3dCQUMvQ2dCLEtBQUssQ0FBQ1YsR0FBRyxHQUFHTixJQUFJLENBQUNNLEdBQUcsQ0FBQzt3QkFDckJVLEtBQUssQ0FBQ0MsRUFBRSxHQUFHakIsSUFBSSxDQUFDSyxJQUFJLENBQUNZLEVBQUUsQ0FBQztxQkFDekIsQ0FBQyxPQUFPZ0IsR0FBRyxFQUFFO3dCQUNaSixPQUFPLENBQUNDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBR0csR0FBRyxDQUFDO3FCQUNyQztpQkFDRjthQUNGO1lBQ0QsT0FBT2YsT0FBTyxDQUFDQyxPQUFPLENBQUNILEtBQUssQ0FBQyxDQUFDO1NBQy9CO0tBQ0Y7Q0FDRixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYnJpZGVsaW5rcy8uL3BhZ2VzL2FwaS9hdXRoL1suLi5uZXh0YXV0aF0uanM/NTI3ZiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTmV4dEF1dGggZnJvbSAnbmV4dC1hdXRoJztcclxuaW1wb3J0IENyZWRlbnRpYWxzUHJvdmlkZXIgZnJvbSAnbmV4dC1hdXRoL3Byb3ZpZGVycy9jcmVkZW50aWFscyc7XHJcbmltcG9ydCBGYWNlYm9va1Byb3ZpZGVyIGZyb20gJ25leHQtYXV0aC9wcm92aWRlcnMvZmFjZWJvb2snO1xyXG5pbXBvcnQgR29vZ2xlUHJvdmlkZXIgZnJvbSAnbmV4dC1hdXRoL3Byb3ZpZGVycy9nb29nbGUnO1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE5leHRBdXRoKHtcclxuICBwcm92aWRlcnM6IFtcclxuICAgIEdvb2dsZVByb3ZpZGVyKHtcclxuICAgICAgY2xpZW50SWQ6IHByb2Nlc3MuZW52LkdPT0dMRV9JRCxcclxuICAgICAgY2xpZW50U2VjcmV0OiBwcm9jZXNzLmVudi5HT09HTEVfU0VDUkVULFxyXG4gICAgfSksXHJcbiAgICBGYWNlYm9va1Byb3ZpZGVyKHtcclxuICAgICAgY2xpZW50SWQ6IHByb2Nlc3MuZW52LkZBQ0VCT09LX0lELFxyXG4gICAgICBjbGllbnRTZWNyZXQ6IHByb2Nlc3MuZW52LkZBQ0VCT09LX1NFQ1JFVFxyXG4gICAgfSksXHJcblxyXG4gICAgQ3JlZGVudGlhbHNQcm92aWRlcih7XHJcbiAgICAgIG5hbWU6ICdjcmVkZW50aWFscycsXHJcbiAgICAgIGFzeW5jIGF1dGhvcml6ZShjcmVkZW50aWFscykge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBjb25zdCB7IGRhdGEgfSA9IGF3YWl0IGF4aW9zLnBvc3QoJ2h0dHA6Ly9sb2NhbGhvc3Q6MTMzNy9hcGkvYXV0aC9sb2NhbCcsIHtcclxuICAgICAgICAgICAgaWRlbnRpZmllcjogY3JlZGVudGlhbHMuZW1haWwsXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiBjcmVkZW50aWFscy5wYXNzd29yZFxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBjb25zdCB1c2VyID0geyAuLi5kYXRhLnVzZXIsIGp3dDogZGF0YS5qd3QgfVxyXG5cclxuICAgICAgICAgIGlmICh1c2VyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB1c2VyO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgIHJldHVybiBudWxsXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KSxcclxuICBdLFxyXG4gIGRhdGFiYXNlOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19EQVRBQkFTRV9VUkwsXHJcbiAgc2Vzc2lvbjoge1xyXG4gICAgand0OiB0cnVlLFxyXG4gICAgbWF4QWdlOiAzMCAqIDI0ICogNjAgKiA2MCxcclxuICB9LFxyXG4gIGNhbGxiYWNrczoge1xyXG4gICAgYXN5bmMgcmVkaXJlY3QoeyB1cmwsIGJhc2VVcmwgfSkge1xyXG4gICAgICByZXR1cm4gYmFzZVVybDtcclxuICAgIH0sXHJcbiAgICBhc3luYyBzZXNzaW9uKHsgc2Vzc2lvbiwgdG9rZW4sIHVzZXIgfSkge1xyXG4gICAgICBzZXNzaW9uLmp3dCA9IHRva2VuLmp3dDtcclxuICAgICAgc2Vzc2lvbi5pZCA9IHRva2VuLmlkO1xyXG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHNlc3Npb24pO1xyXG4gICAgfSxcclxuICAgIGFzeW5jIGp3dCh7IHRva2VuLCB1c2VyLCBhY2NvdW50LCBwcm9maWxlLCBpc05ld1VzZXIgfSkge1xyXG4gICAgICBpZiAodXNlcikge1xyXG4gICAgICAgIGlmICh1c2VyLnByb3ZpZGVyID09IFwibG9jYWxcIikge1xyXG4gICAgICAgICAgdG9rZW4uand0ID0gdXNlci5qd3Q7XHJcbiAgICAgICAgICB0b2tlbi5pZCA9IHVzZXIuaWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZmV0Y2goYCR7cHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQVBJX1VSTH0vYXBpL2F1dGgvJHthY2NvdW50LnByb3ZpZGVyfS9jYWxsYmFjaz9hY2Nlc3NfdG9rZW49JHthY2NvdW50Py5hY2Nlc3NfdG9rZW59YFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gYXdhaXQgcmVzcG9uc2UuanNvbigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlRIRSBEQVRBIFwiICsgSlNPTi5zdHJpbmdpZnkoZGF0YSkpXHJcbiAgICAgICAgICAgIHRva2VuLmp3dCA9IGRhdGEuand0O1xyXG4gICAgICAgICAgICB0b2tlbi5pZCA9IGRhdGEudXNlci5pZDtcclxuICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlRIRSBFUlJPUiBIRVJFIFwiICsgZXJyKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHRva2VuKTtcclxuICAgIH1cclxuICB9XHJcbn0pXHJcbiJdLCJuYW1lcyI6WyJOZXh0QXV0aCIsIkNyZWRlbnRpYWxzUHJvdmlkZXIiLCJGYWNlYm9va1Byb3ZpZGVyIiwiR29vZ2xlUHJvdmlkZXIiLCJheGlvcyIsInByb3ZpZGVycyIsImNsaWVudElkIiwicHJvY2VzcyIsImVudiIsIkdPT0dMRV9JRCIsImNsaWVudFNlY3JldCIsIkdPT0dMRV9TRUNSRVQiLCJGQUNFQk9PS19JRCIsIkZBQ0VCT09LX1NFQ1JFVCIsIm5hbWUiLCJhdXRob3JpemUiLCJjcmVkZW50aWFscyIsImRhdGEiLCJwb3N0IiwiaWRlbnRpZmllciIsImVtYWlsIiwicGFzc3dvcmQiLCJ1c2VyIiwiand0IiwiZSIsImRhdGFiYXNlIiwiTkVYVF9QVUJMSUNfREFUQUJBU0VfVVJMIiwic2Vzc2lvbiIsIm1heEFnZSIsImNhbGxiYWNrcyIsInJlZGlyZWN0IiwidXJsIiwiYmFzZVVybCIsInRva2VuIiwiaWQiLCJQcm9taXNlIiwicmVzb2x2ZSIsImFjY291bnQiLCJwcm9maWxlIiwiaXNOZXdVc2VyIiwicHJvdmlkZXIiLCJyZXNwb25zZSIsImZldGNoIiwiTkVYVF9QVUJMSUNfQVBJX1VSTCIsImFjY2Vzc190b2tlbiIsImpzb24iLCJjb25zb2xlIiwibG9nIiwiSlNPTiIsInN0cmluZ2lmeSIsImVyciJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///(api)/./pages/api/auth/[...nextauth].js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("(api)/./pages/api/auth/[...nextauth].js"));
module.exports = __webpack_exports__;

})();