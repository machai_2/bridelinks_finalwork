import axios from 'axios';

export default async (req, res) => {
  axios
    .post('http://localhost:1337/api/auth/local', {
      identifier: req.body.email,
      password: req.body.password,
    })
    .then(response => {
      console.log('\x1b[33m', "[LOGIN]", "User succesfully authentication. " + response.data.user);
      res.status(200).send(response.data.user);
    })
    .catch(function (error) {
      if (error.response) {
        console.log("error data  " + JSON.stringify(error.response.data.error.name));
        console.log("error details  " + JSON.stringify(error.response.data.error.details.path));
        console.log("error status  " + error.response.status);
        console.log("error headers  " + JSON.stringify(error.response.headers));
        res.status(400).send(error.request);

      } else if (error.request) {
        console.log('error request :')
        res.status(400).send(error.request);
      } else {
        res.status(400).send(error);
      }

    })
}