import NavbarLeft from './../../navbars/navbarLeft';
import NavbarRight from './../../navbars/navbarRight';
import styles from './style.module.scss';

export default function DashboardLayout({ children }) {
  return (
    <>
      <NavbarLeft />
      <main className={styles.mainContent}>
        <div className={styles.frame}>{children}</div>
      </main>
      <NavbarRight />
    </>
  );
}
