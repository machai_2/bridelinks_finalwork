module.exports = {
  i18n: {
    defaultLocale: 'nl-NL',
    locales: ['nl-NL', 'fr'],
    localePath: "./locales",
  },
}