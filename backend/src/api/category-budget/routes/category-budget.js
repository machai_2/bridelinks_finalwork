'use strict';

/**
 * category-budget router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::category-budget.category-budget');
