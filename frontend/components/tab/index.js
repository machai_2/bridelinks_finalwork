import React, { useState } from 'react';
import TabPanel from '../tabHired';
import styles from './style.module.scss';
import { useSession, getSession } from 'next-auth/react';
import useSWR, { SWRConfig } from 'swr'
import TabHired from '../tabHired';
import TabFavorite from '../tabFavorite';
import { fetchAPI } from '../../lib/api';

export default function Tab() {
  const [countHired, setCountHired] = useState(1);
  const [countFavorite, setCountFavorite] = useState(1);

  const handleCountHired = (childData) => { setCountHired(childData) }
  const handleCountFavorite = (childData) => { setCountFavorite(childData) }

  const tabItems = [
    {
      id: 1,
      title: `hired (${countHired})`,
      content: <TabHired type="hired" valueHired={handleCountHired} />,
    },
    {
      id: 2,
      title: `favorites (${countFavorite})`,
      content: <TabFavorite type="favorite" valueFavorite={handleCountFavorite} />,
    },
  ];
  const [currentTab, setCurrentTab] = useState(1);

  return (
    <div className={styles.container}>
      <div className={styles.tabs}>
        {tabItems.map((tab, i) => (
          <button key={i} onClick={() => setCurrentTab(tab.id)} className={tab.id === currentTab ? `${styles.active}` : ''}>
            {tab.title}
          </button>
        ))}
      </div>

      {tabItems.map((tab, i) => {
        if (tab.id === currentTab) {
          return (
            <div key={i}>
              <div className={styles.content}>{tab.content}</div>
            </div>
          );
        } else {
          return null;
        }
      })}
    </div>
  );
}
