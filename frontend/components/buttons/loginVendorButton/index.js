import React, { useState } from 'react';
import { useRouter } from 'next/router';
import styles from './style.module.scss';
import { LoginVendorPopup } from '../../popup/loginVendorPopup';

export function LoginVendorButton() {
  const [OpenPopup, setOpenPopup] = useState(false);
  const router = useRouter()

  const showPopup = () => {
    setOpenPopup(!OpenPopup);
    if (OpenPopup == false) {
      router.replace('/vendors/landingspage', undefined, { shallow: true });
    }
    router.push({
      pathname: '/vendors/landingspage',
      query: { mode: 'login-vendor' }
    })
  };

  return (
    <div>
      <button onClick={showPopup} className={styles.login_btn}>
        Log in
      </button>
      {OpenPopup ? <LoginVendorPopup closePopup={showPopup} /> : null}
    </div>
  );
}
