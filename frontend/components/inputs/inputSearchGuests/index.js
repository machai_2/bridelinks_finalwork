import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TextField from '@mui/material/TextField';
import FeatherIcon from 'feather-icons-react';
import React from 'react';

import { InputStyle } from '../inputStyle';
import styles from './style.module.scss';

export function InputSearchGuests(props) {
  
  const classes = InputStyle();

  return (
    <div className={styles.container}>
      <FeatherIcon icon="search" size={'1.5rem'} />
      <TextField fullWidth value={props.searchQuery} onChange={props.onChange} className={classes.root} placeholder={props.children} variant="standard" />
    </div>
  );
}
