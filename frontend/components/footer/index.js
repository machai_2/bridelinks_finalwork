import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';

import { DownloadButton } from '../../components/buttons';
import styles from './style.module.scss';

export default function Footer() {
  return (
    <section className={styles.container}>
      <div className={styles.footer}>
        <div className={styles.logo}>
          <Image src="/Bridelinks/logo_xhrtcp.svg" layout="intrinsic" width={2200} height={290} alt="logo bridelinks" />
        </div>
        <div className={styles.column_footer}>
          <p className={styles.title_footer}>Follow us on</p>
          <div className={styles.icons_group}>
            <FontAwesomeIcon className={styles.icon_item} icon="fa-brands fa-facebook-square" size="xl" />
            <FontAwesomeIcon className={styles.icon_item} icon="fa-brands fa-instagram" size="xl" />
            <FontAwesomeIcon className={styles.icon_item} icon="fa-brands fa-twitter-square" size="xl" />
            <FontAwesomeIcon className={styles.icon_item} icon="fa-brands fa-pinterest-square" size="xl" />
          </div>
        </div>

        <div className={styles.column_footer}>
          <p className={styles.title_footer}>Download the application</p>
          <div className={styles.group_buttons}>
            <DownloadButton>Download on the appstore</DownloadButton>
            <DownloadButton>Download on the Playstore</DownloadButton>
          </div>
        </div>

        <div className={styles.column_footer}>
          <div className={styles.text_left}>
            <p className={styles.info_vendor}>Sign in right now to get in contact with your clients! start now</p>
          </div>
        </div>
      </div>
      <div className={styles.policy}>
        <p>
          Privacy policy
          <FontAwesomeIcon icon="fa-solid fa-copyright" className={styles.icon_copyright} />
          Bride & Links
        </p>
      </div>
    </section>
  );
}
