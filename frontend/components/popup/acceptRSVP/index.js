import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import axios from 'axios';
import { DashboardSolidButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { SelectDashboard } from '../../selects';
import { useRouter } from 'next/router';


export function AcceptRSVP(props) {
  const [clickedYes, setclickedYes] = useState();
  const [email, setEmail] = useState("");
  const [city, setCity] = useState("");
  const [zipcode, setZipcode] = useState("");
  const [address, setAdress] = useState("");
  const [phone, setPhone] = useState("");
  const [ageGroup, setAgeGroup] = useState("");
  const [diet, setDiet] = useState("None");
  const [firstnamePlus, setFirstnamePlus] = useState("");
  const [lastnamePlus, setLastnamePlus] = useState("");

  const router = useRouter();
  const { token } = router.query

  const handleSubmit = async (e) => {
    e.preventDefault();
    const user_data = {
      zipcode,
      address,
      city,
      phone_number: phone,
      email,
      age: ageGroup,
      diet: diet,
      firstnamePlus: firstnamePlus,
      lastnamePlus: lastnamePlus
    }
    axios.post(`http://localhost:3000/api/invitation/accepted?token=${token}`, {
      data: { user_data }
    }).then(function (response) {
      router.replace('/rsvp/accepted')
    }).catch(function (error) {
      console.log(error);
    })
  };

  const title = [
    {
      label: "Mrs",
      value: "Mrs",
    },
    {
      label: "Mr",
      value: "Mr",
    },
    {
      label: "Ms",
      value: "Ms",
    },
  ]
  const diets = [
    {
      label: "None",
      value: "None",
    },
    {
      label: "Vegetarian",
      value: "Vegetarian",
    },
    {
      label: "Pescatarian",

      value: "Pescatarian",
    },
    {
      value: "Hallal",
      label: "Hallal",

    },
    {
      label: "Vegan",
      value: "Vegan",
    },
    {
      label: "Kosher",
      value: "Kosher",
    }
  ]

  const age = [
    {
      label: "adult",
      value: "Adult",
    },
    {
      label: "child",
      value: "Child",
    },
    {
      label: "baby",
      value: "Baby",
    },
  ];

  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h2>Are you sure you want to <span>accept?</span></h2>
            <div className={styles.text_body}>Fill in your information so we can send you further information about the
              wedding as well as accomodate you for it.
            </div>

            <section className={styles.content}>

              <div className={styles.short_items}>
                <div className={styles.item}>
                  <label>Age group</label>
                  <SelectDashboard options={age} onChange={(e) => { setAgeGroup(e.target.value) }}></SelectDashboard>
                </div>
                <div className={styles.item}>
                  <label>Diet</label>
                  <SelectDashboard options={diets} onChange={(e) => { setDiet(e.target.value) }}></SelectDashboard>
                </div>
              </div>

              <div className={styles.short_items}>
                <div className={styles.item}> <label>Email</label>
                  <div className={styles.input}>

                    <InputForm type="email" placeholder="Email" onChange={(e) => { setEmail(e.target.value) }} />
                  </div>
                </div>
                <div className={styles.item}> <label>Phone number</label><InputForm type="text" placeholder="Phone number" onChange={(e) => { setPhone(e.target.value) }} /></div>
              </div>

              <div className={styles.short_items}>
                <div className={styles.item}> <label>Adress</label>                  <div className={styles.input}>
                  <InputForm placeholder="Adress" onChange={(e) => { setAdress(e.target.value) }} /></div></div>
                <div className={styles.item}> <label>City</label>                  <div className={styles.input}>
                  <InputForm placeholder="City" onChange={(e) => { setCity(e.target.value) }} /></div></div>
                <div className={styles.item}> <label>Zipcode</label>                  <div className={styles.zipcode}>
                  <InputForm placeholder="Zipcode" onChange={(e) => { setZipcode(e.target.value) }} />
                </div></div>
              </div>
            </section>

            <div className={styles.section2}>
              <div>

                <span>Do you plan on bringing a plus one:</span>
                <div className={styles.checkPlus}>
                  <input type="radio" value="true" onClick={(e) => setclickedYes(true)} checked={clickedYes == true} /><span>Yes</span>

                  <input type="radio" value="false" onClick={(e) => setclickedYes(false)} checked={clickedYes == false} /><span>No</span>
                </div>
              </div>           {
                clickedYes && (
                  <section className={styles.section3}>
                    <span>Fill in the firstname and name of your plus one</span>
                    <div className={styles.plusOne}>
                      <div className={styles.item}>
                        <label>Partner firstname</label>
                        <InputForm placeholder="firstname" onChange={(e) => { setFirstnamePlus(e.target.value) }}></InputForm>
                      </div>
                      <div className={styles.item}>
                        <label>Partner lastname</label>
                        <InputForm placeholder="lastname" onChange={(e) => { setLastnamePlus(e.target.value) }}></InputForm>
                      </div>
                    </div>
                  </section>
                )
              }
            </div>
            <div className={styles.submitClose}>
              <DashboardSolidButton type="submit">Submit</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div >
  );
}
