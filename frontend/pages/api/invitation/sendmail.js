import axios from 'axios';

const sendinblue = require("../sendinblue");

export default async (req, res) => {
  console.log('THE PARAMS  ' + JSON.stringify(req.body))

  try {
    let sendSmtpEmail = {
      to: [{
        email: req.body.email //TODO: Change this on production.
      }],

      templateId: 2,
      params: {
        name: req.body.name,
        partner: req.body.partner,
        date: req.body.date,
        location: req.body.location,
        subject: "wedding invitation",
        urlToken: req.body.urlToken

      },

    };

    sendinblue(sendSmtpEmail)
    res.status(200).end();
  } catch (err) {
    console.error(err);
    res.status(400).send(err);

  }
};