import styles from './style.module.scss';

export function DeleteButton( props ) {
  return <button id={props.id} className={styles.dashboard_button} type={props.type} onClick={props.onClick}>{props.children}</button>;
}
