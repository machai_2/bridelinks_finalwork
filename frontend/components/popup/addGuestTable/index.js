import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState, useEffect } from 'react';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputFilter, InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useSession } from 'next-auth/react';
import useSWR from 'swr'
import { useRouter } from 'next/router';
import { AddGuestPopup } from '../addGuestPopup';


export function AddGuestTable(props) {
  const router = useRouter();

  const { data: session } = useSession();
  const [selectedGuest, setSelectedGuest] = useState([""])
  const [OpenGuestPopup, setOpenGuestPopup] = useState(false);
  const [guestId, setGuestId] = useState();

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: Guests, error: GuestsError, mutate: mutateList } = useSWR(session ? `http://localhost:1337/api/guest-lists?filters[wedding_id]=${props.weddingId}` : null,
    fetcher)
  const { data: Groups, error: GroupsError } = useSWR(session ? `http://localhost:1337/api/guest-groups?user_id=${session.id}` : null,
    fetcher)

  const showAddGuest = () => {
    setOpenGuestPopup(!OpenGuestPopup);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    props.closePopup()
    const data = {
      guest_id: guestId,
      table_id: props.tableId
    }

    try {
      const response = await fetch('http://localhost:1337/api/table-guests/', {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data })

      })
      console.log(response);
    } catch (error) {
      console.log(error)
    }
    router.reload(window.location.pathname)

    props.closePopup()

  }


  const filter_guest = Guests?.data?.map(item => (`${item.attributes.firstname} ${item.attributes.lastname}`));
  useEffect(() => {
    if (Guests) {
      const filterGuest = Guests?.data?.filter(item => (`${item.attributes.firstname} ${item.attributes.lastname}` == selectedGuest));
      if (filterGuest[0]) {

        setGuestId(filterGuest[0].id)
      }
    }
  }, [selectedGuest]);

  if (GuestsError) <p>Loading failed...</p>;
  if (Guests) <h1>Loading...</h1>;
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h4>Add guest to tables</h4>
            <section>

              <InputFilter type="text" list={filter_guest} placeholder="Add a guest to a table" onchange={(event, value) => setSelectedGuest(value)} />
              <div className={styles.addGuest}>
                {/* <p>if you can't find your guest</p>
              <button onClick={showAddGuest}>add a new guest</button> */}
                {OpenGuestPopup ? <AddGuestPopup id={props.weddingId} mutate={mutateList} list={Groups} closePopup={showAddGuest} /> : null}
              </div>
            </section>
            <div className={styles.submitClose}>
              <DashboardSolidButton type="submit" >Save</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

