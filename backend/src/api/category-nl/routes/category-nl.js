'use strict';

/**
 * category-nl router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::category-nl.category-nl');
