import { InputCheckbox } from '../../../components/inputs/inputCheckbox';
import DashboardLayout from '../../../components/layout/dashboard';
import { DashboardOutlineButton, DashboardSolidButton } from './../../../components/buttons';
import styles from './style.module.scss';
import useSWR, { useSWRConfig } from 'swr'
import { useEffect, useState } from "react";
import { useSession } from 'next-auth/react';
import { AddTaskPopup, ResetPopup, DeleteChecklistPopup, EditTaskPopup } from '../../../components/popup';
import { SortChecklistDropdown } from '../../../components/dropdowns/sortChecklistDropdown';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { SpinnerCircularFixed } from 'spinners-react';

export default function Checklist() {
  const { data: session } = useSession();
  const [isAddTask, setIsAddTask] = useState(false);
  const [isDeleteTask, setIsDeleteTask] = useState(false);
  const [isResetTasks, setIsResetTasks] = useState(false);
  const [OpenEditChecklist, setOpenEditChecklist] = useState(false);
  const [taskDone, setTaskDone] = useState(0);
  const [taskOngoing, setTaskOngoing] = useState(0);
  const [loading, setLoading] = useState(false);
  const [dropdownFilter, setDropdownFilter] = useState('all');
  const [results, setResults] = useState();
  const [idItem, setIdItem] = useState();

  if (loading) return <SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} />


  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
  ];
  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: checklist, error: error, mutate: mutateList } = useSWR(session ? `http://localhost:1337/api/checklists?pagination[pageSize]=100&filters[user_id]=${session.id}&populate=*` : null,
    fetcher, { refreshInterval: 3000 })


  if (error) <p>Loading failed...</p>;
  if (checklist) <h1>Loading...</h1>;


  const showReset = () => { setIsResetTasks(!isResetTasks); };
  const ShowAddTask = () => { setIsAddTask(!isAddTask); };
  const showDeleteTask = (id) => {
    setIdItem(id)
    setIsDeleteTask(!isDeleteTask);
  };

  const showEditPopup = (id) => {
    setIdItem(id)
    setOpenEditChecklist(!OpenEditChecklist);
  };

  function MonthsFromNow(year, month) {
    return month - new Date().getMonth() + (12 * (year - new Date().getFullYear()))
  }

  const handleChecklist = (childData) => { setDropdownFilter(childData) }

  const sortDates = (list) => {
    var tempSortedDates = {};
    if (list) {
      var sorted = list.sort(function (a, b) {
        return new Date(a.attributes.period) - new Date(b.attributes.period);
      });

      sorted?.forEach((item) => {
        var year = new Date(item.attributes.period).getFullYear();
        var month = new Date(item.attributes.period).getMonth();
        if (typeof tempSortedDates[year] === "undefined") {
          tempSortedDates[year] = {};
        }
        if (typeof tempSortedDates[year][month] === "undefined") {
          tempSortedDates[year][month] = [];
        }

        const arr = tempSortedDates[year][month].sort(function (a, b) {
          return b.attributes.period - a.attributes.period
        });
        arr.push(item);
      });

      return tempSortedDates;
    };
  }
  function monthTitle(year, month) {
    console.log('the month title ' + MonthsFromNow(year, month))
    if (MonthsFromNow(year, month) == 0) {
      return 'this month ';
    }
    if (MonthsFromNow(year, month) == -1) {
      return 'after the wedding '
    }
    return `${MonthsFromNow(year, month)} months`
  }

  function formatDate(date) {
    const newDate = new Date(date)
    const monthString = newDate.toLocaleString('en-GB', {
      month: 'short',
    }).toLowerCase();;
    return `${newDate.getDate()} ${monthString} ${newDate.getFullYear()}`
  }

  useEffect(() => {
    setLoading(true);
    let updatedList = checklist;

    if (dropdownFilter == 'all') {
      updatedList = updatedList?.data
    }
    else if (dropdownFilter == 'done') {
      updatedList = updatedList?.data?.filter((item) => {
        return item.attributes.done == true
      })
      console.log('here is the done   ' + updatedList.length)
      setTaskDone(updatedList.length)
    }
    else if (dropdownFilter == 'todo') {
      updatedList = updatedList?.data?.filter((item) => {
        return item.attributes.done == false
      })
      setTaskOngoing(updatedList.length)
    }
    if (checklist) {
      const isDone = checklist && checklist?.data?.filter((item) => {
        return item && item.attributes.done == true
      })
      setTaskDone((isDone || []).length)
      const notDone = checklist && checklist?.data?.filter((item) => {
        return item && item.attributes.done == false
      })
      setTaskOngoing((notDone || []).length)
    }
    const list = sortDates(updatedList)
    setResults(list);
    setLoading(false);

  }, [checklist, dropdownFilter]);

  async function checkInput(e, id) {
    const newChecklist = { done: e }
    try {
      await fetch(`http://localhost:1337/api/checklists/${id}`, {
        method: 'PUT',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: newChecklist })
      })
    } catch (error) {
      console.log(error)
    }
  }


  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.title_dashboard}>CHECKLIST WEDDING</div>
        <p>Complet your tasks to get closer to our wedding ! Do it now !</p>
      </div>
      <div className={styles.buttons_top}>
        <div className={styles.progress}>
          <span>{`You have completed ${taskDone} out of ${checklist?.data?.length} tasks`}</span>
        </div>
        <div className={styles.buttonsGroup}>
          <DashboardSolidButton onClick={ShowAddTask}>add a task</DashboardSolidButton>
          <DashboardOutlineButton onClick={showReset}>reset tasks</DashboardOutlineButton>
          {isResetTasks ? <ResetPopup closePopup={showReset} mutate={mutateList} /> : null}
          <SortChecklistDropdown valueDropdown={handleChecklist} />
        </div>
      </div>
      <div className={styles.main_content}>
        {results && Object.keys(results).sort((a, b) => b - a).map((year) => (
          Object.keys(results[year]).sort((a, b) => b - a).map((month) => (
            <div key={year + month}>
              <section className={styles.container_section} key={year + month}>
                <span className={styles.title_section}>{`${monthNames[month]}: ${monthTitle(year, month)}`}</span>
                {results[year][month].map((e, i) => (
                  <div className={e.attributes.done ? `${styles.card_active}` : `${styles.card_todo}`} key={e.id} >
                    <div className={styles.column1} onClick={() => showEditPopup(e.id)}>
                      <div className={styles.checkbox_container} >
                        <InputCheckbox text={`${e.attributes.title}`} status={e.attributes.done} id={e.id} onChange={(a) => { checkInput(a.target.checked, e.id) }} />
                      </div>
                      <div className={styles.card_bottom}>
                        <div className={styles.card_date}>
                          <FontAwesomeIcon icon="fa-solid fa-calendar" />
                          {formatDate(e.attributes.period)}
                        </div>
                        <div id={styles.tag}>{e.attributes.tag}</div>
                      </div>
                    </div>
                    <div className={styles.column2}>
                      <button onClick={() => showDeleteTask(e.id)} className={styles.deleteTask}>
                        <FontAwesomeIcon icon="fa-solid fa-trash" />
                      </button>
                    </div>
                  </div>
                ))}

              </section>
            </div>

          ))))}
        {isAddTask ? <AddTaskPopup closePopup={ShowAddTask} mutate={mutateList} /> : null}
        {OpenEditChecklist ? <EditTaskPopup closePopup={showEditPopup} id={idItem} /> : null}
        {isDeleteTask ? <DeleteChecklistPopup id={idItem} closePopup={showDeleteTask} mutate={mutateList} /> : null}
      </div>
    </div>
  );
}


Checklist.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
