import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import {RegisterContent} from '../registerContent';
import {LoginContent} from '../loginContent';
import { ForgotContent } from '../forgotContent';


export function RegisterPopup(props) {
  let comp;
  const router = useRouter();

  if (router.query.mode == 'forgot') {
    comp=<ForgotContent/>
  }
 else if (router.query.mode == 'login') {
    comp=<LoginContent/>
  }else{
    comp=<RegisterContent/>
  }
  
  
  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
     {comp}
          </div>
        </div>
      </div>
    </>
  );
}
