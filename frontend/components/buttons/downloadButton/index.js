import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import styles from './style.module.scss';

export function DownloadButton({ children, icon, size,onclick }) {
  return (
    <button className={styles.download_btn} onClick={onclick}>
      <div className={styles.button_icon}>{icon && <FontAwesomeIcon icon={icon} size={size} />}</div>
      <div className={styles.button_text}>{children}</div>
    </button>
  );
}
