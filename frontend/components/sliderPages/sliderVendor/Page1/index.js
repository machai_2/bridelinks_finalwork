import { InputForm } from '../../../inputs';
import styles from './style.module.scss';
import React from 'react';


export function Page1({ data, setData }) {

  return (
    <div className={styles.container}>
      <div>
        <h2>Tell us more about yourself!</h2>
        <div className={styles.content_container}>
          <section>
            <span>My name is </span>
            <div className={styles.inputForm}>
              <InputForm type='text' value={data.firstname} onChange={(e) => { setData({ ...data, firstname: e.target.value }) }} placeholder="firstname" />
            </div>
            <div className={styles.inputForm}>
              <InputForm type='text' value={data.lastname} onChange={(e) => { setData({ ...data, lastname: e.target.value }) }} placeholder="lastname" />
            </div>
          </section>
        </div>
      </div>
    </div>
  )
}

