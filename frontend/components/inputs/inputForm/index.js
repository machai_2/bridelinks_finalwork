import TextField from '@mui/material/TextField';
import React from 'react';
import { InputStyle } from './../inputStyle';
import styles from './style.module.scss';

export function InputForm(props) {
  const classes = InputStyle();

  return (
    <div className={styles.container}>
      <TextField
        fullWidth
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        className={classes.root}
        variant="standard"
        onChange={props.onChange}
        id={props.id}
        type={props.type}
      />
    </div>
  );
}
