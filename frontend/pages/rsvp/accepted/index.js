
import React, { useState, useEffect } from 'react';
import styles from './style.module.scss';

export default function Refused() {
  return (
    <div className={styles.container}>
      <section>
        You have <span>accepted</span> this invitation, thank you for answering!
      </section>
    </div>
  );
}
