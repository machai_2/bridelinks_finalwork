import Link from 'next/link';
import { useSession } from 'next-auth/react';
import React, { useState, useEffect } from 'react';
import { UserDropdown } from '../dropdowns';
import styles from './style.module.scss';
import { LoginButton, RegisterButton } from '../buttons';


export default function Header() {
  const { data: session } = useSession();

  return (
    <div className={styles.navbar_container}>
      <nav className={styles.navbar_main}>
        <ul>
          <div className={styles.nav_links}>
            <li>
              <Link href={'/#Home'} scroll={true}>
                Home
              </Link>
            </li>

            <li>
              <Link href="/vendors" replace>
                <a>Find vendors</a>
              </Link>
            </li>
            <li>
              <Link href={'/#Features'} scroll={true}>
                Features
              </Link>
            </li>

            <li>
              <Link href={'/#Download'} scroll={true}>
                Download
              </Link>
            </li>
            <div className={styles.logo_area}>
              <Link href={'/#Home'}>
                <a>
                  <div>
                    <span className={styles.logo}>
                      Bride <span>&</span> Links
                    </span>
                  </div>
                  <div>
                    <span className={styles.slogan}>Decide plan create your special day</span>
                  </div>
                </a>
              </Link>
            </div>
            <div className={styles.r_nav}>
              {!session && <>
                <div className={styles.r_item}>
                  <LoginButton />
                </div>
                <div className={styles.r_item}>
                  <RegisterButton />
                </div>
              </>
              }
              {session && <>
                <UserDropdown />
              </>
              }
            </div>
          </div>
        </ul>
      </nav>
    </div>
  );
}

