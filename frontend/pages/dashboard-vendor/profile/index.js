import { useSession, getSession } from 'next-auth/react';
import { useEffect, useState } from "react";
import VendorLayout from './../../../components/layout/vendor';
import styles from './style.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FeatherIcon from 'feather-icons-react';
import { DashboardOutlineButton, DashboardSolidButton } from '../../../components/buttons';
import { InputForm, InputFilter } from '../../../components/inputs';
import { Input } from '@mui/material';

export default function Profile() {
  const [thumbnail, setThumbnail] = useState(null);
  const [urlThumbail, setUrlThumbnail] = useState(null);

  const selectThumbail = (event) => {
    setThumbnail(event.target.files);
  };
  const uploadImage = () => {
    fetch("https://api.cloudinary.com/v1_1/breellz/image/upload", {
      method: "post",
      body: data
    })
      .then(resp => resp.json())
      .then(data => {
        setUrlThumbnail(data.url)
      })
  }
  return (
    <div className={styles.container}>
      <section>
        <div className={styles.cadre}>
          <h4>Thumbnail picture</h4>
          <hr />
          <div>
            <span className={styles.exp}>upload a thumbnail picture of your services 200 x 200</span>
            <div className={styles.content_upload}>
              <input type="file" onChange={(e) => setThumbnail(e.target.files[0])} />
              <DashboardOutlineButton>Upload</DashboardOutlineButton>
            </div>
          </div>
        </div>
      </section >
      <section>
        <div className={styles.cadre}>
          <h4>Personal information</h4>
          <hr />
          <div className={styles.personal_cadre}>
            <div>
              <label>Firstname</label>
              <InputFilter type="normal" placeholder="Firstname"></InputFilter>
            </div>
            <div>
              <label>Lastname</label>
              <InputFilter type="normal" placeholder="lastname"></InputFilter>
            </div>
          </div>
        </div>
      </section >
      <section>
        <div className={styles.cadre}>
          <h4>Business information</h4>
          <hr />
          <div className={styles.cadre_content}>
            <div>
              <label>Your business name</label>
              <InputFilter placeholder="business name"></InputFilter>
            </div>
            <div>
              <label>Address</label>
              <InputFilter placeholder="street"></InputFilter>
            </div>
            <div>
              <label>zipcode</label>
              <InputFilter placeholder="street"></InputFilter>
            </div>
            <div>
              <label>city</label>
              <InputFilter placeholder="street"></InputFilter>
            </div>
          </div>
        </div>

      </section>
      <section>
        <div className={styles.cadre}>
          <h4>Pricing</h4>
          <hr />
          <div className={styles.cadre_content}>
            <InputFilter placeholder="starting price"></InputFilter>
          </div>
        </div>
      </section>
      <section>
        <div className={styles.cadre}>
          <h4>contact information</h4>
          <hr />
          <div className={styles.cadre_content}>

            <div>
              <label>email</label>
              <InputFilter placeholder="email"></InputFilter>
            </div>
            <div>
              <label>home phone</label>
              <InputFilter placeholder="home phone"></InputFilter>
            </div>
            <div>
              <label>mobile phone</label>
              <InputFilter placeholder="mobile phone"></InputFilter>
            </div>
          </div>
        </div>
      </section>
      <div>
        <DashboardSolidButton>Save changes</DashboardSolidButton>
      </div>
    </div >
  )



}


Profile.getLayout = function getLayout(page) {
  return <VendorLayout>{page}</VendorLayout>;
};
