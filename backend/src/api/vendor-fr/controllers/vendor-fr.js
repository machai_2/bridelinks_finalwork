'use strict';

/**
 *  vendor-fr controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::vendor-fr.vendor-fr');
