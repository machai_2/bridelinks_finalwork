'use strict';

/**
 * guest-group router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::guest-group.guest-group');
