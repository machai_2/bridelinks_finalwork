'use strict';

/**
 * guest-group service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::guest-group.guest-group');
