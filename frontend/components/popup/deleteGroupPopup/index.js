import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { DeleteButton, CancelButton } from '../../buttons';
import axios from 'axios';


export function DeleteGroupPopup(props) {

  console.log('group id ' + props.groupId)
  const deleteTasks = () => {
    axios.delete(`http://localhost:1337/api/guest-groups/${props.groupId}`).then(
    ).then(response => {
      console.log('RESPONSE CREATE ' + response)
      props.mutate()
    }).catch(function (error) {
      console.log(error);
    });
    props.closePopup()
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            <h5>Are you sure you want to delete the group ?</h5>
            <span>All guest information will be deleted.</span><br />
            <span>This cannot be undone</span>
            <div className={styles.buttonGroup}>
              <CancelButton onClick={props.closePopup}>Cancel</CancelButton>
              <DeleteButton onClick={deleteTasks}>Delete</DeleteButton>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
