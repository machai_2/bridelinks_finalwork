'use strict';

/**
 * category-fr router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::category-fr.category-fr');
