
import axios from 'axios';
import Guests from '../../dashboard/guests';
const crypto = require("crypto");

export default async (req, res) => {
  const { firstname, lastname, wedding_id, guest_id, email, user_id, phone_number, title, age, sendEmail, address, city, zipcode, sendPhone, diet, group } = req.body.data.user_data;

  axios.get(`http://localhost:1337/api/guest-groups?filters[name][$eq]=${group}&filters[user_id]=${user_id}&populate=*`)
    .then((response) => { addGuest(response.data) }).catch((err) => console.log('error ' + err))


  const addGuest = async (group) => {
    const guest_data = {
      "diet": diet,
      "firstname": firstname,
      "lastname": lastname,
      "title": title,
      "age": age,
      "email": email,
      "phone_number": phone_number,
      "street": address,
      "city": city,
      "zipcode": zipcode,
      "wedding_id": wedding_id,
      "group_id": group.data[0].id
    }

    var config = {
      method: 'PUT',
      url: `http://localhost:1337/api/guest-lists/${guest_id}`,
      headers: { 'Content-Type': 'application/json' },
      data: JSON.stringify({ data: guest_data })
    };

    await axios(config).then(function (response) {
      console.log('\x1b[33m', "[GUEST]", "Guest succesfully registered.");
      createInvitation(guest_id);
    }).catch(function (error) { console.log(error); });

  }
  async function createInvitation(guestData) {
    console.log("[INVITATION]", "Creating invitation..." + JSON.stringify(guest_id));
    const guestToken = {
      token: crypto.randomBytes(16).toString("hex"),
      expired: new Date(Date.now() + (3600 * 1000 * 24)),
      read: false,
      status: "PENDING",
    };
    axios.put(`http://localhost:1337/api/invitations?filters[guest_id]=${guest_id}`, {
      data:
      {
        status: "pending",
        read: false,
        wedding_id: wedding_id,
        guest_id: guest_id,
        token: guestToken.token,
        expired: guestToken.expired
      }
    }).catch((error) => { console.log(error); });


    if (sendEmail && email) {
      try {
        const response = await fetch('http://localhost:3000/api/invitation/sendmail', {
          method: 'POST',
          mode: 'cors',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ name: `${firstname} ${lastname}`, email: email, urlToken: `http://localhost:3000/rsvp?token=${guestToken.token}` })

        })
        console.log(response);
      } catch (error) { console.log("send an email error " + JSON.stringify(error)) }
    }

    if (sendPhone && guestData.phone_number) {
      let SMSBody = `Hello ${guestData.firstname} ${guestData.lastname}, we have the honor to invite you to our wedding that will be celebrated on the 21 of August 2023, at 17pm on rue la Croix Rouge 4020 Liège. Click on the link to see
    mure information as well as fill in your data: http://localhost:3000/rsvp?token=${guestToken.token}`;
      try {
        const response = await fetch('http://localhost:3000/api/invitation/sendsms', {
          method: 'POST',
          mode: 'cors',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ number: phone_number, txt: SMSBody })

        })
        console.log(response);
      } catch (error) { console.log(error) }

    }
  }

}