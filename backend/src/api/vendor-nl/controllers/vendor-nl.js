'use strict';

/**
 *  vendor-nl controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::vendor-nl.vendor-nl');
