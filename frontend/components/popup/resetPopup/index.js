import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import { DeleteButton, CancelButton } from '../../buttons';
import axios from 'axios';
import { signIn, signOut, useSession } from 'next-auth/react';


export function ResetPopup(props) {
  const { data: session } = useSession();


  const deleteTasks = () => {
    axios.get(`http://localhost:1337/api/checklists?filters[user_id]=${session.id}&populate=*`)
      .then((response) => {
        console.log('get them all ' + response)
        response.data.data.forEach(function (item) {
          console.log("found id: ", item.id)

          axios.delete(`http://localhost:1337/api/checklists/${item.id}`)
            .then((response) => {
              console.log(response)
              props.mutate(`http://localhost:1337/api/checklists/${item.id}`)
            })
        }).catch((err) => console.log(err.response))

      }).catch((err) => console.log(err.response))
    props.closePopup()
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            <h3>Reset all tasks?</h3>
            <span>Are you sure you want to reset your checklist? <br/>If you press yes, ALL tasks will be cleared.</span>
            <div className={styles.buttonGroup}>
              <CancelButton>Cancel</CancelButton>
              <DeleteButton onClick={deleteTasks}>reset</DeleteButton>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
