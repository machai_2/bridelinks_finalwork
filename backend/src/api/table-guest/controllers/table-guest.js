'use strict';

/**
 *  table-guest controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::table-guest.table-guest');
