import React, { useState } from 'react';
import styles from './style.module.scss'
import { useRouter } from 'next/router';
import { RegisterVendorPopup } from '../../popup/registerVendorPopup';


export function RegisterVendorButton() {
  const [OpenPopup, setOpenPopup] = useState(false);
  const router = useRouter();

  const showPopup = () => {
    setOpenPopup(!OpenPopup);
    router.push({
      pathname: '/vendors/landingspage',
      query: { mode: 'register-vendor' }
    })
  };

  return (
    <div>
      <button onClick={showPopup} className={styles.register_btn} >Join us</button>
      {OpenPopup ? <RegisterVendorPopup closePopup={showPopup} /> : null}
    </div>
  );
}
