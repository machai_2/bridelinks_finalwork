export const data = [
  { name: "The grooms family" },
  { name: "The bride's family" },
  { name: "The grooms friends" },
  { name: "The bride's friends" },
  { name: "The grooms co-workers" },
  { name: "The bride's co-workers" },
  { name: "Mutual friends" }
]