import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import FacebookProvider from 'next-auth/providers/facebook';
import GoogleProvider from 'next-auth/providers/google';
import axios from 'axios';


export default NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_SECRET,
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET
    }),

    CredentialsProvider({
      name: 'credentials',
      async authorize(credentials) {
        try {
          const { data } = await axios.post('http://localhost:1337/api/auth/local', {
            identifier: credentials.email,
            password: credentials.password
          });
          const user = { ...data.user, jwt: data.jwt }

          if (user) {
            return user;
          }
          else {
            return null;
          }
        } catch (e) {
          return null
        }
      }
    }),
  ],
  database: process.env.NEXT_PUBLIC_DATABASE_URL,
  session: {
    jwt: true,
    maxAge: 30 * 24 * 60 * 60,
  },
  callbacks: {
    async redirect({ url, baseUrl }) {
      return baseUrl;
    },
    async session({ session, token, user }) {
      session.jwt = token.jwt;
      session.id = token.id;
      return Promise.resolve(session);
    },
    async jwt({ token, user, account, profile, isNewUser }) {
      if (user) {
        if (user.provider == "local") {
          token.jwt = user.jwt;
          token.id = user.id;
        } else {
          try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/auth/${account.provider}/callback?access_token=${account?.access_token}`
            );
            const data = await response.json();
            console.log("THE DATA " + JSON.stringify(data))
            token.jwt = data.jwt;
            token.id = data.user.id;
          } catch (err) {
            console.log("THE ERROR HERE " + err)
          }
        }
      }
      return Promise.resolve(token);
    }
  }
})
