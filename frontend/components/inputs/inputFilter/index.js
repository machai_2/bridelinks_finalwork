import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import React from 'react';
import { InputStyle } from './inputStyle'
import styles from './style.module.scss';

export function InputFilter(props) {

  const classes = InputStyle();
  if (props.type == "comment") {
    return (
      <div className={styles.comment}>
        <TextField
          fullWidth
          className={classes.root}
          id="input-comment"
          multiline
          value={props.value}
          placeholder={props.placeholder}
          rows={props.row}
          onChange={props.onChange}
          defaultValue={props.defaultValue}
        />    </div>

    )
  }
  if (props.type == 'text') {
    return (
      <div className={styles.dropdownMenu}>
        <Autocomplete
          options={props.list}
          disableListWrap
          autoHighlight={true}
          inputValue={props.inputValue}
          onChange={props.onchange}
          autoComplete={true}
          value={props.value}
          id="input-filter"
          popupIcon={<FontAwesomeIcon size="2xs" icon="fa-solid fa-chevron-down" />}
          renderInput={(params) => (
            <TextField
              {...params}
              className={classes.root}
              type={props.type}
              placeholder={props.placeholder}
            />
          )}
        />
      </div>
    );
  } else {
    return (
      <div className={styles.normal}>
        <TextField fullWidth className={classes.root} onChange={props.onchange} type={props.type} placeholder={props.placeholder} />
      </div>
    );
  }
}
