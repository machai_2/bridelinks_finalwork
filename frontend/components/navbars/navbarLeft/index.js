import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { useState } from 'react';
import useSWR from 'swr'

import styles from './style.module.scss';

export const menu = [
  { title: 'Home', icon: 'fa-house', path: '/dashboard/main' },
  { title: 'Checklist', icon: 'fa-list-check', path: '/dashboard/checklist' },
  { title: 'Guestlist', icon: 'fa-user-group', path: '/dashboard/guests' },
  { title: 'Budget', icon: 'fa-calculator', path: '/dashboard/budget' },
  { title: 'Table plan', icon: 'fa-chair', path: '/dashboard/table' },
  { title: 'Vendors', icon: 'fa-shop', path: '/dashboard/vendors' },
];


export default function NavbarLeft() {

  return (
    <div className={styles.container}>
      <div className={styles.logo}> <Link href='/'>&</Link></div>

      <ul>
        {menu.map((item, index) => (
          <li key={index} className={styles.item}>
            <Link href={item.path}>
              <a>
                <div className={styles.icon}>
                  <FontAwesomeIcon size="lg" icon={item.icon} />
                </div>

                <span>{item.title}</span>
              </a>
            </Link>
          </li>
        ))}
        <li className={styles.settings}>
          <Link href='/'>
            <a>
              <FontAwesomeIcon size="lg" icon="fa-solid fa-arrow-right-from-bracket" />
            </a>
          </Link>
        </li>
      </ul>
    </div>
  );
}
