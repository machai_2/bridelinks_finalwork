
import styles from './style.module.scss';
import React from 'react';
import { InputForm, InputSearchVendors } from './../../../inputs';

export function Page3({ data, setData, list }) {
  return (
    <div className={styles.container}>
      <h2>What type of vendor are you ?</h2>
      <div>
        <div className={styles.content_container}>
          <section>
            <InputSearchVendors list={list} onchange={(event, value) => setData({ ...data, category: value })}>City</InputSearchVendors>

          </section>
        </div>
      </div>
    </div>
  )
}