const { i18n } = require('./next-i18next.config');


/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    loader: 'cloudinary',
    path: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/',
    formats: ["image/webp"],
  },
};

module.exports = nextConfig;
