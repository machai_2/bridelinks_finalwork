'use strict';

/**
 *  category-nl controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::category-nl.category-nl');
