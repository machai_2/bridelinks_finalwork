export const data = [
  {
    title: 'Checklist',
    icon: 'check-circle',
    description: 'Get the ultimate Wedding Checklist to make sure everything gets done.',
  },
  {
    title: 'Guestlist',
    icon: 'users',
    description: 'Easily track invitations, RSVPs, and guest info all in one place.',
  },
  {
    title: 'Budget',
    icon: 'dollar-sign',
    description: 'Dont worry about going over budget customize the numbers, track your spending, and schedule payments',
  },
  {
    title: 'Vendor management',
    icon: 'shopping-cart',
    description: 'Get the ultimate Wedding Checklist to make sure everything gets done.',
  },
  {
    title: 'Table plan',
    icon: 'user-check',
    description: 'Keep track of the table and seats as well as the guests assigned to them.',
  },
  {
    title: 'Invitation management',
    icon: 'mail',
    description:
      'Send your invitation automatically through mail and text message and customize the style of your invitations!',
  },
];
