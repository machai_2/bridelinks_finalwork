export const data = [
  //1year before
  { title: "Announce your engagement", tag: "engagement", year: 0, month: 12, day: 2 },
  { title: "Settle on a budget", tag: "pre-wedding", year: 0, month: 12, day: 5 },
  { title: "Research venues", tag: "pre-wedding", year: 0, month: 12, day: 8 },
  { title: "Start writing your guest list", tag: "pre-wedding",year: 0, month: 12, day: 23 },
  { title: "Plan an engagement party", tag: "engagement",year: 0, month: 12, day: 26 },
  { title: "Hire a photographer", tag: "engagement", year: 0, month: 12, day: 29 },
  { title: "Buy a wedding ring", tag: "engagement", year: 0, month: 12, day: 30 },

  //9month
  { title: "Chooses the bridesmaid and bridesmen attire", tag: "pre-wedding", year: 0, month: 10, day: 2 },
  { title: "Start planning the honeymoon", tag: "pre-wedding", year: 0, month: 10, day: 3 },
  { title: "Research and book music/DJ/entertainers", tag: "pre-wedding", year: 0, month: 10, day: 18 },
  { title: "Book transportation to and from a venue  ", tag: "pre-wedding", year: 0, month: 10, day: 24 },
  { title: "Research florists", tag: "pre-wedding", year: 0, month: 11, day: 3 },
  { title: "Decide on a wedding cake", tag: "pre-wedding", year: 0, month: 10, day: 28 },
  { title: "book a venue or hotel ", tag: "pre-wedding", year: 0, month: 9, day: 26 },

  { title: "Book a florist", tag: "pre-wedding", year: 0, month: 9, day: 9 },
  { title: "Book a caterer and create a menu", tag: "pre-wedding", year: 0, month: 9, day: 7 },
  { title: "Start looking for videographer", tag: "pre-wedding", year: 0, month: 9, day: 2 },

  //6-5month

  { title: "Book a make up artist and hairdresser", tag: "pre-wedding", year: 0, month: 6, day: 3 },
  { title: "Have the wedding dress altered", tag: "pre-wedding", year: 0, month: 5, day: 28 },
  { title: "Book honeymoon flights and hotels", tag: "pre-wedding", year: 0, month: 5, day: 18 },
  { title: "Purchase wedding decore and accessories", tag: "pre-wedding", year: 0, month: 5, day: 7 },
  { title: "Order reheasal dinner invitations", tag: "pre-wedding", year: 0, month: 5, day: 2 },

  //3_4month
  { title: "Mail invitations", tag: "pre-wedding", year: 0, month: 4, day: 2 },
  { title: "Write wedding vows", tag: "pre-wedding", year: 0, month: 4, day: 16 },
  { title: "Finalize seating arrangement", tag: "pre-wedding", year: 0, month: 4, day: 19 },
  { title: "Apply for a mariage license", tag: "pre-wedding", year: 0, month: 3, day: 22 },
  { title: "Create a schedule for your wedding date", tag: "pre-wedding", year: 0, month: 3, day: 29 },
  { title: "Order welcome bags", tag: "pre-wedding", year: 0, month: 3, day: 2 },

  //1-2week
  { title: "Confirm honeymoon reservations and pack", tag: "pre-wedding", year: 0, month: 1, day: 13 },
  { title: "Confirm the timing with all vendors", tag: "pre-wedding", year: 0, month: 1, day: 10 },
  { title: "Pay final balances", tag: "pre-wedding", year: 0, month: 1, day: 7 },
  { title: "Collect groomswear and wedding dress", tag: "pre-wedding", year: 0, month: 0, day: 12 },

  { title: "Congrats! It is your wedding day", tag: "post-wedding", year: 0, month: 0, day: 0 },
  { title: "Write thank you notes", tag: "post-wedding", year: 0, month: 0, day: -2 },
  { title: "Leave reviews for your vendors", tag: "post-wedding", year: 0, month: 0, day: -30 },

]