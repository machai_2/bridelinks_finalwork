import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef, useState } from 'react';

import styles from './style.module.scss';


export function SortChecklistDropdown(props) {
  const [OpenDropdown, setOpenDropdown] = useState(false);
  const [filter, setFilter] = useState('all');
  const list = [{id:1,value:'all',label:'All'}, {id:2,value:'todo',label:'To do'}, {id:3,value:'done',label:'Done'}];

  const showDropdown = () => {
    setOpenDropdown(!OpenDropdown);
  };


  const ValueSelected = (select) => () => {
    setFilter(select.label);
    setOpenDropdown(false);
    props.valueDropdown(select.value);
  };

  return (
    <div>
      <button className={styles.container} onClick={showDropdown}>
        <span>{filter}</span>
        <FontAwesomeIcon
          className={`${OpenDropdown ? styles.ArrowUp : styles.ArrowDown}`}
          size="xs"
          icon="fa-solid fa-chevron-down"
        />
      </button>

      {OpenDropdown ? (
        <div className={styles.dropdown}>
          {list.map((item,index) => (
            <li onClick={ValueSelected(item)} key={index} value={item.value} className={styles.item}>
              <a>{item.label}</a>
            </li>
          ))}
        </div>
      ) : null}
    </div>
  );
}
