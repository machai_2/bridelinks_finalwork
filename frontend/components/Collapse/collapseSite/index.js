import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';

import styles from './style.module.scss';

export function CollapseSite({ children, name }) {
  const [OpenDropdown, setOpenDropdown] = useState(true);
  const showDropdown = () => {
    setOpenDropdown(!OpenDropdown);
  };

  return (
    <div>
      <button className={styles.containerButton} onClick={showDropdown}>
        <FontAwesomeIcon
          className={`${OpenDropdown ? styles.ArrowUp : styles.ArrowDown}`}
          size="xs"
          icon="fa-solid fa-chevron-down"
        />
        <span className={styles.titleDropdown}>{name}</span>
      </button>

      {OpenDropdown ? <div className={styles.dropdownMenu}>{children}</div> : null}
    </div>
  );
}
