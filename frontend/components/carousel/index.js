import React from 'react';
import SimpleImageSlider from 'react-simple-image-slider';
import styles from './style.module.scss';

export default function Carousel() {
  return (
    <div className={styles.carousel_area}>
      <SimpleImageSlider
        width={1455}
        height={595}
        images={images}
        showBullets={true}
        showNavs={false}
        autoPlay={true}
        useGPURender={true}
        priority
      />
    </div>
  );
}
const images = [
  { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/v1649169713/Bridelinks/home_main_EDITED_ohaisw.png' },
  { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/v1649169713/Bridelinks/home_main_EDITED_ohaisw.png' },
  { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/v1649169713/Bridelinks/home_main_EDITED_ohaisw.png' },
  { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/v1649169713/Bridelinks/home_main_EDITED_ohaisw.png' },
  { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/v1649169713/Bridelinks/home_main_EDITED_ohaisw.png' },
];
