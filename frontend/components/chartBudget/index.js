import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);
export function ChartBudget() {

  const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Noc', 'Dec'];

  const data = {
    labels,
    datasets: [
      {
        maxBarThickness: 36,
        backgroundColor: '#cd8256',
        borderWidth: 1,
        data: [65, 59, 80, 81, 56, 55, 40, 90, 87, 60, 14, 33]
      },

    ],
  };


  const options = {
    tooltips: { enabled: true },
    showTooltips: true,
    events: [],
    responsive: true,
    maintainAspectRatio: false, title: {
      display: true,
      text: 'Average Rainfall per month',
      fontSize: 20
    },
    plugins: {
      legend: {
        display: false
      },
    },

    scales: {
      x: {
        grid: {
          display: true,
          drawTicks: true

        },
        beginAtZero: false,
      },
      y: {
        grid: {
          display: false,
          drawTicks: true
        },
        beginAtZero: true,
        ticks: {
          display: true
        }

      }
    }
  }
  return (
    <div className="App">
      <Bar width="1010" height="250" data={data} options={options} />
    </div>
  );

}
