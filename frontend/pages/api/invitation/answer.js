
import axios from 'axios';

export default async (req, res) => {
  const { token } = req.query;
  axios.get(`http://localhost:1337/api/invitations?filters[token][$eq]=${token}&populate=*`)
    .then((response) => {
      const invitation = response.data.data[0]
      if (invitation === undefined) {
        res.status(200).json({ message: 'Your reset token is invalid' });
      } else if (Date.now() > invitation.attributes.expired) {
        res.status(200).json({ message: 'Password reset token is invalid or has expired' });
      } else if (invitation.attributes.status === "accepted") {
        res.status(200).json({ message: 'accepted',invitation:invitation });
      }
      else if (invitation.attributes.status === "refused") {
        res.status(200).json({ message: 'refused',invitation:invitation });
      }
       if (invitation.attributes.read === true) {
        console.log("[INVITATION]", `${invitation.attributes.guest_id.data.attributes.firstname} ${invitation.attributes.guest_id.data.attributes.lastname} already read invitation.`)
        res.status(200).json({ message: 'ok',invitation:invitation });
      } else {
        axios.put(`http://localhost:1337/api/invitations/${invitation.id}`,{data:{read:true}})
        .catch((err) => res.status(500).json({ error: err.response}))
           console.log("[INVITATION]", `${invitation.attributes.guest_id.data.attributes.firstname} ${invitation.attributes.guest_id.data.attributes.lastname}  read his invitation.`)
           res.status(200).json({ message: 'ok',invitation:invitation });
          }

    }).catch((err) => res.status(500).json({ error: err}))
}  