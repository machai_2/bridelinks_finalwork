import { makeStyles } from '@material-ui/styles';


export const InputStyle = makeStyles({
  root: {
    '& .MuiInput-underline:before': {
      borderBottomColor: '#838282',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#af4720',
    },
    '&.MuiInputBase-root-MuiInput-root:after': {
      borderBottom: '1px solid #1af11a',
  }
}
});
