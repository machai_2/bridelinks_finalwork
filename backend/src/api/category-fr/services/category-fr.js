'use strict';

/**
 * category-fr service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::category-fr.category-fr');
