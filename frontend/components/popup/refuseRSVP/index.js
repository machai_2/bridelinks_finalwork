import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import axios from 'axios';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import styles from './style.module.scss';
import { signOut, useSession } from 'next-auth/react';
import { useRouter } from 'next/router';

export function RefuseRSVP(props) {
  const [groupName, setGroupName] = useState("");
  const { data: session } = useSession();
  const router = useRouter();
  const { token } = router.query

  const handleSubmit = async (e) => {
    e.preventDefault();
    axios.post(`http://localhost:3000/api/invitation/refused`,{token
  }).then(function (response) {
    router.replace('/rsvp/refused')
}).catch(function (error) {
    console.log(error);
});
    }

  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h2>Are you sure you want to <span>refuse?</span></h2>
            <DashboardSolidButton onClick={handleSubmit}>confirm</DashboardSolidButton>
            <DashboardOutlineButton onClick={props.closePopup}>cancel</DashboardOutlineButton>
          </form>
        </div>
      </div>
    </div>
  );
}
