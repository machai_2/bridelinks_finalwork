'use strict';

/**
 * category-budget service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::category-budget.category-budget');
