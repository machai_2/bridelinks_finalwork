import React from 'react';

import { useCountdown } from './../../helpers/useCountdown';
import styles from './style.module.scss';

const ExpiredNotice = () => (
  <div className={styles.expired}>
    <span>Congrats on your wedding</span>
  </div>
);

const ShowCounter = ({ days, hours, minutes, seconds, type }) =>
  type == '1' ? (
    <div className={styles.showCounter}>
      <div className={styles.countContainer}>
        <div className={styles.redBorder}>{days}</div>
        <p className={styles.title}>DAYS</p>
      </div>
      <div className={styles.countContainer}>
        <div className={styles.redBorder}>{hours}</div>
        <p className={styles.title}>HOURS</p>
      </div>

      <div className={styles.countContainer}>
        <div className={styles.redBorder}>{minutes}</div>
        <p className={styles.title}>MINUTES</p>
      </div>
      <div className={styles.countContainer}>
        <div className={styles.redBorder}>{seconds}</div>
        <p className={styles.title}>SECONDS</p>
      </div>
    </div>
  ) : (
    <div className={styles.showCounter}>
      <div className={styles.countContainer2}>
        <div className={styles.noBorder}>{days}</div>
        <p className={styles.title2}>DAYS</p>
      </div>
      <div className={styles.countContainer2}>
        <div className={styles.noBorder}>{hours}</div>
        <p className={styles.title2}>HOURS</p>
      </div>

      <div className={styles.countContainer2}>
        <div className={styles.noBorder}>{minutes}</div>
        <p className={styles.title2}>MINUTES</p>
      </div>
      <div className={styles.countContainer2}>
        <div className={styles.noBorder}>{seconds}</div>
        <p className={styles.title2}>SECONDS</p>
      </div>
    </div>
  );

const CountdownTimer = ({ targetDate, type }) => {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);
  if (days + hours + minutes + seconds <= 0) {
    return <ShowCounter days={0} hours={0} minutes={0} seconds={0} type={type} />;
  } else {
    return <ShowCounter days={days} hours={hours} minutes={minutes} seconds={seconds} type={type} />;
  }
};

export default CountdownTimer;
