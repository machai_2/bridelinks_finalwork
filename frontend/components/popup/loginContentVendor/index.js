import { signIn } from 'next-auth/react';
import { PrimaryButton, SocialsButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link'

export function LoginContentVendor() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState('');
  const [error, setError] = useState(false);


  const router = useRouter();

  const onsubmit = async (e) => {
    e.preventDefault();

    setLoading(true);
    const res = await signIn('credentials', {
      redirect: false,
      email: email,
      password: password,
      url: `${window.location.origin}`,
    });
    console.log('the response here ' + JSON.stringify(res))
    if (res?.status == 401) {
      console.log('login errors ' + res.error)
      setError(true);
    }
    if (res?.status == 200) {
      router.replace('/');
      return;
    }
    setLoading(false);
  }

  function handleEmailChange(event) { setEmail(event.target.value); }

  function handlePasswordChange(event) { setPassword(event.target.value); }

  return (
    <div>
      <div>
        <h2>Sign in to your account</h2>
      </div>

      <div className={styles.buttonsSocials}>
        <SocialsButton className={styles.button_items} icon="fa-brands fa-google" size="xl" onClick={() => signIn('google')}>
          Log in with Google
        </SocialsButton>
        <SocialsButton className={styles.button_items} icon="fa-brands fa-facebook-square" size="xl" onClick={() => signIn('facebook')}>
          Log in with Facebook
        </SocialsButton>
      </div>
      <div className={styles.text_OR}>
        <span>OR</span>
      </div>
      <form className={styles.form} onSubmit={(e) => onsubmit(e)}>
        <label>
          <InputForm type='email' value={email} id={email} onChange={handleEmailChange} placeholder="email" />
        </label>
        <label>
          <InputForm type='password' value={password} id={password} onChange={handlePasswordChange} placeholder="password" />
        </label>
        <Link href={{ pathname: '/', query: { mode: 'forgot' } }}>
          <a>
            <span className={styles.forgot} >Forgot your password?</span>
          </a>
        </Link>

        <div className={styles.form_button}>
          <PrimaryButton>Login</PrimaryButton>       </div>  </form>
      <div className={styles.error}>{error ? `Your password and email do not match. Try again` : ''}</div>
      <div className={styles.footer}>
        <span>Not joined yet?
          <Link href={{ pathname: '/', query: { mode: 'register' } }}>
            <a>
              <span className={styles.linkPopup}> create your account here</span>
            </a>
          </Link>
        </span>
      </div>
    </div>
  );
}
