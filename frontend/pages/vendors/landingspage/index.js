import React, { useState, useEffect } from 'react';
import { PrimaryButton, SliderButtons } from '../../../components/buttons';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import axios from 'axios';
import VendorLayout from '../../../components/layout/vendor';
import Image from 'next/image';
import { InputForm } from '../../../components/inputs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SimpleImageSlider from 'react-simple-image-slider';
import FeatherIcon from 'feather-icons-react';


export default function Landingspage() {
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const images = [
    { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,w_637/v1661746003/Bridelinks/vendor1_sqpu10.jpg' },
    { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,h_405/v1661745987/Bridelinks/florist_ccmau0.webp' },
    { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,h_420/v1661745985/Bridelinks/vendor2_p3zu5p.webp' },
    { url: 'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,h_448/v1661746203/Bridelinks/wedCake_jnuhzz.jpg' },
  ];


  async function handleSubmit(e) {
    e.preventDefault();
    const user_type = "vendor";
    try {
      await axios.post('/api/auth/register', { email, password, user_type });
      handleSignIn(email, password)

      router.replace("/vendor-questions");

    } catch (err) {
      console.log('the reg error ' + JSON.stringify(err.response.data))
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <Image src="/Bridelinks/pexels-melike-benli-8633364_fpvwzh.jpg"
          width={1820}
          layout="responsive"
          height={500}
          priority={true}
          objectFit="cover" />
        <div className={styles.banner_content}>
          <div className={styles.body_form}>
            <h1>Want to maximize your potential?</h1>
            <p>Advertise now on Brides & Link and bring your business to a new level</p>
            <form onsubmit={handleSubmit}>
              <div className={styles.row}>
                <InputForm placeholder='firstname'></InputForm>
                <InputForm placeholder='lastname'></InputForm>
              </div>
              <div className={styles.row}>
                <InputForm placeholder='name company'></InputForm>
                <InputForm placeholder='vendor category'></InputForm>
              </div>
              <div className={styles.row}>
                <InputForm placeholder='Email '></InputForm>
                <InputForm placeholder='password'></InputForm>
              </div>
              <div className={styles.checkbox}>

                <input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter" />
                <label for="subscribeNews">By submitting and sharing your information you agree to BrideLinks terms of use and privacy policy.</label>
              </div>
              <PrimaryButton>Start now</PrimaryButton>
            </form>
          </div>
        </div>
      </div>

      <section className={styles.part1}>

        <div className={styles.promote}>
          <div className={styles.main_title}>
            <h2>Promote your business</h2>
            <p>We connect suppliers & vendors to their ideal couple</p>
          </div>

          <div className={styles.carousel_area}>
            <SimpleImageSlider
              width={400}
              navSize={15}
              height={330}
              images={images}
              showNavs={true}
              autoPlay={true}
              useGPURender={true}
              priority
            />
          </div>
          <PrimaryButton>Showcase your business</PrimaryButton>
        </div>

        <div className={styles.split}>
          <h2>Manage & grow your business with our platform !</h2>
          <div className={styles.core}>
            <div>
              <h3>Create a business profile</h3>

              <img
                src={'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,h_470/v1649427025/Bridelinks/pexels-trung-nguyen-2959192_vyaqqe.jpg'}
                // layout="fixed"
                // width={300}
                // height={300}
                // alt="rose arc svg"
                className={styles.item_img}
              />
              <p>Forge a brand and profile by giving extensive
                information about the services you provide to attract potential new clients.
              </p>
            </div>
            <div>
              <h3>Boost your visibility and exposure</h3>

              <img
                src={'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,h_339/v1653286378/Bridelinks/houseWedding_gehmep.jpg'}
                // layout="fixed"
                // width={300}
                // height={300}
                // alt="rose arc svg"
                className={styles.item_img}
              />
              <p>Show off your work and attract couples who are looking for you.
              </p>
            </div>
            <div>
              <h3>Be part of clients most important day</h3>
              <img
                src={'https://res.cloudinary.com/dmj6tqnkw/image/upload/c_scale,h_410/v1661745985/Bridelinks/vendor2_p3zu5p.webp'}
                // layout="fixed"
                // width={300}
                // height={300}
                // alt="rose arc svg"
                className={styles.item_img}
              />
              <p>Nothing is better than being part of a couples big day!
                Provide your best services to create new smiles! </p>
            </div>
          </div>
        </div>
        <div>
        </div>

      </section>
      <section className={styles.part2}>
        <h2>Features & benefits</h2>
        <div className={styles.cards}>
          <div className={styles.card_item}>
            <h3 className={styles.title}>customer reviews & feedback</h3>
            <FeatherIcon icon="message-square" size={'3rem'} color="#AF5520" />

            <p className={styles.text}>Get feedback from your customers to establish beter strategies base on their scores and comments</p>
          </div>

          <div className={styles.card_item}>
            <h3 className={styles.title}>Contact with customers</h3>
            <FeatherIcon icon="globe" size={'3rem'} color="#AF5520" />
            <p className={styles.text}>With our wide array of clients, you can expand your customer base as well as contact them directly</p>
          </div>

          <div className={styles.card_item}>
            <h3 className={styles.title}>personal business page</h3>
            <FeatherIcon icon="briefcase" size={'3rem'} color="#AF5520" />
            <p className={styles.text}>Stand out by creating a full profiles of your services to attract customers</p>
          </div>
        </div>
      </section>
    </div>

  );
}




Landingspage.getLayout = function getLayout(page) {
  return <VendorLayout>{page}</VendorLayout>;
};
