'use strict';

/**
 * plus-one router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::plus-one.plus-one');
