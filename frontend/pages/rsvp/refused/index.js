
import React, { useState, useEffect } from 'react';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import useSWR from 'swr'

export default function Refused() {
  const router = useRouter();



  return (

    <div className={styles.container}>
      <section>
        You refused this invitation, thank you for answering!
      </section>

    </div>




  );
}
