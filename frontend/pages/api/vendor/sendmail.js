import axios from 'axios';

const sendinblue = require("../sendinblue");

export default async (req, res) => {
  console.log('THE PARAMS  ' + JSON.stringify(req.body))

  try {
    let sendSmtpEmail = {
      to: [{
        email: req.body.email //TODO: Change this on production.
      }],

      templateId: 3,
      params: {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        body: req.body.body,
        email: req.body.email,
        subject: "Message client",
      },

    };

    sendinblue(sendSmtpEmail)
    res.status(200).end();
  } catch (err) {
    console.error(err);
    res.status(400).send(err);

  }
};