import styles from './style.module.scss';
import React from 'react';
import { InputForm } from './../../../inputs';

export function Page2({ data, setData }) {
  return (
    <div className={styles.container}>
      <h2>When is your special day ?</h2>
      <div>
        <div className={styles.content_container}>
          The date was on the
          <div>
            <div>
            </div><InputForm type='date' value={data.date} onChange={(e) => setData({ ...data, date_wedding: e.target.value })} placeholder="date" />
          </div>
        </div>
      </div>
    </div>
  )
}
