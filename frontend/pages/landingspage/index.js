import Image from 'next/image';
import { useSession } from 'next-auth/react';
import Carousel from '../../components/carousel';
import Download from '../../components/download';
import HomeLayout from '../../components/layout/home';
import Platform from '../../components/platform';
import { PreviewDashboard } from '../../components/previewDashboard';
import Services from '../../components/services';
import { PortalButton, PrimaryButton } from './../../components/buttons';
import Features from './../../components/features';
import styles from './style.module.scss';
import { useRouter } from 'next/router'
import { InputSearchVendors } from '../../components/inputs';
import useSWR from 'swr'
import { useState, useEffect } from 'react';


export default function Landingspage() {
  const router = useRouter()
  const { data: session } = useSession();
  const [selectedCategory, setSelectedCategory] = useState("");
  const [selectedLocation, setSelectedLocation] = useState("");

  const vendor_url = 'http://localhost:1337/api/vendor-nls?populate=*';
  const category_url = 'http://localhost:1337/api/category-nls?populate=*'

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: vendors, error: vendorErr, mutate: mutateVendor } = useSWR(vendor_url, fetcher);
  const { data: category, error: categoryErr, mutate: mutateCategory } = useSWR(category_url, fetcher)

  let categoriesNames = category && category.data?.map(i => i.attributes.name);
  let filter_location = vendors && vendors.data?.filter((element, index, array) => array.indexOf(element) === index).map(i => i.attributes.location.split(',')[1].trim())


  const handleSubmit = async (e) => {
    e.preventDefault();
    router.push({ pathname: '/vendors', query: { category: selectedCategory, location: selectedLocation } })
  }
  const handlePortalSubmit = async () => {
    router.replace("/vendors/landingspage");
  }


  return (
    <div>
      <div className={styles.container}>
        <main className={styles.main} id="Home">
          <Carousel />
          <div className={styles.primary_arc}>
            <Image
              src="/Bridelinks/arc_primary_nhc1kq.svg"
              width={1134}
              priority={true}
              height={865}
              quality={50}
              layout="responsive"
              alt="beige decorative arc svg"
            />

            <div className={styles.text_top}>
              <h4>Want to plan your wedding?</h4>
              <h1>Use our wedding platform to plan your own wedding</h1>
              <p>Book the best local vendors and look at the reviews from other couples</p>
              <div className={styles.vendorForm}>
                <form onSubmit={handleSubmit}>
                  <InputSearchVendors list={categoriesNames} onchange={(event, value) => setSelectedCategory(value)}>Category vendor</InputSearchVendors>
                  <InputSearchVendors list={filter_location} onchange={(event, value) => setSelectedLocation(value)}>City</InputSearchVendors>
                  <PrimaryButton>Search</PrimaryButton>
                </form>
              </div>
              {!session ? (<>
                <div className={styles.text_bottom}>
                  <h4>Are you a proffessional vendor or venue?</h4>
                  <p>Grow your small business by using our platform and accessing our clients.</p>
                  <PortalButton onClick={handlePortalSubmit}>business portal</PortalButton>
                </div></>) : null}
            </div>
          </div>
        </main>
        {session ? (
          <PreviewDashboard />
        ) : null}
        <Services />
        <Platform />
      </div>
      <Features />
      <Download />
    </div>
  );


}

Landingspage.getLayout = function getLayout(page) {
  return <HomeLayout>{page}</HomeLayout>;
};




