import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DashboardSolidButton } from '../../../components/buttons/dashboardSolidButton';
import DashboardLayout from '../../../components/layout/dashboard';
import Tab from '../../../components/tab';
import styles from './style.module.scss';
import useSWR from 'swr'
import { useSession, getSession } from 'next-auth/react';
import { AddVendorPopup } from '../../../components/popup';

export default function Vendors() {
  const [OpenAddVendor, setOpenAddVendor] = useState(false);

  const showAddVendors = () => {
    setOpenAddVendor(!OpenAddVendor);
  };

  return (
    <div className={styles.container}>
      <div className={styles.top_vendor}>
        <div className={styles.title_dashboard}>
          ALL OF MY VENDORS
          <p>21 out of 30 categories hired</p>
        </div>
        <Tab />
        <div className={styles.action_buttons}>
          <DashboardSolidButton onClick={showAddVendors}>Add a vendor</DashboardSolidButton>
          {OpenAddVendor ? <AddVendorPopup closePopup={showAddVendors} /> : null}
        </div>
      </div>
    </div>
  );
}


Vendors.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
