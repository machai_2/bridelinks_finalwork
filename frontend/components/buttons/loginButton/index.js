import React, { useState } from 'react';
import { useRouter } from 'next/router';
import styles from './style.module.scss';
import { LoginPopup } from '../../popup';

export function LoginButton() {
  const [OpenPopup, setOpenPopup] = useState(false);
  const router = useRouter()

  const showPopup = () => {
    setOpenPopup(!OpenPopup);
    if(OpenPopup==false){
      router.replace('/', undefined, { shallow: true });
    }
  };

  return (
    <div>
      <button onClick={showPopup} className={styles.login_btn}>
        Log in
      </button>
      {OpenPopup ? <LoginPopup closePopup={showPopup} /> : null}
    </div>
  );
}
