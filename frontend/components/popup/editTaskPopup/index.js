import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from "react";
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputCheckbox, InputForm } from '../../inputs';
import styles from './style.module.scss';
import useSWR, { useSWRConfig } from 'swr'
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';

const tags = ["engagement", "pre-wedding", "post-wedding", "paperwork"]


export function EditTaskPopup(props) {
  const router = useRouter();

  const [titleTask, setTitleTask] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState(tags[0]);
  const [period, setPeriod] = useState();
  const [done, setDone] = useState(false);
  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: checklist, error: error } = useSWR(props.id ? `http://localhost:1337/api/checklists/${props.id}` : null,
    fetcher)

  if (error) <p>Loading failed...</p>;
  if (checklist) <h1>Loading...</h1>;

  useEffect(() => {
    if (checklist?.data) {
      setTitleTask(checklist.data.attributes.title)
      setDescription(checklist.data.attributes.description)
      setCategory(checklist.data.attributes.tag)
      setDone(checklist.data.attributes.done)
      setPeriod(checklist.data.attributes.period)
    }
  }
    , [checklist]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (titleTask) {
      const data = {
        title: titleTask,
        done,
        tag: category,
        period,
        user_id: session.id,
        description
      }

      try {
        const response = await fetch(`http://localhost:1337/api/checklists/${props.id}`, {
          method: 'PUT',
          mode: 'cors',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ data })

        })
        console.log(response);
        router.reload(window.location.pathname)

      } catch (error) {
        console.log(error)
      }
      props.closePopup()

    }
  };

  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h4>Edit {titleTask}</h4>
            <section className={styles.content} key={titleTask}>
              <div>
                <label>Title of the task</label>
                <InputForm type='text' placeholder='name your task' defaultValue={titleTask} onChange={(e) => { setTitleTask(e.target.value) }}></InputForm>
              </div>
              <div>
                <label>Description of the task</label>
                <InputForm type='text' placeholder='add a note or description of the task' defaultValue={`${description}`} onChange={(e) => { setDescription(e.target.value) }}></InputForm>
              </div>
              <div>

                <label>Due date</label>
                <InputForm type='date' placeholder='date' defaultValue={`${period}`} onChange={(e) => { setPeriod(e.target.value) }}></InputForm>
              </div>
              <div>
                <label>Category</label>
                <select name="categories" value={category} onChange={(e) => { setCategory(e.target.value) }}>
                  {tags && tags.map((item, index) => (<option value={item}>{item}</option>))}
                </select>
              </div>
              <div>
                <label>Mark as done</label>
                <InputCheckbox status={done} onChange={(e) => { setDone(e.target.checked) }} />
              </div>
            </section>

            <div className={styles.saveTask}>
              <DashboardSolidButton type="submit">Save changes</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
