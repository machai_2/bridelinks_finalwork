import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';

import styles from './style.module.scss';


export function SortVendorDropdown() {
  const [OpenDropdown, setOpenDropdown] = useState(false);
  const [value, setValue] = useState('rating');
  const list = ['rating', 'price asc', 'pric desc'];

  const showDropdown = () => {
    setOpenDropdown(!OpenDropdown);
  };

  const ValueSelected = (select) => () => {
    setValue(select);
    setOpenDropdown(false);
    console.log(value);
  };

  return (
    <div>
      <button className={styles.container} onClick={showDropdown}>
        <span>{value}</span>
        <FontAwesomeIcon
          className={`${OpenDropdown ? styles.ArrowUp : styles.ArrowDown}`}
          size="xs"
          icon="fa-solid fa-chevron-down"
        />
      </button>

      {OpenDropdown ? (
        <div className={styles.dropdown}>
          {list.map((item) => (
            <li onClick={ValueSelected(item)} className={styles.item}>
              <a>{item}</a>
            </li>
          ))}
        </div>
      ) : null}
    </div>
  );
}
