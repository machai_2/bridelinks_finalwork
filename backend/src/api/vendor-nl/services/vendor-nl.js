'use strict';

/**
 * vendor-nl service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::vendor-nl.vendor-nl');
