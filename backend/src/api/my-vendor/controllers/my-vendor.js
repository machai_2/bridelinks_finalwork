'use strict';

/**
 *  my-vendor controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::my-vendor.my-vendor');
