import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import MuiNativeSelect from '@mui/material/NativeSelect';
import { InputStyle } from './../inputStyle';


export function SelectDashboard(props) {
  const classes = InputStyle();

  return (
    // 120
    <Box sx={{ minWidth: 80 }}
    >
      <FormControl fullWidth>
        <InputLabel htmlFor="uncontrolled-native"
        >
        </InputLabel>
        <MuiNativeSelect
          defaultValue={props.defaultValue}
          onChange={props.onChange}
          inputProps={{
            id: 'uncontrolled-native',
          }}
        >
          {props.options?.map((item, index) => (
            <option value={props.value}
              key={index} className={classes.root}>{item.label}</option>
          ))}
        </MuiNativeSelect>
      </FormControl>
    </Box>
  );
}