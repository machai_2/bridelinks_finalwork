'use strict';

/**
 *  guest-group controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::guest-group.guest-group');
