'use strict';

/**
 * vendor-fr router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::vendor-fr.vendor-fr');
