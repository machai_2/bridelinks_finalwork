import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';
import React from 'react';
import { UserDropdown } from '../dropdowns';
import styles from './style.module.scss';
import { LoginVendorButton } from '../buttons/loginVendorButton'
import { RegisterVendorButton } from '../buttons/registerVendorButton';
import { VendorDropdown } from '../dropdowns/vendorDropdown';


export default function HeaderVendor() {
  const { data: session } = useSession();
  console.log('the header vendor ' + JSON.stringify(session))
  return (
    <div className={styles.navbar_container}>
      <nav className={styles.navbar_main}>
        <ul>
          <div className={styles.nav_links}>

            <div className={styles.logo_area}>
              <Link href={'/dashboard-vendor/main'}>
                <a>
                  <div>
                    <span className={styles.logo}>
                      Bride <span>&</span> Links
                    </span>
                  </div>
                </a>
              </Link>

            </div>

            <div className={styles.r_nav}>
              {!session &&
                <>
                  <div className={styles.r_item}>
                    <LoginVendorButton />
                  </div>
                  <div className={styles.r_item}>
                    <RegisterVendorButton />
                  </div>
                </>
              }
              {session &&
                <VendorDropdown />
              }
            </div>

          </div>
        </ul>
      </nav>
    </div>
  );
}
