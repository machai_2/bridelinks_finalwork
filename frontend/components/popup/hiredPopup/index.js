import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { DashboardSolidButton } from '../../buttons';
import useSWR from 'swr'
import { useRouter } from 'next/router';


export function HiredPopup(props) {
  const router = useRouter()

  const saveHired = async () => {
    const objectData = {
      hired: true,
      wedding_id: props.weddingId,
      id_link: props.vendor.data.attributes.id_link
    }
    const response = await fetch(`http://localhost:1337/api/my-vendors?filters[id_link]=${props.vendor.data.attributes.id_link}`)
    let data = await response.json();
    console.log('it exist here ' + JSON.stringify(data) + "    " + data.data.length)

    if (data.data.length) {
      const response = await fetch('http://localhost:1337/api/my-vendors/' + data.data[0].id, {
        method: 'PUT',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: objectData })
      })
      console.log(response);
    } else {
      try {
        const response = await fetch('http://localhost:1337/api/my-vendors/', {
          method: 'POST',
          mode: 'cors',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ data: objectData })
        })
        console.log(response);
      } catch (error) {
        console.log(error)
      }
    }
    props.closePopup()
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            <h3>Do you want to hire "{props.vendor.data.attributes.name}"?</h3>
            <p>This will mark them as hired in your dashboard.</p>
            <div className={styles.buttonGroup}>
              <DashboardSolidButton onClick={saveHired}>Yes !</DashboardSolidButton>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
