import Image from 'next/image';
import { DiscoverButton } from './../buttons';
import styles from './style.module.scss';

export default function Services() {

  return (
    <section className={styles.container} id="Services">
      <div className={styles.text_middle}>
        <h2>Use our platform to plan your own wedding !</h2>
        <p>Sign up and get access to our all-in one wedding planner.</p>
      </div>
      <div className={styles.arcs}>
        <div className={styles.arc_item}>
          <Image
            src="/Bridelinks/arc_rose_adge2y.svg"
            alt="rose arc svg"
            className={styles.item_img}
            layout="fixed"
            width={200}
            quality={60}
            height={300}
          />
          <h3>Find vendors</h3>
          <p>Look at our wide range of local vendors near you</p>
          <DiscoverButton />
        </div>
        <div className={styles.arc_item}>
          <Image
            src={'/Bridelinks/arc_redletter_rtsaqb.svg'}
            layout="fixed"
            width={300}
            height={380}
            alt="rose arc svg"
            className={styles.item_img}
          />
          <h3>Send & management invitation</h3>
          <p>Send invitations automatically through email & sms</p>
          <DiscoverButton />
        </div>
        <div className={styles.arc_item}>
          <Image
            src={'/Bridelinks/arc_organize_hnnawu.svg'}
            layout="fixed"
            width={200}
            height={300}
            alt="rose arc svg"
            className={styles.item_img}
          />
          <h3>Organise everything</h3>
          <p>from budget to seat arrangements our tools will help you</p>
          <DiscoverButton />
        </div>
      </div>
    </section>
  );
}

