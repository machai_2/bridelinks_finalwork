import Image from 'next/image';
import { useEffect, useState } from 'react';
import useSWR from 'swr'
import { useSession, getSession } from 'next-auth/react';
import styles from './style.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';

export default function TabHired(props) {
  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: vendors, error: vendorError } = useSWR('http://localhost:1337/api/vendor-nls?populate=*', fetcher)

  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}&populate=*` : null,
    fetcher)
  const { data: myVendors, error: myVendorError, mutate: myMutate } = useSWR(wedding ? `http://localhost:1337/api/my-vendors?filters[wedding_id]=${wedding.data[0].id}&populate=*` : null, fetcher);

  if (weddingError || myVendorError) <p>Loading failed...</p>;
  if (myVendors || wedding) <h1>Loading...</h1>;

  const [filteredArray, setFilteredArray] = useState([""]);

  useEffect(() => {
    if (vendors && myVendors) {
      getDetailsVendors();
    }
  }, [vendors, myVendors]);

  function getDetailsVendors() {
    let temp = [];
    myVendors && myVendors?.data?.filter(t => t.attributes.hired).map(async (items) => {
      vendors && vendors?.data?.filter(e => e.attributes.id_link == items.attributes.id_link).map(async (item) => {
        temp.push(item)
      })
    })
    props.valueHired(temp.length);
    setFilteredArray(temp)
  }
  const showDelete = async (data) => {
    console.log('the id ' + data.attributes.id_link)
    axios.get(`http://localhost:1337/api/my-vendors?filters[id_link]=${data.attributes.id_link}`).then(
    ).then(response => {
      console.log('the response ' + JSON.stringify(response.data.data[0].id))
      axios.delete(`http://localhost:1337/api/my-vendors/${response.data.data[0].id}`).then(
      ).then(response => {
        myMutate()
      }).catch(function (error) {
        console.log(error);
      });

    }).catch(function (error) {
      console.log(error);
    });

  }


  return (
    <div className={styles.container}>
      <div className={styles.cards}>
        {filteredArray.map((item, index) => (
          <div key={index} className={styles.card_item}>
            <button onClick={() => showDelete(item)} className={styles.deleteVendor}>
              <FontAwesomeIcon icon="fa-solid fa-xmark" />
            </button>
            <div className={styles.card_image}>

              <img
                src={`https://www.mariage.be/${item.attributes?.image_profile}`}
                alt="wedding cake"
                width={200}
                height={213}
              />
            </div>

            <div className={styles.card_detail}>
              <div className={styles.category}>{item.attributes?.category_nl_id.data.attributes.name}</div>
              <div className={styles.vendor_name}>{item.attributes?.name}</div>
            </div>

          </div>
        ))}
      </div>
    </div>
  );
}
