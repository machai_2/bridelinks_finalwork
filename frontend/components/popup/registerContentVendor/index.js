import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { signIn } from 'next-auth/react';
import { PrimaryButton, SocialsButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useState, useRef } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import Link from 'next/link'
import { SpinnerCircularFixed } from 'spinners-react';

export function RegisterContentVendor() {

  const router = useRouter();

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .required('Email is required')
      .email('Email is invalid'),
    password: Yup.string()
      .min(6, 'Password must be at least 6 characters')
      .required('Password is required'),
  });


  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const [error, setError] = useState(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  if (loading) return <SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} />


  async function handleSignIn(em, pass) {
    setLoading(true);

    const res = await signIn('credentials', {
      redirect: false,
      email: em,
      password: pass,
      callbackUrl: `${window.location.origin}`,
    });
    if (res?.error) {
      setError(res.error);
    } else {
      setError(null);
    }
    setLoading(false);
  }


  async function onsubmit(e) {
    setLoading(true);
    e.preventDefault();
    const user_type = "couple";
    try {
      await axios.post('/api/auth/register', { email, password, user_type });
      handleSignIn(email, password)
      router.replace("/vendors/questions");
      setLoading(false);
    } catch (err) {
      console.log('the reg error ' + JSON.stringify(err.response.data))
    }
  }

  function handleEmailChange(event) {
    setEmail(event.target.value);
  }

  function handlePasswordChange(event) {
    setPassword(event.target.value);
  }


  return (
    <>
      <div>
        <div>
          <h2>Create your new vendor account</h2>
        </div>
        <form className={styles.form} onSubmit={(e) => onsubmit(e)}>
          <label>
            <InputForm type='email' {...register('email')} onChange={handleEmailChange} placeholder="email" />
            {/* <div className="invalid-feedback">{errors.email?.message}</div> */}
          </label>
          <label>
            <InputForm name="password" type="password" {...register('password')} onChange={handlePasswordChange} placeholder="password" />
            {/* <div className="invalid-feedback">{errors.password?.message}</div> */}
          </label>
          <div className={styles.checkbox}>

            <input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter" />
            <label for="subscribeNews">By submitting and sharing your information you agree to BrideLinks terms of use and privacy policy.</label>
          </div>
          <div className={styles.form_button}>
            <PrimaryButton type="submit">Register</PrimaryButton>
          </div>
        </form>
        <div className={styles.footer}>
          <span>Do you already have an account?
            <Link href={{ pathname: '/', query: { mode: 'login-vendor' } }}>
              <a>
                <span className={styles.linkPopup}>Already have an account?</span>
              </a>
            </Link>
          </span>
        </div>
      </div>
    </>
  );
}
