import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';

export default function Greeting(props) {
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <p>
          Hi welcome back, <span id={styles.name}>{props.user.firstname} {props.user.lastname}</span>
        </p>
      </div>
      <div className={styles.icons}>
        <FontAwesomeIcon icon="fa-solid fa-bell" />
        <FontAwesomeIcon icon="fa-solid fa-user" />
        <FontAwesomeIcon icon="fa-solid fa-gear" />
      </div>
    </div>
  );
}


