
import React, { useState, useEffect } from 'react';
import styles from './style.module.scss';
import { RefuseRSVP, AcceptRSVP } from '../../../components/popup';
import { DashboardOutlineButton, DashboardSolidButton } from '../../../components/buttons';
import FeatherIcon from 'feather-icons-react';
import { useRouter } from 'next/router'

export default function Pending() {
  const [OpenPopupRefuse, setOpenPopupRefuse] = useState(false);
  const [OpenPopupAccept, setOpenPopupAccept] = useState(false);
  const [wedding, setWedding] = useState(null);
  const [rsvpDate, setRsvpDate] = useState(null);

  const router = useRouter()
  const { token } = router.query;

  const showPopupRefuse = () => {
    setOpenPopupRefuse(!OpenPopupRefuse);
  };

  const showPopupAccept = () => {
    setOpenPopupAccept(!OpenPopupAccept);
  };

  function subtractWeeks(numOfWeeks, date) {
    const dateCopy = new Date(date.getTime());
    dateCopy.setDate(dateCopy.getDate() - numOfWeeks * 7);
    return dateCopy;
  }

  const convertDateRSVP = (date) => {
    let rsvp_date = subtractWeeks(4, new Date(date))
    var datestring = rsvp_date.getDate() + "." + rsvp_date.getMonth() + "." + rsvp_date.getFullYear();
    setRsvpDate(datestring)
  };

  const fetchWedding = async () => {
    const res = await fetch(`http://localhost:1337/api/invitations?filters[token]=${token}&populate=*`);
    const data = await res.json();
    setWedding(data);
    convertDateRSVP(data.data[0].attributes.wedding_id.data.attributes.date_wedding)
  };

  useEffect(() => {
    fetchWedding();
  }, [token]);


  return (

    <div className={styles.container_pending}>
      <section className={styles.content_pending}>
        <h3>Hi {wedding?.data[0].attributes.guest_id.data.attributes.title}. {wedding?.data[0].attributes.guest_id.data.attributes.firstname} {wedding?.data[0].attributes.guest_id.data.attributes.lastname},</h3>
        <div className={styles.icon}>
          <FeatherIcon icon='mail' size={'3rem'} color="#AF5520" />
        </div>
        <div className={styles.text_content}>
          <div>
            <p>You are formally invited to celebrate the mariage of {wedding?.data[0].attributes.wedding_id.data.attributes.partner_firstname} {wedding?.data[0].attributes.wedding_id.data.attributes.partner_lastname} and Sarah D.</p>
            <p>Click on one of the following buttons to <strong>confirm</strong> or <strong>deny</strong> your presence. </p>
          </div>
          <div className={styles.respond}>
            Kindly reply by the {rsvpDate}
          </div>
        </div>
        <div className={styles.buttonGroup}>
          <DashboardSolidButton onClick={showPopupAccept}>Accept</DashboardSolidButton>
          <DashboardOutlineButton onClick={showPopupRefuse}>Refuse</DashboardOutlineButton>
        </div>
      </section >
      {OpenPopupAccept ? <AcceptRSVP closePopup={showPopupAccept} /> : null
      }
      {OpenPopupRefuse ? <RefuseRSVP closePopup={showPopupRefuse} /> : null}

    </div >
  );
}
