import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { DashboardSolidButton } from '../../buttons';
import useSWR from 'swr'
import { useRouter } from 'next/router';


export function Notification(props) {
  const router = useRouter()
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <h3>Do you want to add "{props.vendor.data.attributes.name}" to your favorites?</h3>
          <p>They will be added in your favorites in the vendors dashboard.</p>
          <div className={styles.buttonGroup}>
            <DashboardSolidButton onClick={saveFavorite}>Yes !</DashboardSolidButton>
          </div>
        </div>
      </div>
    </div>
  );
}
