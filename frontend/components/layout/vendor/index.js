import Footer from '../../footer';
import HeaderVendor from '../../headerVendor';

export default function VendorLayout({ children }) {
  return (
    <>
      <HeaderVendor />
      <main>{children}</main>
      <Footer />
    </>
  );
}
