'use strict';

/**
 * table-guest router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::table-guest.table-guest');
