
import React, { useState, useEffect } from 'react';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import useSWR from 'swr'
import Pending from './pending';

export default function Rsvp() {
  const router = useRouter();
  const { token } = router.query
  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data, error } = useSWR(token ? `http://localhost:3000/api/invitation/answer?token=${token}` : null,
    fetcher
  )

  let comp;
  if (data) {
    if (data.message === 'ok') {
      comp = <Pending />
    } else if (data.message == "accepted" || data.message == "refused") {
      comp = "Thank you for giving a response, you will recieve more information soon"
    }
    else {
      comp = `error ${data.message}`
    }
  }

  if (error) return <div>Failed to load</div>
  if (!data) return <div>Loading...</div>

  return (
    <div className={styles.container}>
      <section>
        {comp}
      </section>

    </div>
  );
}
