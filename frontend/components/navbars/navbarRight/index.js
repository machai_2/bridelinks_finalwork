import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import { useEffect, useState } from "react";
import useSWR, { useSWRConfig } from 'swr'
import Greeting from '../../greeting';
import { InputCheckbox } from '../../inputs';
import styles from './style.module.scss';
import { useSession } from 'next-auth/react';
import Link from 'next/link';


export default function NavbarRight() {
  const { data: session } = useSession();
  const [user, setUser] = useState([""]);
  const [weddingDate, setWeddingDate] = useState([""]);

  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
  ];
  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: checklist, error, mutate: mutateList } = useSWR(session ? `http://localhost:1337/api/checklists?filters[user_id]=${session.id}&pagination[limit]=5` : null, fetcher)

  const { data: wedding, WedError, mutate: mutateWed } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}` : null, fetcher)

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(`http://localhost:3000/api/auth/restricted`);
      const data = await res.json()
      setUser(data);

    };
    fetchData();
  }, []);

  useEffect(() => {
    if (wedding) {
      convertDate(wedding.data[0].attributes.location)
    }
  }, [wedding]);

  if (error) <p>Loading failed...</p>;
  if (checklist) <h1>Loading...</h1>;


  async function checkInput(e, id) {
    const newChecklist = { done: e }
    try {
      await fetch(`http://localhost:1337/api/checklists/${id}`, {
        method: 'PUT',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: newChecklist })
      })
      mutateList()
    } catch (error) {
      console.log(error)
    }
  }

  const convertDate = (date) => {
    let weddingD = new Date(date)
    var datestring = weddingD.getDate() + "th of" + monthNames[weddingD.getMonth()] + " " + weddingD.getFullYear();
    setWeddingDate(datestring)
  };
  return (
    <div className={styles.container}>
      <Greeting user={user} />
      <div className={styles.content}>
        <Image
          priority
          id={styles.img_profile}
          src="/Bridelinks/pexels-pham-hoang-kha-3785644_gtjytd.jpg"
          width={500}
          height={500}
          layout="responsive"
          alt="couple hugging"
        />
        <div className={styles.profile_text}>
          <div className={styles.names}> {user?.firstname} {user?.lastname} & {wedding?.data[0]?.attributes.partner_firstname} {wedding?.data[0].attributes.partner_lastname}</div>
          <div>
            <div className={styles.date}> {weddingDate}</div>
            <div className={styles.location}> {wedding?.data[0].attributes.location}</div>
          </div>
        </div>

        <div className={styles.container_tasks}>
          <div className={styles.title_task}>Upcoming tasks</div>
          {checklist && checklist["data"]?.map((item, index) => (
            <div className={styles.item_task} key={index}>
              <div className={styles.checkbox_container}>
                <InputCheckbox text={item.attributes.title} status={item.attributes.done} id={item.id} onChange={(a) => { checkInput(a.target.checked, item.id) }} />
              </div>
              <div className={styles.task_name}>
                <div className={styles.category_text}>{item.attributes.category_nl}</div>
              </div>
              <div className={styles.chevron_right}>
                <Link href={`/dashboard/checklist/#${item.id}`} scroll={true} passHref>
                  <a>
                    <FontAwesomeIcon icon="fa-solid fa-chevron-right" />
                  </a>
                </Link>
              </div>
            </div>
          ))}
          <div className={styles.seeMore}>
            <Link href="/dashboard/checklist/" passHref>
              <a>See more</a>
            </Link>          </div>
        </div>
      </div>
    </div>
  );
}