import React from 'react';

import HomeLayout from '../components/layout/home';
import Landingspage from './landingspage';

export default function Home() {
  return <Landingspage />;
}

Home.getLayout = function getLayout(page) {
  return <HomeLayout>{page}</HomeLayout>;
};

