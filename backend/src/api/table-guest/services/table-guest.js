'use strict';

/**
 * table-guest service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::table-guest.table-guest');
