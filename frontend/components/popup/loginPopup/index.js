import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { useState, useRef } from 'react';
import { useRouter } from 'next/router';
import { LoginContent } from '../loginContent';
import { ForgotContent } from '../forgotContent';
import { RegisterContent } from '../registerContent';

export function LoginPopup(props) {
  const router = useRouter()
  let comp;

  if (router.query.mode == 'forgot') {
    comp = <ForgotContent />
  } else if (router.query.mode == 'register') {
    comp = <RegisterContent />
  } else {
    comp = <LoginContent />
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            {comp}
          </div>
        </div>
      </div>
    </>
  );
}
