import React from 'react';

import styles from './style.module.scss';

export function InputCheckbox(props) {
  return (
    <div className={styles.container}>
      <input defaultValue={props.default} className={props.status ? `${styles.isDone}` : `${styles.notDone}`} type="checkbox" id={`checkbox${props.id}`} onChange={props.onChange} />
      <label htmlFor={`checkbox${props.id}`}><span>{props.text}</span></label>
    </div>
  );
}
