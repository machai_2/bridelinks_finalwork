import { makeStyles } from '@material-ui/styles';


export const InputStyle = makeStyles({
  root: {
    '& .MuiInputBase-root-MuiInput-root:before': {
      borderBottomColor: 'red',
    },
    '& .MuiInputBase-root-MuiInput-root:after': {
      borderBottomColor: 'red',
    },
    '&.MuiNativeSelect-select MuiNativeSelect-outlined MuiInputBase-root-MuiInput-root:after': {
      borderBottom: '1px solid #1af11a',
    }
  }
});
