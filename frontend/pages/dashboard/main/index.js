import { useSession, getSession } from 'next-auth/react';
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FeatherIcon from 'feather-icons-react';
import Image from 'next/image';
import { Progress } from 'react-sweet-progress';
import 'react-sweet-progress/lib/style.css';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import CountdownTimer from '../../../components/countdown';
import DashboardLayout from './../../../components/layout/dashboard';
import styles from './style.module.scss';
import useSWR from 'swr'
import { EditBudgetPopup } from '../../../components/popup/EditBudgetPopup';
import { ChartBudget } from '../../../components/chartBudget';
import Link from 'next/link';

//jk

export default function Main() {
  const { data: session } = useSession();
  const [taskDone, setTaskDone] = useState(0);
  const [taskOngoing, setTaskOngoing] = useState(0);
  const [weddingDate, setWeddingDate] = useState(null);

  const [weddingBudget, setWeddingBudget] = useState(0);

  const [estimatedTotal, setEstimatedTotal] = useState(0);
  const [costTotal, setCostTotal] = useState(0);
  const [openBudget, setOpenBudget] = useState(false);
  const [attending, setAttending] = useState(0);
  const [favorite, setFavorite] = useState(0);
  const [hired, setHired] = useState(0);

  // const fetchUser = async () => {
  //   const res = await fetch(`http://localhost:3000/api/auth/session`)
  //   .then((response) => response.json())
  //   .then((data) => setUser(data))
  // };
  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: wedding, error: weddingErr, mutate: weddingMutate } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}` : null,
    fetcher)

  const { data: checklist, error: checklistErr, mutate: mutateList } = useSWR(session ? `http://localhost:1337/api/checklists?filters[user_id]=${session.id}&populate=*` : null,
    fetcher)
  const { data: Guests, error: guestErr } = useSWR(wedding ? `http://localhost:1337/api/guest-lists?wedding_id=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: budgets, error: budgetErr } = useSWR(wedding ? `http://localhost:1337/api/budgets?filters[wedding_id]=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: myVendors, error: myVendorsErr } = useSWR(wedding ? `http://localhost:1337/api/my-vendors?wedding_id=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: invitations, error: inviteError } = useSWR(wedding ? `http://localhost:1337/api/invitations?filters[wedding_id]=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  const { data: tableGuests, error: tableGuestsErr } = useSWR(wedding ? `http://localhost:1337/api/table-guests?filters[wedding_id]=${wedding?.data[0]?.id}&populate=*` : null,
    fetcher)
  if (weddingErr || guestErr || budgetErr) <p>Loading failed...</p>;
  if (wedding || budgets || checklist) <h1>Loading...</h1>;

  function showBudget() { setOpenBudget(!openBudget); };


  useEffect(() => {
    if (wedding) {
      const weddingDate = new Date(wedding?.data[0]?.attributes.date_wedding).getTime();
      console.log('the wedding date ' + weddingDate + "  " + wedding.data[0].attributes.date_wedding)
      setWeddingDate(wedding.data[0].attributes.date_wedding)
      setWeddingBudget(parseFloat(wedding.data[0].attributes.budget))

    }
    if (budgets) {
      const total = budgets.data?.reduce((accumulator, object) => { return accumulator + object.attributes.estimated_cost; }, 0);
      const costs = budgets.data?.reduce((accumulator, object) => { return accumulator + object.attributes.amount_paid; }, 0);
      setEstimatedTotal(parseFloat(total))
      setCostTotal(costs)
    }
    if (checklist) {
      const notDone = checklist?.data?.filter((item) => { return item.attributes.done == false })
      const isDone = checklist?.data?.filter((item) => { return item.attributes.done == true })
      setTaskOngoing(notDone.length)
      setTaskDone(isDone.length)
    }
    if (myVendors) {
      const favorite = myVendors?.data?.filter((item) => { return item.attributes.favorite == true })
      const hired = myVendors?.data?.filter((item) => { return item.attributes.hired == true })
      setFavorite(favorite.length)
      setHired(hired.length)
    }
  }, [wedding, budgets, checklist, myVendors])

  useEffect(() => {
    if (invitations) {
      const attending = invitations?.data?.filter((item) => {
        return item.attributes.status == "accepted"
      }).length
      setAttending(attending)
    }
  }, [invitations])




  return (
    <div className={styles.container}>
      <div className={styles.title_dashboard}>HOME</div>

      <section className={styles.content_top}>
        <div className={styles.count_wedding}>
          <div>Count-down to wedding</div>
          <div>
            <CountdownTimer targetDate={weddingDate} type="2" />
          </div>
        </div>
        <div className={styles.progress}>
          <span>checklist wedding: Progress</span>
          <div className={styles.linearProgress}>
            <Progress
              id={styles.progressId}
              percent={`${taskDone / checklist?.data?.length * 100}`}
              theme={{
                active: {
                  symbol: ' ',
                  color: '#CD8256',
                  trailColor: '#F0EDED',
                },
              }}
            />
          </div>
          <div className={styles.numbers}>
            <div>Completed tasks: {taskDone}</div>
            <div>Ongoing tasks: {taskOngoing}</div>
            <div>Total tasks: {checklist?.data?.length}</div>
          </div>
        </div>

      </section>
      <section className={styles.cards}>
        <Link href='/dashboard/vendors' scroll={true} passHref>
          <div className={styles.card_category}>
            <div className={styles.card_title}>
              <span>Vendors</span>
              <div className={styles.arc_icon}>
                <Image src="/Bridelinks/arc_mini_lxsf1u.svg" width={80} height={70} alt="mini decoration arc" />
                <FeatherIcon icon="shopping-cart" size={'2rem'} color="#CD8256" />
              </div>
            </div>

            <span className={styles.card_numbers}>{favorite} favorited | {hired} hired</span>
          </div>
        </Link>

        <Link href='/dashboard/guests' scroll={true} passHref>
          <div className={styles.card_category}>
            <div className={styles.card_title}>
              <span>Guests</span>
              <div className={styles.arc_icon}>
                <Image src="/Bridelinks/arc_mini_lxsf1u.svg" width={80} height={70} alt="mini decoration arc" />
                <FeatherIcon icon="users" size={'2rem'} color="#CD8256" />
              </div>
            </div>
            <span className={styles.card_numbers}>{invitations?.data?.length} invited | {attending} attending</span>
          </div>
        </Link>
        <Link href='/dashboard/table' scroll={true} passHref>
          <div className={styles.card_category}>
            <div className={styles.card_title}>
              <span>Tables</span>
              <div className={styles.arc_icon}>
                <Image src="/Bridelinks/arc_mini_lxsf1u.svg" width={80} height={70} alt="mini decoration arc" />
                <FontAwesomeIcon icon="fa-solid fa-chair" color="#CD8256" size="2xl" />
              </div>
            </div>
            <span className={styles.card_numbers}>{Guests?.data?.length} guests | {tableGuests?.data?.length} seated</span>
          </div>
        </Link>
      </section>

      <section className={styles.budget}>
        <span>Budget Statistics</span>
        <div className={styles.boxPrice}>
          <div className={styles.wedding_price}><span>Wedding budget</span><div className={styles.priceTitle}>€ {weddingBudget.toFixed(2)}</div></div>
          <div className={styles.wedding_cost}><span>Current wedding cost</span><div className={styles.priceTitle}>€ {costTotal.toFixed(2)}</div></div>

        </div>
        <ChartBudget />
      </section>
      {openBudget ? <EditBudgetPopup id={wedding.data[0].id} value={weddingBudget} mutate={weddingMutate} closePopup={showBudget} /> : null}

    </div >
  );
}




Main.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};