import styles from './style.module.scss';

export function PrimaryButton({ children }) {
  return (
    <button className={styles.primary} type="submit">
      {children}
    </button>
  );
}
