import { useState, useEffect, Children } from 'react';
import { useSession } from 'next-auth/react';
import DashboardLayout from '../../../components/layout/dashboard';
import styles from './style.module.scss';
import useSWR, { useSWRConfig } from 'swr'
import GroupGuests from '../../../components/guestComponents/groupGuests';
import MenuGuests from '../../../components/guestComponents/menuGuests';
import GuestStatus from '../../../components/guestComponents/GuestStatus';

const tabItems = [
  {
    id: 1,
    title: 'Groups',
    content: <GroupGuests/>,
  },
  {
    id: 2,
    title: 'Menu',
    content: <MenuGuests/>,
  },
  {
    id: 3,
    title: 'Attendance',
    content: <GuestStatus/>,
  }
];

export default function Guests() {
  const [currentTab, setCurrentTab] = useState(1);

  const [totalInvited, setTotalInvited] = useState(0);
  const [pending, setPending] = useState(0);
  const [refused, setRefused] = useState(0);
  const [accepted, setAccepted] = useState(0);

  const [adults, setAdult] = useState(0);
  const [children, setChild] = useState(0);
  const [babies, setBaby] = useState(0);


 const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?user_id=${session.id}` : null,
    fetcher)
    const { data: Groups, error: GroupsError } = useSWR(session ? `http://localhost:1337/api/guest-groups?filters[user_id]=${session.id}` : null,
    fetcher)

  const { data: Guests, error: GuestsError } = useSWR(wedding ? `http://localhost:1337/api/guest-lists?wedding_id=${wedding.data[0].id}&populate=*` : null,
    fetcher)

    const { data: invitations, error: inviteError } = useSWR(wedding ? `http://localhost:1337/api/invitations?filters[wedding_id]=${wedding.data[0].id}&populate=*` : null,
    fetcher)

  useEffect(() => {
    if(Guests)
  {  
    const adults = Guests?.data?.filter((item) => {return item.attributes.age == "adult" })
    const children = Guests?.data?.filter((item) => { return item.attributes.age == "child"})
    const babies = Guests?.data?.filter((item) => { return item.attributes.age == "baby" })

    setBaby(babies?.length)
    setChild(children?.length)
    setAdult(adults?.length)

  }

  if(invitations){
    const total = invitations?.data?.length
    const refused = invitations?.data?.filter((item) => { return item.attributes.status == "refused"}).length
    const pending = invitations?.data?.filter((item) => { return item.attributes.status == "pending" }).length
    const accepted = invitations?.data?.filter((item) => { return item.attributes.status == "accepted" }).length
    setTotalInvited(total)
   setRefused(refused)
   setPending(pending)
   setAccepted(accepted)
  }   
  },[invitations,Guests])


  if (GroupsError && GuestsError) <p>Loading failed...</p>;
  if (Groups) <h1>Loading...</h1>;

  return (
    <div className={styles.container}>
      <div className={styles.title_dashboard}>GUESTS LIST</div>
      <div className={styles.boxes}>
        <div className={styles.numbers_guests}>
          <div className={styles.column}>
            <div className={styles.row}>
              <p className={styles.title}>Invited</p>
              <p className={styles.number}>{totalInvited}</p>
            </div>
            <div className={styles.row}>
              <p className={styles.title}>Accepted</p>
              <p className={styles.number}>{accepted}</p>
            </div>
          </div>
          <div className={styles.vertical_line}></div>

          <div className={styles.column}>
            <div className={styles.row}>
              <p className={styles.title}>Pending</p>
              <p className={styles.number}>{pending}</p>
            </div>
            <div className={styles.row}>
              <p className={styles.title}>Refused</p>
              <p className={styles.number}>{refused}</p>
            </div>
          </div>
        </div>
        <div className={styles.identity_guests}>
          <div>
            <p className={styles.title}>Adults</p>
            <p className={styles.number}>{adults}</p>
          </div>
          <div>
            <p className={styles.title}>Children</p>
            <p className={styles.number}>{children}</p>
          </div>
          <div>
            <p className={styles.title}>Babies</p>
            <p className={styles.number}>{babies}</p>
          </div>
        </div>
      </div>
      <div className={styles.tabs}>
        {tabItems.map((tab, i) => (
          <button
            key={i}
            onClick={() => setCurrentTab(tab.id)}
            className={tab.id === currentTab ? `${styles.active}` : ''}
          >
            {tab.title}
          </button>
        ))}
      </div>

        {tabItems.map((tab, i) => {
        if (tab.id === currentTab) {
          return (
            <div key={i}>
              <div className={styles.content}>{tab.content}</div>
            </div>
          );
        } else {
          return null;
        }
      })}
    </div>
  );
}


Guests.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
