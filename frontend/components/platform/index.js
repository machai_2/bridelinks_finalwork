import Image from 'next/image';

import styles from './style.module.scss';

export default function Platform() {
  return (
    <div className={styles.container}>
      <section className={styles.section1}>
        <div className={styles.group_images}>
          <div className={styles.image1}>
            <Image
              src="/Bridelinks/dashboard_home_gbwawx.png"
              width="642"
              height="399"
              alt="Home page of the dashboard ui"
            />
          </div>
          <div className={styles.image2}>
            <Image
              src="/Bridelinks/dashboard_guests_q3pwpz.png"
              width="642"
              height="399"
              alt="guests page of the dashboard ui"
            />
          </div>
        </div>

        <div className={styles.text_right}>
          <h2>Your own wedding planner platform</h2>
          <p>
            Plan anywhere, anytime with our planning tools! Weddix is a virtual workspace for the bride, groom, and professional wedding planners. <br />
                <br/>From setting up your budget to planning your honeymoon,
            We’ll be with you every step of the way.
          </p>
        </div>
      </section>

      <section className={styles.section2}>
        <div className={styles.text_imgs}>
          <h2>Browse all our vendors</h2>
          <p>
            With the help of local vendors and venues we present a wide array of choices for your wedding!
            <br/>
            Contact them throught the website and get a proper response shortly!
          </p>
        </div>
        <div className={styles.image_vendor}>
          <Image
            src="/Bridelinks/vendor_page_dashboard_nqlxbc"
            width="642"
            height="399"
            alt="vendor page of the dashboard ui"
          />
        </div>
      </section>
    </div>
  );
}
