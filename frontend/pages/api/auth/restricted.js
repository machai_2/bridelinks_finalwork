import { getSession } from "next-auth/react";
import axios from 'axios';


export default async (req, res) => {
  const session = await getSession({ req })
  if (session.user) {
    return new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: 'http://localhost:1337/api/users/me',
        headers: {
          'Authorization': `Bearer ${session.jwt}`
        }
      };

      axios(config)
        .then(function (response) {
          res.status(200).json(response.data);
        })
        .catch((error) => {
          if (error.response) {
            console.log('error problem here ' + error.response)
            res.status(400).send(error.response);
            resolve();
          } else if (error.request) {
            console.log('error request :')
            res.status(400).send(error.request);
            resolve();
          } else {
            console.log('error content : ' + error.request)
          }
        });

    })
  } else {
    res.send({
      error: "You must be sign in to view the protected content on this page.",
    })
  }
}


