import styles from './style.module.scss';
import React, { useState } from 'react';


export function Toggle(props) {

  return (
    <div className={styles.toggle_switch}>
    <input
      type="checkbox"
      className={styles.checkbox}
      name={props.Name}
      id={props.Name}
    />
    <label className={styles.label} htmlFor={props.Name}>
      <span className={styles.inner} />
      <span className={styles.switch}/> 
    </label>
  </div>
  )
}