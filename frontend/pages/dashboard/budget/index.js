import React, { useRef, useState, useEffect, useMemo } from 'react';
import { useSession } from 'next-auth/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from '../../../components/layout/dashboard';
import { DashboardSolidButton } from './../../../components/buttons';
import styles from './style.module.scss';
import { AddCategoryBudgetPopup, DeleteCategoryPopup } from '../../../components/popup';
import useSWR, { useSWRConfig } from 'swr'
import { InputForm } from '../../../components/inputs';
import debounce from 'lodash.debounce';
import { useRouter } from 'next/router'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { SpinnerCircularFixed } from 'spinners-react';
import { EditBudgetPopup } from '../../../components/popup/EditBudgetPopup';


export default function Budget() {
  const router = useRouter()
  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: wedding, error: weddingError, mutate: weddingMutate } = useSWR(session ? `http://localhost:1337/api/weddings?filters[user_id]=${session.id}` : null,
    fetcher)
  const { data: bugetCategories, error: bugetCategoriesErr, mutate: mutateList } = useSWR(wedding ? `http://localhost:1337/api/category-budgets?wedding_id=${wedding.data[0].id}&populate=*` : null,
    fetcher)

  const { data: budgets, error: budgetErr, mutate: mutateBudget } = useSWR(wedding ? `http://localhost:1337/api/budgets?filters[wedding_id]=${wedding.data[0].id}&populate=*` : null,
    fetcher, { refreshInterval: 3000 })


  const [inputList, setInputList] = useState([]);
  const [showCategory, setShowCategory] = useState(false);
  const [loading, setLoading] = useState()
  const [selectedBudget, setSelectedBudget] = useState();
  const [itemsBudget, setItemsBudget] = useState([]);
  const [idItem, setIdItem] = useState('');
  const [nameExpense, setNameExpense] = useState('');
  const [openBudget, setOpenBudget] = useState(false);
  const [estimatedCost, setEstimatedCost] = useState(0);
  const [paidCost, setPaidCost] = useState(0);
  const [isDeleteCategory, setIsDeleteCategory] = useState(false);
  const [weddingId, setWeddingId] = useState(0);
  const [estimatedTotal, setEstimatedTotal] = useState(0);
  const [costTotal, setCostTotal] = useState(0);

  const [estimatedTotalItem, setEstimatedTotalItem] = useState(0);
  const [costTotalItem, setCostTotalItem] = useState(0);
  const [costCategories, setCostCategories] = useState(0);
  const [showDelete, setShowDelete] = useState(0);


  if (bugetCategories && bugetCategoriesErr) <p>Loading failed...</p>;
  if (bugetCategories) <h1>Loading...</h1>;
  if (!bugetCategories && bugetCategories < 0) {
    return <h1>empty...</h1>;
  }

  useEffect(() => { if (wedding) { setWeddingId(wedding.data[0].id) } }, [wedding]);

  useEffect(() => {
    if (bugetCategories) {
      setSelectedBudget(bugetCategories.data[0])
      fetchItems(bugetCategories?.data[0]?.id)
    }
  }, [bugetCategories]);

  useEffect(() => {
    if (itemsBudget) {
      const total = itemsBudget.data?.reduce((accumulator, object) => { return accumulator + object.attributes.estimated_cost; }, 0);
      const costs = itemsBudget.data?.reduce((accumulator, object) => { return accumulator + object.attributes.amount_paid; }, 0);
      setEstimatedTotalItem(total)
      setCostTotalItem(costs)
    }
  }, [itemsBudget]);

  useEffect(() => {
    if (budgets) {
      const total = budgets.data?.reduce((accumulator, object) => { return accumulator + object.attributes.estimated_cost; }, 0);
      const costs = budgets.data?.reduce((accumulator, object) => { return accumulator + object.attributes.amount_paid; }, 0);
      setEstimatedTotal(total)
      setCostTotal(costs)
    }
  }, [budgets])

  useEffect(() => {
    if (nameExpense && estimatedCost && paidCost && selectedBudget) {
      debounceCreateHandle(nameExpense, estimatedCost, paidCost, selectedBudget.id)
    }
  }, [nameExpense, estimatedCost, paidCost]);

  const showDeleteCategory = () => {
    setIsDeleteCategory(!isDeleteCategory);
  }
  function showAddCategoryInput() {
    setShowCategory(!showCategory);
  }

  function showDeleteItem(id) {
    document.getElementById(`delete-${id}`).remove();
    axios.delete(`http://localhost:1337/api/budgets/${id}`)
      .then(response => {
        mutateBudget()
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const handleClick = (item) => {
    console.log('onclick handle ckli')
    setSelectedBudget(item);
    fetchItems(item.id);
  };

  const fetchItems = async (id) => {
    setLoading(true);
    const res = await fetch(`http://localhost:1337/api/budgets?filters[category_budget_id]=${id}&populate=*`);
    const data = await res.json()
    setItemsBudget(data)
    setLoading(false);
  };


  const FormInput = () => {
    return (<div className={styles.card}>
      <div>
        <InputForm placeholder="expense name" onChange={(e) => { setNameExpense(e.target.value) }} ></InputForm>
      </div>
      <div className={styles.price} >
        <InputForm placeholder="0.00 €" type="number" onChange={(e) => { setEstimatedCost(e.target.value) }} ></InputForm>
      </div>
      <div className={styles.price}>
        <InputForm placeholder="0.00 €" type="number" onChange={(e) => { setPaidCost(e.target.value) }} ></InputForm>
      </div>
    </div>)
  };

  function AddItem() { setInputList(inputList.concat(<FormInput key={inputList.length} />)); }

  const debounceUpdateHandle = useMemo((name, val, id) => debounce(updateItem, 1000), []);
  const debounceCreateHandle = useMemo((name, cost, paid, cat) => debounce(CreateItem, 1000), []);

  const handleChange = async (name, val, id) => {
    setNameExpense(val);
    setIdItem(id)
    debounceUpdateHandle(name, id, val)
  }

  function updateItem(name, id, val) {
    axios.put(`http://localhost:1337/api/budgets/${id}`, { data: { [name]: val } }).then(response => {
    })
      .catch((error) => {
        console.log(error);
      });
  }

  async function CreateItem(name, cost, paid, cat) {
    const budgetItem = {
      name_expense: name,
      estimated_cost: cost,
      amount_paid: paid,
      category_budget_id: cat,
      wedding_id: weddingId
    }
    try {
      const response = await fetch(`http://localhost:1337/api/budgets?category_budget_id=${selectedBudget}`, {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: budgetItem })
      })
    } catch (error) {
      console.log(error)
    }
    router.reload(window.location.pathname)
  }
  // if (loading) return <div className={styles.spinner}><SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} /></div>

  return (
    <div className={styles.container}>
      <div className={styles.title_dashboard}>YOUR WEDDING BUDGET</div>
      <span>Keep track of your spending and total budget.</span>
      <section className={styles.section2}>
        <div className={styles.section2_content}>
          <div className={styles.listItems}>
            <div className={styles.add_category_button}>
              <DashboardSolidButton onClick={showAddCategoryInput}><FontAwesomeIcon icon="fa-solid fa-plus" /> add a category</DashboardSolidButton>
              {showCategory ? <AddCategoryBudgetPopup id={wedding.data[0].id} mutate={mutateList} closePopup={showAddCategoryInput} /> : null}
            </div>
            {bugetCategories && bugetCategories?.data?.map((item, index) => (
              <div className={selectedBudget?.attributes?.category_nl_id.data?.id == item.attributes.category_nl_id.data?.id ? `${styles.active_item}` : `${styles.item}`} key={index} onClick={() => handleClick(item)}>
                <div>{item.attributes.category_nl_id.data?.attributes.name != null ? `${item.attributes.category_nl_id.data?.attributes.name}` : `${item.attributes.name}`}</div>
                <div></div>
              </div>
            ))}
          </div>
          <div className={styles.item_detail}>
            <div className={styles.header}>
              <div className={styles.title_detail}>{selectedBudget?.attributes?.category_nl_id.data?.attributes.name != null ? `${selectedBudget.attributes.category_nl_id.data?.attributes.name}` : `${selectedBudget?.attributes?.name}`}</div>
              <button onClick={showDeleteCategory} className={styles.deleteCategory}>
                <FontAwesomeIcon icon="fa-solid fa-trash" />
              </button>
              {isDeleteCategory ? <DeleteCategoryPopup id={selectedBudget.id} onChange={(a) => { selectedBudget.id }} mutate={mutateList} closePopup={showDeleteCategory} /> : null}
            </div>
            <div className={styles.container_items}>
              <div className={styles.column_wrapper}>
                <span>Items summary</span>
                <span>Estimated budget</span>
                <span>Paid</span>
              </div>
              <hr />
              <div className={styles.card_wrapper}>
                {itemsBudget.data?.map((items, index) => (
                  <div className={styles.card} key={index} id={`delete-${items.id}`}>
                    <div>
                      <InputForm defaultValue={items.attributes.name_expense.toLocaleString(navigator.language, { minimumFractionDigits: 2 })} onChange={(e) => handleChange('name_expense', e.target.value, items.id)}></InputForm>
                    </div>
                    <div className={styles.price}>
                      <InputForm defaultValue={"€ " + items.attributes.estimated_cost.toLocaleString(navigator.language, { minimumFractionDigits: 2 })} onChange={(e) => handleChange('estimated_cost', e.target.value, items.id)}></InputForm>
                    </div>
                    <div className={styles.price}>
                      <InputForm defaultValue={"€ " + items.attributes.amount_paid.toLocaleString(navigator.language, { minimumFractionDigits: 2 })} onChange={(e) => handleChange('amount_paid', e.target.value, items.id)}></InputForm>
                    </div>
                    <button onClick={() => showDeleteItem(items.id)} className={styles.deleteGuest}>
                      <FontAwesomeIcon icon="fa-solid fa-xmark" />
                    </button>
                  </div>
                ))
                }
              </div>
              <div>
                {inputList}

                <DashboardSolidButton onClick={AddItem}>Add item</DashboardSolidButton>
              </div>
            </div>
            <div className={styles.bottomWrapper}>
              <div className={styles.total}>Total:</div>
              <div>€ {estimatedTotalItem ? estimatedTotalItem.toLocaleString(navigator.language, { minimumFractionDigits: 2 }) : ''}</div>
              <div>€ {costTotalItem ? costTotalItem.toLocaleString(navigator.language, { minimumFractionDigits: 2 }) : ''}</div>
            </div>
          </div>

        </div>
      </section>

    </div>
  );
}

Budget.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
