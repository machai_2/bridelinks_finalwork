import styles from './style.module.scss';
import React from 'react';
import { InputSearchVendors } from './../../../inputs';

export function Page4({ data, setData, list }) {
      return (
            <div className={styles.container}>
                  <h2>Where do you plan on holding the wedding?</h2>
                  <div>
                        <div className={styles.content_container}>
                              The location is
                              <div>
                                    <InputSearchVendors list={list} onchange={(event, value) => setData({ ...data, location: value })}>City</InputSearchVendors>
                              </div>
                        </div>
                  </div>
            </div>
      )
}
