'use strict';

/**
 * vendor-nl router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::vendor-nl.vendor-nl');
