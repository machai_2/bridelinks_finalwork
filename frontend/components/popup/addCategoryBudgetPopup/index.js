import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import axios from 'axios';
import { DashboardSolidButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useRouter } from 'next/router'

export function AddCategoryBudgetPopup(props) {
  const [budgetCategory, setBudgetCategory] = useState("");
  const router = useRouter()

  const handleSubmit = async (e) => {
    e.preventDefault();

    console.log('group popup wedding id ' + props.id + "  " + budgetCategory)

    const budget_group = {
      name: budgetCategory,
      wedding_id: props.id
    }
    try {
      const response = await fetch(`http://localhost:1337/api/category-budgets?wedding_id=${props.id}`, {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: budget_group })

      })
    } catch (error) {
      console.log(error)
    }
    router.reload(window.location.pathname)

    props.closePopup()
  };
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h4>Create a new category</h4>
            <section>
              <InputForm type='text' placeholder="Category name" value={budgetCategory} onChange={(e) => { setBudgetCategory(e.target.value) }} />
            </section>

            <div className={styles.submitClose}>
              <DashboardSolidButton type="submit">Save</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
