import styles from './style.module.scss';

const list = [
  { name: 'John Doe', status: 'Accepted', table: 34, diet: 'Vegan' },
  { name: 'John Doe', status: 'Accepted', table: 34, diet: 'Vegan' },
  { name: 'John Doe', status: 'Accepted', table: 34, diet: 'Vegan' },
];


export function GuestCard() {
  

  return (
    { list.map((item,index) => (   
         <div key={index} className={styles.guest_item}>
        <div className={styles.person_data}>{item.name}</div>
        <div className={styles.person_data}>
          {item.status} <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
        </div>
        <div className={styles.person_data}>
          <FontAwesomeIcon icon="fa-solid fa-phone" className={styles.icon} />
          <FontAwesomeIcon icon="fa-solid fa-envelope" className={styles.icon} />
          <FontAwesomeIcon icon="fa-solid fa-home" className={styles.icon} />
        </div>
        <div className={styles.person_data}>{item.table}</div>
        <div className={styles.person_data}>{item.diet}</div>
      </div>
    ))}
  );
}
