import './../assets/styles/globals.scss';
import '@fortawesome/fontawesome-svg-core/styles.css';
import './../assets/icons';
import { appWithTranslation } from 'next-i18next';

import { config } from '@fortawesome/fontawesome-svg-core';
import { SessionProvider } from 'next-auth/react';
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
config.autoAddCss = false;

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const MyApp = ({ Component, pageProps: { session, ...pageProps } }) => {
  const getLayout = Component.getLayout || ((page) => page);
  return (
    <>
      <SessionProvider session={session}>{getLayout(<Component {...pageProps}></Component>)}</SessionProvider>
    </>
  );
}
export default appWithTranslation(MyApp)