
import styles from './style.module.scss';
import React from 'react';
import { InputForm } from './../../../inputs';

export function Page2({ data, setData }) {
  return (
    <div className={styles.container}>
      <h2>What is the name of your business ?</h2>
      <div>
        <div className={styles.content_container}>
          <div>
          </div><InputForm type='text' value={data.name} onChange={(e) => setData({ ...data, name: e.target.value })} placeholder="business name" />
        </div>
      </div>
    </div>
  )

}
