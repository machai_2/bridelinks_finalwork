import styles from './style.module.scss';

export function DashboardSolidButton( props ) {
  return <button className={styles.dashboard_button} type={props.type} onClick={props.onClick}>{props.children}</button>;
}
