'use strict';

/**
 *  category-budget controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::category-budget.category-budget');
