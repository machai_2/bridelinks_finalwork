import React, { useState } from 'react';
import styles from './style.module.scss'
import {RegisterPopup} from '../../popup';


export function RegisterButton() {
  const [OpenPopup, setOpenPopup] = useState(false);

  const showPopup = () => {
    setOpenPopup(!OpenPopup);
  };

  return (
    <div>
      <button onClick={showPopup} className={styles.register_btn}>Join us</button>
      {OpenPopup ? <RegisterPopup closePopup={showPopup} /> : null}
    </div>
  );
}
