import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { useRouter } from 'next/router';

export function AddTablePopup(props) {
  const [seats, setSeats] = useState();
  const [tablenumber, setTablenumber] = useState();
  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      table_number: tablenumber,
      seats,
      wedding_id: props.weddingId
    }
    try {
      const response = await fetch('http://localhost:1337/api/tables', {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data })
      })
      props.mutate()

    } catch (error) {
      console.log(error)
    }
    props.closePopup()
  };



  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h4>Create a new table</h4>
            <section>
              <InputForm type='text' placeholder="table number" id={tablenumber} onChange={(e) => { setTablenumber(e.target.value) }} />
              <InputForm type='text' placeholder="number of seats" id={seats} onChange={(e) => { setSeats(e.target.value) }} />
            </section>
            <div className={styles.submitClose}>
              <DashboardSolidButton type="submit" >Submit</DashboardSolidButton>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
