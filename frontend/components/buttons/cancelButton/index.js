import styles from './style.module.scss';

export function CancelButton(props) {
  return <button className={styles.btn} onClick={props.onClick}>{props.children}</button>;
}
