/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./assets/icons/index.js":
/*!*******************************!*\
  !*** ./assets/icons/index.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ \"@fortawesome/fontawesome-svg-core\");\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ \"@fortawesome/free-brands-svg-icons\");\n/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ \"@fortawesome/free-regular-svg-icons\");\n/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ \"@fortawesome/free-solid-svg-icons\");\n/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\nconst icons = [\n    _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faHandshake,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faPen,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faEllipsisVertical,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faTrash,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faMinus,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faCalendar,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faCamera,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faGear,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faCircleCheck,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faFilter,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faUserGroup,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faUsers,\n    _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faHeart,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faPhone,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faEnvelope,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faStar,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faChair,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faCalculator,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faHouse,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faListCheck,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faLocationDot,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faShop,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faUserGroup,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faBell,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faXmark,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faAppStoreIos,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faFacebookSquare,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faGooglePlay,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faBriefcase,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faStar,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faInstagram,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faPinterestSquare,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faTwitterSquare,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faChevronDown,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faChevronUp,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faCopyright,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faUser,\n    _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faGoogle,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faChevronRight,\n    _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__.faArrowRightFromBracket\n];\n_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_0__.library.add(...icons);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hc3NldHMvaWNvbnMvaW5kZXguanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQTREO0FBU2hCO0FBQytCO0FBK0JoQztBQUUzQyxNQUFNdUMsS0FBSyxHQUFHO0lBQ1o5Qiw0RUFBVztJQUNYQyxvRUFBSztJQUNMRSxpRkFBa0I7SUFDbEJPLHNFQUFPO0lBQ1BXLHNFQUFPO0lBQ1BuQix5RUFBVTtJQUNWSyx1RUFBUTtJQUNSVSxxRUFBTTtJQUNOWiw0RUFBYTtJQUNiVyx1RUFBUTtJQUNSVSwwRUFBVztJQUNYQyxzRUFBTztJQUNQNUIsd0VBQU87SUFDUHVCLHNFQUFPO0lBQ1BQLHlFQUFVO0lBQ1ZTLHFFQUFNO0lBQ05oQixzRUFBTztJQUNQRiwyRUFBWTtJQUNaWSxzRUFBTztJQUNQQywwRUFBVztJQUNYQyw0RUFBYTtJQUNiRyxxRUFBTTtJQUNORywwRUFBVztJQUNYdEIscUVBQU07SUFDTndCLHNFQUFPO0lBQ1BwQyw2RUFBYTtJQUNiQyxnRkFBZ0I7SUFDaEJDLDRFQUFZO0lBQ1plLDBFQUFXO0lBQ1hlLHFFQUFNO0lBQ041QiwyRUFBVztJQUNYQyxpRkFBaUI7SUFDakJGLCtFQUFlO0lBQ2ZnQiw0RUFBYTtJQUNiRSwwRUFBVztJQUNYQywwRUFBVztJQUNYVyxxRUFBTTtJQUNOM0Isd0VBQVE7SUFDUmMsNkVBQWM7SUFDZGlCLHNGQUF1QjtDQUN4QjtBQUVEdEMsMEVBQVcsSUFBSXVDLEtBQUssQ0FBQyxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYnJpZGVsaW5rcy8uL2Fzc2V0cy9pY29ucy9pbmRleC5qcz82NDA3Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGxpYnJhcnkgfSBmcm9tICdAZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtc3ZnLWNvcmUnO1xyXG5pbXBvcnQge1xyXG4gIGZhQXBwU3RvcmVJb3MsXHJcbiAgZmFGYWNlYm9va1NxdWFyZSxcclxuICBmYUdvb2dsZVBsYXksXHJcbiAgZmFUd2l0dGVyU3F1YXJlLFxyXG4gIGZhSW5zdGFncmFtLFxyXG4gIGZhUGludGVyZXN0U3F1YXJlLFxyXG4gIGZhR29vZ2xlXHJcbn0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZyZWUtYnJhbmRzLXN2Zy1pY29ucyc7XHJcbmltcG9ydCB7IGZhSGVhcnQsIGZhSGFuZHNoYWtlIH0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZyZWUtcmVndWxhci1zdmctaWNvbnMnO1xyXG5pbXBvcnQge1xyXG4gIGZhUGVuLFxyXG4gIGZhQ2FsZW5kYXIsXHJcbiAgZmFFbGxpcHNpc1ZlcnRpY2FsLFxyXG4gIGZhQmVsbCxcclxuICBmYUNpcmNsZUNoZWNrLFxyXG4gIGZhQ2FsY3VsYXRvcixcclxuICBmYUNhbWVyYSxcclxuICBmYUNoYWlyLFxyXG4gIGZhQnJpZWZjYXNlLFxyXG4gIGZhVHJhc2gsXHJcbiAgZmFDaGV2cm9uRG93bixcclxuICBmYUNoZXZyb25SaWdodCxcclxuICBmYUNoZXZyb25VcCxcclxuICBmYUNvcHlyaWdodCxcclxuICBmYUVudmVsb3BlLFxyXG4gIGZhRmlsdGVyLFxyXG4gIGZhR2VhcixcclxuICBmYUhvdXNlLFxyXG4gIGZhTGlzdENoZWNrLFxyXG4gIGZhTG9jYXRpb25Eb3QsXHJcbiAgZmFNaW51cyxcclxuICBmYVBob25lLFxyXG4gIGZhU2hvcCxcclxuICBmYVN0YXIsXHJcbiAgZmFVc2VyLFxyXG4gIGZhVXNlckdyb3VwLFxyXG4gIGZhVXNlcnMsXHJcbiAgZmFYbWFyayxcclxuICBmYUFycm93UmlnaHRGcm9tQnJhY2tldFxyXG59IGZyb20gJ0Bmb3J0YXdlc29tZS9mcmVlLXNvbGlkLXN2Zy1pY29ucyc7XHJcblxyXG5jb25zdCBpY29ucyA9IFtcclxuICBmYUhhbmRzaGFrZSxcclxuICBmYVBlbixcclxuICBmYUVsbGlwc2lzVmVydGljYWwsXHJcbiAgZmFUcmFzaCxcclxuICBmYU1pbnVzLFxyXG4gIGZhQ2FsZW5kYXIsXHJcbiAgZmFDYW1lcmEsXHJcbiAgZmFHZWFyLFxyXG4gIGZhQ2lyY2xlQ2hlY2ssXHJcbiAgZmFGaWx0ZXIsXHJcbiAgZmFVc2VyR3JvdXAsXHJcbiAgZmFVc2VycyxcclxuICBmYUhlYXJ0LFxyXG4gIGZhUGhvbmUsXHJcbiAgZmFFbnZlbG9wZSxcclxuICBmYVN0YXIsXHJcbiAgZmFDaGFpcixcclxuICBmYUNhbGN1bGF0b3IsXHJcbiAgZmFIb3VzZSxcclxuICBmYUxpc3RDaGVjayxcclxuICBmYUxvY2F0aW9uRG90LFxyXG4gIGZhU2hvcCxcclxuICBmYVVzZXJHcm91cCxcclxuICBmYUJlbGwsXHJcbiAgZmFYbWFyayxcclxuICBmYUFwcFN0b3JlSW9zLFxyXG4gIGZhRmFjZWJvb2tTcXVhcmUsXHJcbiAgZmFHb29nbGVQbGF5LFxyXG4gIGZhQnJpZWZjYXNlLFxyXG4gIGZhU3RhcixcclxuICBmYUluc3RhZ3JhbSxcclxuICBmYVBpbnRlcmVzdFNxdWFyZSxcclxuICBmYVR3aXR0ZXJTcXVhcmUsXHJcbiAgZmFDaGV2cm9uRG93bixcclxuICBmYUNoZXZyb25VcCxcclxuICBmYUNvcHlyaWdodCxcclxuICBmYVVzZXIsXHJcbiAgZmFHb29nbGUsXHJcbiAgZmFDaGV2cm9uUmlnaHQsXHJcbiAgZmFBcnJvd1JpZ2h0RnJvbUJyYWNrZXRcclxuXTtcclxuXHJcbmxpYnJhcnkuYWRkKC4uLmljb25zKTtcclxuIl0sIm5hbWVzIjpbImxpYnJhcnkiLCJmYUFwcFN0b3JlSW9zIiwiZmFGYWNlYm9va1NxdWFyZSIsImZhR29vZ2xlUGxheSIsImZhVHdpdHRlclNxdWFyZSIsImZhSW5zdGFncmFtIiwiZmFQaW50ZXJlc3RTcXVhcmUiLCJmYUdvb2dsZSIsImZhSGVhcnQiLCJmYUhhbmRzaGFrZSIsImZhUGVuIiwiZmFDYWxlbmRhciIsImZhRWxsaXBzaXNWZXJ0aWNhbCIsImZhQmVsbCIsImZhQ2lyY2xlQ2hlY2siLCJmYUNhbGN1bGF0b3IiLCJmYUNhbWVyYSIsImZhQ2hhaXIiLCJmYUJyaWVmY2FzZSIsImZhVHJhc2giLCJmYUNoZXZyb25Eb3duIiwiZmFDaGV2cm9uUmlnaHQiLCJmYUNoZXZyb25VcCIsImZhQ29weXJpZ2h0IiwiZmFFbnZlbG9wZSIsImZhRmlsdGVyIiwiZmFHZWFyIiwiZmFIb3VzZSIsImZhTGlzdENoZWNrIiwiZmFMb2NhdGlvbkRvdCIsImZhTWludXMiLCJmYVBob25lIiwiZmFTaG9wIiwiZmFTdGFyIiwiZmFVc2VyIiwiZmFVc2VyR3JvdXAiLCJmYVVzZXJzIiwiZmFYbWFyayIsImZhQXJyb3dSaWdodEZyb21CcmFja2V0IiwiaWNvbnMiLCJhZGQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./assets/icons/index.js\n");

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _assets_styles_globals_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../assets/styles/globals.scss */ \"./assets/styles/globals.scss\");\n/* harmony import */ var _assets_styles_globals_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_assets_styles_globals_scss__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _fortawesome_fontawesome_svg_core_styles_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core/styles.css */ \"./node_modules/@fortawesome/fontawesome-svg-core/styles.css\");\n/* harmony import */ var _fortawesome_fontawesome_svg_core_styles_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_svg_core_styles_css__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _assets_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../assets/icons */ \"./assets/icons/index.js\");\n/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next-i18next */ \"next-i18next\");\n/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_i18next__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ \"@fortawesome/fontawesome-svg-core\");\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next-auth/react */ \"next-auth/react\");\n/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_auth_react__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var nprogress__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! nprogress */ \"nprogress\");\n/* harmony import */ var nprogress__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(nprogress__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var nprogress_nprogress_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! nprogress/nprogress.css */ \"./node_modules/nprogress/nprogress.css\");\n/* harmony import */ var nprogress_nprogress_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(nprogress_nprogress_css__WEBPACK_IMPORTED_MODULE_9__);\n\n\n\n\n\n\n\n\n\n\n_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__.config.autoAddCss = false;\nnext_router__WEBPACK_IMPORTED_MODULE_7___default().events.on(\"routeChangeStart\", ()=>nprogress__WEBPACK_IMPORTED_MODULE_8___default().start()\n);\nnext_router__WEBPACK_IMPORTED_MODULE_7___default().events.on(\"routeChangeComplete\", ()=>nprogress__WEBPACK_IMPORTED_MODULE_8___default().done()\n);\nnext_router__WEBPACK_IMPORTED_MODULE_7___default().events.on(\"routeChangeError\", ()=>nprogress__WEBPACK_IMPORTED_MODULE_8___default().done()\n);\nconst MyApp = ({ Component , pageProps: { session , ...pageProps }  })=>{\n    const getLayout = Component.getLayout || ((page)=>page\n    );\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(next_auth_react__WEBPACK_IMPORTED_MODULE_6__.SessionProvider, {\n            session: session,\n            children: getLayout(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                ...pageProps\n            }, void 0, false, {\n                fileName: \"C:\\\\Users\\\\chai1\\\\OneDrive\\\\Documents\\\\bridelinks_finalwork\\\\frontend\\\\pages\\\\_app.js\",\n                lineNumber: 21,\n                columnNumber: 53\n            }, undefined))\n        }, void 0, false, {\n            fileName: \"C:\\\\Users\\\\chai1\\\\OneDrive\\\\Documents\\\\bridelinks_finalwork\\\\frontend\\\\pages\\\\_app.js\",\n            lineNumber: 21,\n            columnNumber: 7\n        }, undefined)\n    }, void 0, false);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,next_i18next__WEBPACK_IMPORTED_MODULE_4__.appWithTranslation)(MyApp));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFBeUM7QUFDYTtBQUMzQjtBQUN1QjtBQUVTO0FBQ1Q7QUFDakI7QUFDQztBQUNEO0FBQ2pDQyxnRkFBaUIsR0FBRyxLQUFLLENBQUM7QUFFMUJFLDREQUFnQixDQUFDLGtCQUFrQixFQUFFLElBQU1DLHNEQUFlLEVBQUU7QUFBQSxDQUFDLENBQUM7QUFDOURELDREQUFnQixDQUFDLHFCQUFxQixFQUFFLElBQU1DLHFEQUFjLEVBQUU7QUFBQSxDQUFDLENBQUM7QUFDaEVELDREQUFnQixDQUFDLGtCQUFrQixFQUFFLElBQU1DLHFEQUFjLEVBQUU7QUFBQSxDQUFDLENBQUM7QUFFN0QsTUFBTU0sS0FBSyxHQUFHLENBQUMsRUFBRUMsU0FBUyxHQUFFQyxTQUFTLEVBQUUsRUFBRUMsT0FBTyxHQUFFLEdBQUdELFNBQVMsRUFBRSxHQUFFLEdBQUs7SUFDckUsTUFBTUUsU0FBUyxHQUFHSCxTQUFTLENBQUNHLFNBQVMsSUFBSSxDQUFDLENBQUNDLElBQUksR0FBS0EsSUFBSTtJQUFBLENBQUM7SUFDekQscUJBQ0U7a0JBQ0UsNEVBQUNiLDREQUFlO1lBQUNXLE9BQU8sRUFBRUEsT0FBTztzQkFBR0MsU0FBUyxlQUFDLDhEQUFDSCxTQUFTO2dCQUFFLEdBQUdDLFNBQVM7Ozs7O3lCQUFjLENBQUM7Ozs7O3FCQUFtQjtxQkFDdkcsQ0FDSDtDQUNIO0FBQ0QsaUVBQWVaLGdFQUFrQixDQUFDVSxLQUFLLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9icmlkZWxpbmtzLy4vcGFnZXMvX2FwcC5qcz9lMGFkIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAnLi8uLi9hc3NldHMvc3R5bGVzL2dsb2JhbHMuc2Nzcyc7XHJcbmltcG9ydCAnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLXN2Zy1jb3JlL3N0eWxlcy5jc3MnO1xyXG5pbXBvcnQgJy4vLi4vYXNzZXRzL2ljb25zJztcclxuaW1wb3J0IHsgYXBwV2l0aFRyYW5zbGF0aW9uIH0gZnJvbSAnbmV4dC1pMThuZXh0JztcclxuXHJcbmltcG9ydCB7IGNvbmZpZyB9IGZyb20gJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1zdmctY29yZSc7XHJcbmltcG9ydCB7IFNlc3Npb25Qcm92aWRlciB9IGZyb20gJ25leHQtYXV0aC9yZWFjdCc7XHJcbmltcG9ydCBSb3V0ZXIgZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5pbXBvcnQgTlByb2dyZXNzIGZyb20gJ25wcm9ncmVzcyc7XHJcbmltcG9ydCAnbnByb2dyZXNzL25wcm9ncmVzcy5jc3MnO1xyXG5jb25maWcuYXV0b0FkZENzcyA9IGZhbHNlO1xyXG5cclxuUm91dGVyLmV2ZW50cy5vbigncm91dGVDaGFuZ2VTdGFydCcsICgpID0+IE5Qcm9ncmVzcy5zdGFydCgpKTtcclxuUm91dGVyLmV2ZW50cy5vbigncm91dGVDaGFuZ2VDb21wbGV0ZScsICgpID0+IE5Qcm9ncmVzcy5kb25lKCkpO1xyXG5Sb3V0ZXIuZXZlbnRzLm9uKCdyb3V0ZUNoYW5nZUVycm9yJywgKCkgPT4gTlByb2dyZXNzLmRvbmUoKSk7XHJcblxyXG5jb25zdCBNeUFwcCA9ICh7IENvbXBvbmVudCwgcGFnZVByb3BzOiB7IHNlc3Npb24sIC4uLnBhZ2VQcm9wcyB9IH0pID0+IHtcclxuICBjb25zdCBnZXRMYXlvdXQgPSBDb21wb25lbnQuZ2V0TGF5b3V0IHx8ICgocGFnZSkgPT4gcGFnZSk7XHJcbiAgcmV0dXJuIChcclxuICAgIDw+XHJcbiAgICAgIDxTZXNzaW9uUHJvdmlkZXIgc2Vzc2lvbj17c2Vzc2lvbn0+e2dldExheW91dCg8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9PjwvQ29tcG9uZW50Pil9PC9TZXNzaW9uUHJvdmlkZXI+XHJcbiAgICA8Lz5cclxuICApO1xyXG59XHJcbmV4cG9ydCBkZWZhdWx0IGFwcFdpdGhUcmFuc2xhdGlvbihNeUFwcCkiXSwibmFtZXMiOlsiYXBwV2l0aFRyYW5zbGF0aW9uIiwiY29uZmlnIiwiU2Vzc2lvblByb3ZpZGVyIiwiUm91dGVyIiwiTlByb2dyZXNzIiwiYXV0b0FkZENzcyIsImV2ZW50cyIsIm9uIiwic3RhcnQiLCJkb25lIiwiTXlBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiLCJzZXNzaW9uIiwiZ2V0TGF5b3V0IiwicGFnZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./assets/styles/globals.scss":
/*!************************************!*\
  !*** ./assets/styles/globals.scss ***!
  \************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/@fortawesome/fontawesome-svg-core/styles.css":
/*!*******************************************************************!*\
  !*** ./node_modules/@fortawesome/fontawesome-svg-core/styles.css ***!
  \*******************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/nprogress/nprogress.css":
/*!**********************************************!*\
  !*** ./node_modules/nprogress/nprogress.css ***!
  \**********************************************/
/***/ (() => {



/***/ }),

/***/ "@fortawesome/fontawesome-svg-core":
/*!****************************************************!*\
  !*** external "@fortawesome/fontawesome-svg-core" ***!
  \****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/fontawesome-svg-core");

/***/ }),

/***/ "@fortawesome/free-brands-svg-icons":
/*!*****************************************************!*\
  !*** external "@fortawesome/free-brands-svg-icons" ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ "@fortawesome/free-regular-svg-icons":
/*!******************************************************!*\
  !*** external "@fortawesome/free-regular-svg-icons" ***!
  \******************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ "@fortawesome/free-solid-svg-icons":
/*!****************************************************!*\
  !*** external "@fortawesome/free-solid-svg-icons" ***!
  \****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ "next-auth/react":
/*!**********************************!*\
  !*** external "next-auth/react" ***!
  \**********************************/
/***/ ((module) => {

"use strict";
module.exports = require("next-auth/react");

/***/ }),

/***/ "next-i18next":
/*!*******************************!*\
  !*** external "next-i18next" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next-i18next");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ "nprogress":
/*!****************************!*\
  !*** external "nprogress" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("nprogress");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();