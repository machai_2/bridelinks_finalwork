import Image from 'next/image';
import { DownloadButton } from './../buttons';
import styles from './style.module.scss';

export default function Download() {
  return (
    <section className={styles.container} id="Download">
      <div className={styles.split}>
        <div className={styles.couple_image}>
          <Image src="/Bridelinks/arc_couple_ht7ojo.svg" width={562} height={714} alt="couple with forehead touching" />
        </div>
        <div className={styles.text_download}>
          <h2>Plan everywhere and everywhere with the app</h2>
          <p>Wedding Planner and Best friend in one:</p>
          <div className={styles.buttonGroup}>
            <DownloadButton size="xl" icon="fa-brands fa-app-store-ios">
              Download on the Appstore
            </DownloadButton>
            <DownloadButton size="xl" icon="fa-brands fa-google-play">
              Download on the Playstore
            </DownloadButton>
          </div>
        </div>
      </div>
    </section>
  );
}
