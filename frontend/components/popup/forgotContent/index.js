export function ForgotContent() {
  return (
    <>
      <h2>Forgot password</h2>
      <p>Enter your email address and we'll send you a link to change your password.</p>
      <form>
        <input type="email" id="email" name="email" placeholder="Email" ></input>
        <button type="submit">Request a new password</button>
      </form>
    </>
  );

}