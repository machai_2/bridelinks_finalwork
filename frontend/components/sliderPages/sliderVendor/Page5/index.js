
import styles from './style.module.scss';
import React, { useState } from 'react';
import { InputForm, InputSearchVendors } from './../../../inputs';


export function Page5({ data, setData }) {
      return (
            <div className={styles.container}>
                  <h2>What is your average starting price?</h2>
                  <div>
                        <div className={styles.content_container}>
                              <div>
                                    <InputForm onchange={(event, value) => setData({ ...data, price: value })} placeholder="€ starting price" />
                              </div>
                        </div>
                  </div>
            </div>
      )
}
