"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/auth/restricted";
exports.ids = ["pages/api/auth/restricted"];
exports.modules = {

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ "next-auth/react":
/*!**********************************!*\
  !*** external "next-auth/react" ***!
  \**********************************/
/***/ ((module) => {

module.exports = require("next-auth/react");

/***/ }),

/***/ "(api)/./pages/api/auth/restricted.js":
/*!**************************************!*\
  !*** ./pages/api/auth/restricted.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next-auth/react */ \"next-auth/react\");\n/* harmony import */ var next_auth_react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_auth_react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);\n\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{\n    const session = await (0,next_auth_react__WEBPACK_IMPORTED_MODULE_0__.getSession)({\n        req\n    });\n    if (session.user) {\n        return new Promise((resolve, reject)=>{\n            var config = {\n                method: \"get\",\n                url: \"http://localhost:1337/api/users/me\",\n                headers: {\n                    \"Authorization\": `Bearer ${session.jwt}`\n                }\n            };\n            axios__WEBPACK_IMPORTED_MODULE_1___default()(config).then(function(response) {\n                res.status(200).json(response.data);\n            }).catch((error)=>{\n                if (error.response) {\n                    console.log(\"error problem here \" + error.response);\n                    res.status(400).send(error.response);\n                    resolve();\n                } else if (error.request) {\n                    console.log(\"error request :\");\n                    res.status(400).send(error.request);\n                    resolve();\n                } else {\n                    console.log(\"error content : \" + error.request);\n                }\n            });\n        });\n    } else {\n        res.send({\n            error: \"You must be sign in to view the protected content on this page.\"\n        });\n    }\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9wYWdlcy9hcGkvYXV0aC9yZXN0cmljdGVkLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQTZDO0FBQ25CO0FBRzFCLGlFQUFlLE9BQU9FLEdBQUcsRUFBRUMsR0FBRyxHQUFLO0lBQ2pDLE1BQU1DLE9BQU8sR0FBRyxNQUFNSiwyREFBVSxDQUFDO1FBQUVFLEdBQUc7S0FBRSxDQUFDO0lBQ3pDLElBQUlFLE9BQU8sQ0FBQ0MsSUFBSSxFQUFFO1FBQ2hCLE9BQU8sSUFBSUMsT0FBTyxDQUFDLENBQUNDLE9BQU8sRUFBRUMsTUFBTSxHQUFLO1lBQ3RDLElBQUlDLE1BQU0sR0FBRztnQkFDWEMsTUFBTSxFQUFFLEtBQUs7Z0JBQ2JDLEdBQUcsRUFBRSxvQ0FBb0M7Z0JBQ3pDQyxPQUFPLEVBQUU7b0JBQ1AsZUFBZSxFQUFFLENBQUMsT0FBTyxFQUFFUixPQUFPLENBQUNTLEdBQUcsQ0FBQyxDQUFDO2lCQUN6QzthQUNGO1lBRURaLDRDQUFLLENBQUNRLE1BQU0sQ0FBQyxDQUNWSyxJQUFJLENBQUMsU0FBVUMsUUFBUSxFQUFFO2dCQUN4QlosR0FBRyxDQUFDYSxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUNDLElBQUksQ0FBQ0YsUUFBUSxDQUFDRyxJQUFJLENBQUMsQ0FBQzthQUNyQyxDQUFDLENBQ0RDLEtBQUssQ0FBQyxDQUFDQyxLQUFLLEdBQUs7Z0JBQ2hCLElBQUlBLEtBQUssQ0FBQ0wsUUFBUSxFQUFFO29CQUNsQk0sT0FBTyxDQUFDQyxHQUFHLENBQUMscUJBQXFCLEdBQUdGLEtBQUssQ0FBQ0wsUUFBUSxDQUFDO29CQUNuRFosR0FBRyxDQUFDYSxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUNPLElBQUksQ0FBQ0gsS0FBSyxDQUFDTCxRQUFRLENBQUMsQ0FBQztvQkFDckNSLE9BQU8sRUFBRSxDQUFDO2lCQUNYLE1BQU0sSUFBSWEsS0FBSyxDQUFDSSxPQUFPLEVBQUU7b0JBQ3hCSCxPQUFPLENBQUNDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDOUJuQixHQUFHLENBQUNhLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQ08sSUFBSSxDQUFDSCxLQUFLLENBQUNJLE9BQU8sQ0FBQyxDQUFDO29CQUNwQ2pCLE9BQU8sRUFBRSxDQUFDO2lCQUNYLE1BQU07b0JBQ0xjLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLGtCQUFrQixHQUFHRixLQUFLLENBQUNJLE9BQU8sQ0FBQztpQkFDaEQ7YUFDRixDQUFDLENBQUM7U0FFTixDQUFDO0tBQ0gsTUFBTTtRQUNMckIsR0FBRyxDQUFDb0IsSUFBSSxDQUFDO1lBQ1BILEtBQUssRUFBRSxpRUFBaUU7U0FDekUsQ0FBQztLQUNIO0NBQ0YiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9icmlkZWxpbmtzLy4vcGFnZXMvYXBpL2F1dGgvcmVzdHJpY3RlZC5qcz9kNzA3Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGdldFNlc3Npb24gfSBmcm9tIFwibmV4dC1hdXRoL3JlYWN0XCI7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgY29uc3Qgc2Vzc2lvbiA9IGF3YWl0IGdldFNlc3Npb24oeyByZXEgfSlcclxuICBpZiAoc2Vzc2lvbi51c2VyKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICB2YXIgY29uZmlnID0ge1xyXG4gICAgICAgIG1ldGhvZDogJ2dldCcsXHJcbiAgICAgICAgdXJsOiAnaHR0cDovL2xvY2FsaG9zdDoxMzM3L2FwaS91c2Vycy9tZScsXHJcbiAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgJ0F1dGhvcml6YXRpb24nOiBgQmVhcmVyICR7c2Vzc2lvbi5qd3R9YFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIGF4aW9zKGNvbmZpZylcclxuICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgIHJlcy5zdGF0dXMoMjAwKS5qc29uKHJlc3BvbnNlLmRhdGEpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgaWYgKGVycm9yLnJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlcnJvciBwcm9ibGVtIGhlcmUgJyArIGVycm9yLnJlc3BvbnNlKVxyXG4gICAgICAgICAgICByZXMuc3RhdHVzKDQwMCkuc2VuZChlcnJvci5yZXNwb25zZSk7XHJcbiAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoZXJyb3IucmVxdWVzdCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3IgcmVxdWVzdCA6JylcclxuICAgICAgICAgICAgcmVzLnN0YXR1cyg0MDApLnNlbmQoZXJyb3IucmVxdWVzdCk7XHJcbiAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlcnJvciBjb250ZW50IDogJyArIGVycm9yLnJlcXVlc3QpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfSlcclxuICB9IGVsc2Uge1xyXG4gICAgcmVzLnNlbmQoe1xyXG4gICAgICBlcnJvcjogXCJZb3UgbXVzdCBiZSBzaWduIGluIHRvIHZpZXcgdGhlIHByb3RlY3RlZCBjb250ZW50IG9uIHRoaXMgcGFnZS5cIixcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcblxyXG5cclxuIl0sIm5hbWVzIjpbImdldFNlc3Npb24iLCJheGlvcyIsInJlcSIsInJlcyIsInNlc3Npb24iLCJ1c2VyIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJjb25maWciLCJtZXRob2QiLCJ1cmwiLCJoZWFkZXJzIiwiand0IiwidGhlbiIsInJlc3BvbnNlIiwic3RhdHVzIiwianNvbiIsImRhdGEiLCJjYXRjaCIsImVycm9yIiwiY29uc29sZSIsImxvZyIsInNlbmQiLCJyZXF1ZXN0Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///(api)/./pages/api/auth/restricted.js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("(api)/./pages/api/auth/restricted.js"));
module.exports = __webpack_exports__;

})();