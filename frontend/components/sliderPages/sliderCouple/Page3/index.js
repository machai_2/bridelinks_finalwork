import styles from './style.module.scss';
import React from 'react';
import { InputForm } from '../../../inputs';

export function Page3({ data, setData }) {
  return (
    <div className={styles.container}>
      <h2>Got it! What is your estimated budget and guests ?</h2>
      <div>
        <div className={styles.content_container}>

          <section>
            <span>
              We are inviting
            </span>
            <div className={styles.inputForm}>
              <InputForm type='text' value={data.invites} onChange={(e) => { setData({ ...data, invites: e.target.value }) }} placeholder="invited" />

            </div>
            <span>
              people.
            </span>
          </section>
          <section>
            <span>Our budget is</span>
            <div className={styles.inputForm}>
              <InputForm type='text' value={data.budget} onChange={(e) => { setData({ ...data, budget: e.target.value }) }} placeholder="budget €" />

            </div>
          </section>
        </div>
      </div>
    </div>
  )
}
