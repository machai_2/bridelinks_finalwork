import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import { InputStyle } from './../inputStyle';

export function SelectGroups(props) {
  const classes = InputStyle();

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel variant="standard" htmlFor="uncontrolled-native">
        </InputLabel>
        <NativeSelect
          onChange={props.onChange}
          inputProps={{
            id: 'uncontrolled-native',
          }}
          value={props.value}>
          {props.options?.map((item, index) => (
            <option value={item.attributes.name} defaultValue={props.defaultValue}
              key={index} className={classes.root}>{item.attributes.name}</option>
          ))}
        </NativeSelect>
      </FormControl>
    </Box>
  );
}