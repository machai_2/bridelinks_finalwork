import axios from 'axios';
import { data } from './../guests/groups_init.js';

export default async (req, res) => {

  data.forEach(element => {
    axios.post(`http://localhost:1337/api/guest-groups`, {
      data:
      {
        user_id: req.body.user_id,
        name: element.name,
      }
    }).then(response => {
      res.status(200).end();
    })
      .catch(function (err) {
        if (err.response) {
          console.log(err);
          console.log(err.response.data.error.name);
          res.status(400).send(err);
        }
      });
  })
}