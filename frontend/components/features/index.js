import FeatherIcon from 'feather-icons-react';
import Image from 'next/image';
import { PrimaryButton } from '../buttons';
import styles from './style.module.scss';
const cards = require('./../../helpers/cards_data.js');

export default function Features() {
  return (
    <section className={styles.container} id="Features">
      <div className={styles.img_arc}>
        <Image
          src="/Bridelinks/wide_arc_awne6y"
          width={2000}
          height={2000}
          layout="fixed"
          alt="beige decorative arc svg"
        />
      </div>
      <div className={styles.content_features}>
        <div className={styles.text_features}>
          <h2>Features</h2>
          <p>
            Plan anywhere, anytime with our planning tools! Weddix is a virtual workspace for the bride, groom, and professional wedding planners.
          </p>
        </div>

        <div className={styles.cards}>
          {cards.data.map((items, index) => (
            <div className={styles.card_item} key={index}>
              <p className={styles.title}>{items.title}</p>
              <FeatherIcon icon={items.icon} size={'3rem'} color="#AF5520" />
              <p className={styles.text}>{items.description}</p>
            </div>
          ))}
        </div>
        <div className={styles.started}>
          <PrimaryButton>GET STARTED</PrimaryButton>
        </div>
      </div>
    </section>
  );
}
