'use strict';

/**
 *  plus-one controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::plus-one.plus-one');
