'use strict';

/**
 * vendor-fr service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::vendor-fr.vendor-fr');
