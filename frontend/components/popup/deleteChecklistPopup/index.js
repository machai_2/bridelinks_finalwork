import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { DeleteButton, CancelButton } from '../../buttons';
import axios from 'axios';


export function DeleteChecklistPopup(props) {

  const deleteTasks = async ()  => {
    axios.delete(`http://localhost:1337/api/checklists/${props.id}`).then(
    ).then(response => {
      props.mutate()
    }).catch(function (error) {
      console.log(error);
    });
    props.closePopup()
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            <h3>Are you sure you want the task?</h3>
            <span>This cannot be undone</span>
            <div className={styles.buttonGroup}>
              <CancelButton onClick={props.closePopup}>Cancel</CancelButton>
              <DeleteButton id={props.id} onClick={deleteTasks}>Delete</DeleteButton>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
