import Slider from '@mui/material/Slider';
import React, { useState } from 'react';

// const prices = [
//   {
//     value: 0,
//     scaledValue: 0,
//   },
//   {
//     value: 20,
//     scaledValue: 2000,
//   },
//   {
//     value: 60,
//     scaledValue: 6000,
//   },
//   {
//     value: 80,
//     scaledValue: 8000,
//   },
//   {
//     value: 100,
//     scaledValue: 10000,
//   },
// ];

// const scale = (value) => {
//   const previousMarkIndex = Math.floor(value / 25);
//   const previousMark = prices[previousMarkIndex];
//   const remainder = value % 25;

//   if (remainder === 0) {
//     return previousMark.scaledValue;
//   }
//   const nextMark = prices[previousMarkIndex + 1];
//   const increment = (nextMark.scaledValue - previousMark.scaledValue) / 25;

//   return remainder * increment + previousMark.scaledValue;
// };

// function numFormatter(num) {
//   if (num > 999 && num < 1000000) {
//     return (num / 1000).toFixed(0) + 'K'; // convert to K for number from > 1000 < 1 million
//   } else if (num >= 1000000) {
//     return (num / 1000000).toFixed(0) + 'M'; // convert to M for number from > 1 million
//   } else if (num < 900) {
//     return num; // if value < 1000, nothing to do
//   }
// }

function valuetext(value) {
  return `${value}€`;
}

export default function SliderPrice() {
  const [startPrice, setStartPrice] = useState(0);
  const [endPrice, setEndPrice] = useState(3000);

  const handleChange = (event, value) => {
    setStartPrice(value[0]);
    setEndPrice(value[1]);
    console.log("slider price " + value[0] + "    " + value[1])

  };


  return (
    <div>
      <Slider
        sx={{
          color: '#AF4720',
          margin: '0em 1.5rem',
          width: '11rem',
        }}
        key={`slider`}
        getAriaLabel={(index) => (index === 0 ? 'Minimum price' : 'Maximum price')}
        getAriaValueText={valuetext}
        min={200}
        max={3000}
        defaultValue={[startPrice, endPrice]}
        onChange={handleChange}
        step={100}
        valueLabelDisplay="on"
      />
      {/* <br />
      <br />
      <br />
      <p>min value {startPrice}</p>
      <p>max value {endPrice}</p> */}
    </div>
  );
}
