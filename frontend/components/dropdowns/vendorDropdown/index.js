import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { signOut, useSession } from 'next-auth/react';
import React, { useRef, useState, useEffect } from 'react';
import { useDetectOutsideClick } from '../../../helpers/useDetectOutside.Js';
import styles from './style.module.scss';

export const menuItems = [
  {
    title: 'Profile',
  },
  {
    title: 'My Wedding Dashboard',
  },
  {
    title: 'Settings',
  },
  {
    title: 'Logout',
  },
];

export function VendorDropdown() {
  const { data: session } = useSession();
  const dropdownRef = useRef(null);
  const [OpenDropdown, setOpenDropdown] = useDetectOutsideClick(dropdownRef, false);
  const [user, setUser] = useState();
  const [error, setError] = useState();

  const showDropdown = () => {
    setOpenDropdown(!OpenDropdown);
  };

  const fetchUser = async () => {
    const res = await fetch(`http://localhost:3000/api/auth/restricted`);
    const data = await res.json();
    setUser(data);
  };

  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <div>
      <button ref={dropdownRef} className={styles.containerButton} onClick={showDropdown}>
        <FontAwesomeIcon icon="fa-solid fa-user" />
        <span className={styles.userName}>{user?.firstname == undefined ? `${session?.user.name}` : `${user.firstname} ${user.lastname}`}</span>
        <FontAwesomeIcon
          className={`${OpenDropdown ? styles.ArrowUp : styles.ArrowDown}`}
          size="xs"
          icon="fa-solid fa-chevron-down"
        />
      </button>

      {OpenDropdown ? (
        <div className={styles.dropdownMenu}>
          <div className={styles.menuItem}>
            <li>
              <Link href="/dashboard-vendor/profile">
                <a>Profile</a>
              </Link>
            </li>
            <li>
              <Link href="/dashboard-vendor/main">
                <a>My vendor Dashboard</a>
              </Link>
            </li>
          </div>
          <button className={styles.logoutButton} onClick={() => signOut()}>Logout</button>
        </div>
      ) : null}
    </div>
  );
}
