import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef,useState} from 'react';
import {DeleteGroupPopup,EditGroupPopup} from '../../popup';
import styles from './style.module.scss';
import { useDetectOutsideClick } from './../../../helpers/useDetectOutside.js'

export function EditDropdowns(props) {
  const dropdownRef = useRef(null);
  const [OpenDropdown, setOpenDropdown] = useDetectOutsideClick(dropdownRef, false);
  const [rename, setRename] = useState(false);
  const [remove, setRemove] = useState(false);


  const list = [{ title: 'rename', icon: 'fa-pen' }, { title: 'remove', icon: 'fa-trash' }];

  const showDropdown = () => {
    setOpenDropdown(!OpenDropdown);
  };

  const showDeletePopup = () => {
    setRemove(false);
  };

  const showEditPopup = () => {
    setRename(false);
  };

  const ValueSelected = (selected) => () => {
    setOpenDropdown(false);
  };
  const PopupButtons = (selected) => () => {
    console.log(selected)
    if (selected == 'rename') {
      setRename(!rename)
    }
    if (selected == 'remove') {
      setRemove(!rename)
    }
  }
  return (
    <div>
      <button ref={dropdownRef} className={styles.container} onClick={showDropdown}>
        <FontAwesomeIcon icon="fa-solid fa-ellipsis-vertical" size="xs" />
      </button>
      {rename ? (<EditGroupPopup groupname={props.groupname} groupId={props.groupId} closePopup={showEditPopup} mutate={props.mutate}/>) : null}
      {remove ? (<DeleteGroupPopup groupname={props.groupname} groupId={props.groupId} closePopup={showDeletePopup} mutate={props.mutate}/>) : null}
      {OpenDropdown ? (
        <div className={styles.dropdown}>
          {list.map((item) => (
            <li onClick={ValueSelected(item.title)} className={styles.item} key={item.title}>
              <FontAwesomeIcon icon={`fa-solid ${item.icon}`} size="xs" /> <a onClick={PopupButtons(item.title)}>{item.title}</a>
            </li>
          ))}


        </div>
      ) : null}
    </div>
  );
}

