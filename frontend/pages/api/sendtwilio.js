const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require("twilio")(accountSid, authToken);

// let SMSBody = `Hi ${req.body.name}, you are invited to my wedding`;


const sendtwilio = (body) => {
  console.log("body twilio " + JSON.stringify(body))
  client.messages
    .create({
      body: body.txt,
      from: "+19044478993",
      to: "+32" + body.number,
    })
    .then((message) => console.log('\x1b[32m', "[SMS SENT]", message.sid))
    .done();
};

module.exports = sendtwilio;