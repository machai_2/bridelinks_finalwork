
import axios from 'axios';

export default async (req, res) => {
  const { token } = req.body;
  axios.get(`http://localhost:1337/api/invitations?filters[token][$eq]=${token}`, {
  }).then(async (response) => {
    const invitation = response.data.data[0]

    console.log('found token in refuse ' + JSON.stringify(invitation))
    axios.put(`http://localhost:1337/api/invitations/${invitation.id}`, {
      data: { status: "refused", read: true }
    }).then(response => { console.log('Invitation ', response); res.status(200).end(); }).
      catch(function (err) { if (err.response) { console.log(err); res.status(400).send(err); } });

  }).catch(function (err) {
    if (err.response) {
      console.log(err);
      console.log(err);
      console.log(err.response.data.error.name);
      res.status(400).send(err);
    }
  });
};