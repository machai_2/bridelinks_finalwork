import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import axios from 'axios';
import { DashboardOutlineButton, DashboardSolidButton, PrimaryButton } from '../../buttons';
import { InputForm } from '../../inputs';
import styles from './style.module.scss';
import { signOut, useSession } from 'next-auth/react';
import useSWR, { mutate, useSWRConfig } from 'swr'
import { useRouter } from 'next/router'

export function AddGroupPopup(props) {
  const [groupName, setGroupName] = useState("");
  const router = useRouter()

  const handleSubmit = async (e) => {

    e.preventDefault();
    const user_data = {
      name: groupName,
      user_id: props.id
    }
    try {
      const response = await fetch('http://localhost:1337/api/guest-groups/', {
        method: 'POST',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: user_data })

      })
      props.mutate()
    } catch (error) {
      console.log(error)
    }
    props.closePopup()
  };
  return (
    <div className={styles.bg_popup}>
      <div className={styles.popup_area}>
        <div className={styles.close}>
          <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
        </div>
        <div className={styles.popup_body}>
          <form onSubmit={handleSubmit}>
            <h4>Create a new group</h4>
            <section>
              <InputForm type='text' placeholder="group name" id={groupName} value={groupName} onChange={(e) => { setGroupName(e.target.value) }} />
              <div className={styles.submitClose}>
                <DashboardSolidButton type="submit">Save</DashboardSolidButton>
              </div>
            </section>
          </form>
        </div>
      </div>
    </div>
  );
}
