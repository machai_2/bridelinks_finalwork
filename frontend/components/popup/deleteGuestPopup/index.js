import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './style.module.scss';
import { DeleteButton, CancelButton } from '../../buttons';
import axios from 'axios';


export function DeleteGuestPopup(props) {

  console.log('Guest id ' + props.id)

  const deleteTasks = () => {
    axios.delete(`http://localhost:1337/api/guest-lists/${props.id}`).then(
    ).then(response => {
      props.mutate()
    }).catch(function (error) {
      console.log(error);
    });
    props.closePopup()
  }

  return (
    <>
      <div className={styles.bg_popup}>
        <div className={styles.popup_area}>
          <div className={styles.close}>
            <FontAwesomeIcon icon="fa-solid fa-xmark" onClick={props.closePopup} size="lg" />
          </div>
          <div className={styles.popup_body}>
            <h3>Are you sure you want to delete the guest?</h3>
            <span>All guest information will be deleted .</span>
            <span>This cannot be undone</span>
            <div className={styles.buttonGroup}>
              <CancelButton onClick={props.closePopup}>Cancel</CancelButton>
              <DeleteButton onClick={deleteTasks}>Delete</DeleteButton>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
