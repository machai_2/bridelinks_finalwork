
import React, { useState } from 'react';
import { SliderButtons } from './../../../components/buttons';
import { Page1, Page2, Page3, Page4, Page5 } from './../../../components/sliderPages/sliderVendor';
import styles from './style.module.scss';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';
import { fetchAPI } from './../../../lib/api';
import axios from 'axios';

function getUniqueArray(_array) {
  let newArray = _array.filter((element, index, array) => array.indexOf(element) === index);
  return newArray
}


export default function Questions({ vendors, categories }) {
  const router = useRouter();
  const filter_location = vendors.map(i => i.attributes.location.split(',')[1].trim())
  const unique_location = getUniqueArray(filter_location)

  let unique_categories = categories.map(i => i.attributes.name)

  const { data: session } = useSession();
  const [page, setPage] = useState(0);
  const [data, setData] = useState({
    firstname: "",
    lastname: "",
    name: "",
    location: "",
    category: "",
    price: 0
  });

  const pages = ["page1", "page2", "page3", "page4", "page5"];

  const handleNext = () => {
    if (page === pages.length - 1) {

      axios.put(`http://localhost:1337/api/users/${session.id}`, {
        firstname: data.firstname,
        lastname: data.lastname
      })
        .then(response => {
          console.log('response ', response.data);
        }).catch(err => {
          console.log(err);
        });

      axios.post(`http://localhost:1337/api/weddings`, {
        data: {
          partner_firstname: data.partner_firstname,
          partner_lastname: data.partner_lastname,
          date_wedding: data.date_wedding,
          invites: data.invites,
          budget: data.budget,
          user_id: session.id,
          location: data.location
        }
      })
        .then(response => {
          console.log('response ', response.data);
        }).catch(err => {
          // Handle error
          console.log(err.response);
          console.log(err.request);
          console.log(err.message);

        });

      axios.post('http://localhost:3000/api/checklist/init', { id: session.id, period: data.date_wedding })
        .then(function (response) {
          console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
          console.log(error);
        });

      axios.post('http://localhost:3000/api/guests/init', { user_id: session.id })
        .then(function (response) {
          console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
          console.log(error);
        });

      router.replace('/dashboard-vendor/main');

    } else {
      setPage((currPage) => currPage + 1);
    }

  }

  const backButton = () => { setPage((currPage) => currPage - 1); }

  const PageDisplay = () => {
    if (page === 0) {
      return <Page1 data={data} setData={setData} />;
    } else if (page === 1) {
      return <Page2 data={data} setData={setData} />;
    } else if (page === 2) {
      return <Page3 data={data} setData={setData} list={unique_categories} />;
    } else if (page === 3) {
      return <Page4 data={data} setData={setData} list={unique_location} />;
    }

    else {
      return <Page5 data={data} setData={setData} />;

    }
  };

  const progress = () => {
    if (page == 0) {
      return "20%"
    }
    if (page == 1) {
      return "40%"
    }
    if (page == 2) {
      return "60%"
    }
    if (page == 3) {
      return "80%"
    }
    if (page == 4) {
      return "100%"
    }
  }


  return (
    <div className={styles.container}>
      <div className={styles.logo}>&</div>
      <div>
        <div className={styles.progressbar}>
          <div
            style={{ width: progress() }}
          >
          </div>
        </div>
        <div className={styles.container_content}>
          {PageDisplay()}
        </div>
      </div>


      <div className={styles.buttons_group}>
        <button className={styles.backBtn} onClick={backButton} disabled={page == 0}>back</button>
        <SliderButtons onClick={handleNext} disabled={data.length == 0}> {page === pages.length - 1 ? "Submit" : "Next"}</SliderButtons>

      </div>
    </div>
  );
}

export async function getStaticProps() {
  const [allvendors, allcategories] = await Promise.all([
    fetchAPI("/vendor-nls?populate=*"),
    fetchAPI(`/category-nls?populate=*`),

  ]);
  return {
    props: {
      vendors: allvendors.data,
      categories: allcategories.data

    }
  }
}