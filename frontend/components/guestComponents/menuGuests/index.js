import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSession } from 'next-auth/react';
import DashboardLayout from '../../../components/layout/dashboard';
import styles from './../style.module.scss';
import { useState, useEffect } from 'react';
import useSWR, { useSWRConfig } from 'swr'
import { AddGuestPopup, DeleteGuestPopup, EditGuestPopup } from '../../../components/popup';
import { EditDropdowns } from '../../../components/dropdowns/editDropdown';
import { DashboardSolidButton } from '../../buttons';
import { InputSearchGuests } from '../../inputs';
import { SpinnerCircularFixed } from 'spinners-react';


export default function MenuGuests() {

  const [OpenGuestPopup, setOpenGuestPopup] = useState(false);
  const [OpenGroupPopup, setOpenGroupPopup] = useState(false);
  const [deleteGuestPopup, setDeleteGuestPopup] = useState(false);
  const [filterInvite, setFilterInvite] = useState([]);
  const [loading, setLoading] = useState(false);
  const [editGuestPopup, setEditGuestPopup] = useState(false);
  const [idItem, setIdItem] = useState();


  const [searchQuery, setSearchQuery] = useState(null);

  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())

  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?user_id=${session.id}` : null,
    fetcher)
  const { data: Groups, error: GroupsError, mutate: mutateGroups } = useSWR(session ? `http://localhost:1337/api/guest-groups?filters[user_id]=${session.id}` : null,
    fetcher)

  const { data: guests, error: GuestsError, mutate: mutateGuests } = useSWR(wedding ? `http://localhost:1337/api/guest-lists?wedding_id=${wedding.data[0].id}&populate=*` : null,
    fetcher)


  const filteredGuests = guests ? guests.data?.filter((el) => {
    if (searchQuery == null) {
      return el;
    }
    else {
      return el.attributes.firstname.toLowerCase().includes(searchQuery);
    }
  }) : [];

  if (GuestsError) <p>Loading failed...</p>;
  if (guests) <h1>Loading...</h1>;

  const showGuest = () => {
    setOpenGuestPopup(!OpenGuestPopup);
  };

  const showGroup = () => {
    setOpenGroupPopup(!OpenGroupPopup);
  };
  const showDeleteGuest = (id) => {
    setIdItem(id)
    setDeleteGuestPopup(!deleteGuestPopup);
  };
  const showEditGuest = (id) => {
    setIdItem(id)
    setEditGuestPopup(!editGuestPopup);
  };
  let inputHandler = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    setSearchQuery(lowerCase);
  };

  const diets = [
    {
      label: "None",
      value: "None",
    },
    {
      label: "Vegetarian",
      value: "Vegetarian",
    },
    {
      label: "Pescatarian",

      value: "Pescatarian",
    },
    {
      value: "Hallal",
      label: "Hallal",

    },
    {
      label: "Vegan",
      value: "Vegan",
    },
    {
      label: "Kosher",
      value: "Kosher",
    }
  ]

  useEffect(() => {
    GuestStatus()
  }, [guests]);


  async function GuestStatus() {
    let temp = [];
    setLoading(true);
    const invitePromise = guests && guests?.data?.map(async (item) => {
      let response = await fetch(`http://localhost:1337/api/invitations?filters[guest_id]=${item.id}&populate=*`);
      const data = await response.json();
      temp.push(data.data[0])
    })

    await Promise.all(invitePromise)
    console.log('the ivnite promiiise ' + JSON.stringify(invitePromise))
    setFilterInvite(temp)
    setLoading(false);

  }
  if (loading) return <div className={styles.spinner}><SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} /></div>

  return (
    <div className={styles.container}>
      <div className={styles.header}>

        <div className={styles.group_search}>
          <div className={styles.search}>
            <InputSearchGuests onChange={inputHandler} value={searchQuery} >Search your guests here</InputSearchGuests>
          </div>
        </div>
        <div className={styles.buttonAlone}>
          <DashboardSolidButton onClick={showGuest}>add a guest</DashboardSolidButton>
          {OpenGuestPopup ? <AddGuestPopup id={wedding.data[0].id} mutate={mutateGuests} list={Groups} closePopup={showGuest} /> : null}
        </div>
      </div>
      <section className={styles.main_content}>
        {diets &&
          diets.map((items, i) => (

            <div className={styles.main_body} key={i}>
              <div>
                <div className={styles.title_category}>
                  <div>
                    {items.label}

                    <span>{filteredGuests?.filter(el => el.attributes.diet === items.label).length}</span>
                  </div>
                </div>
                <div className={styles.labels}>
                  <div className={styles.label_item}>
                    <span>NAME</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>
                    <span>STATUS</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>

                  </div>
                  <div className={styles.label_item}>
                    <span>TABLE</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                  <div className={styles.label_item}>
                    <span>DIET</span>
                    <div className={styles.icon_arrows}>
                      <FontAwesomeIcon icon="fa-solid fa-chevron-up" size="2xs" />
                      <FontAwesomeIcon icon="fa-solid fa-chevron-down" size="2xs" />
                    </div>
                  </div>
                </div>

              </div>
              {filteredGuests
                && filteredGuests?.filter(el => el.attributes.diet === items.label).map((item, index) => (
                  <div key={index} className={styles.guest_item}>
                    <div className={styles.person_data}>{item.attributes.firstname} {item.attributes.lastname}</div>
                    <div className={styles.person_data}>
                      {filterInvite.filter(el => el.attributes.guest_id.data.id == item.id)
                        .map((item, index) => item.attributes.status)}
                    </div>
                    <div className={styles.person_data}>
                      <FontAwesomeIcon icon="fa-solid fa-phone" className={item.attributes.phone_number ? `${styles.active}` : `${styles.disabled}`} />
                      <FontAwesomeIcon icon="fa-solid fa-envelope" className={item.attributes.email ? `${styles.active}` : `${styles.disabled}`} />
                      <FontAwesomeIcon icon="fa-solid fa-home" className={item.attributes.street ? `${styles.active}` : `${styles.disabled}`} />
                    </div>
                    <div className={styles.person_data}>{item.attributes.table ? `${item.attributes.table}` : `none`}</div>
                    <div className={styles.person_data}>{item.attributes.diet}</div>
                    <div>
                      <button onClick={() => showDeleteGuest(item.id)} className={styles.deleteGuest}>
                        <FontAwesomeIcon icon="fa-solid fa-trash" />

                      </button>
                    </div>
                    {deleteGuestPopup ? <DeleteGuestPopup mutate={mutateGuests} id={idItem} closePopup={showDeleteGuest} /> : null}
                    {editGuestPopup ? <EditGuestPopup list={Groups} mutate={mutateGuests} id={idItem} closePopup={showEditGuest} /> : null}

                  </div>

                ))}
            </div>
          ))}
      </section>
    </div>
  );
}


MenuGuests.getLayout = function getLayout(page) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
