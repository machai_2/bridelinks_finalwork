'use strict';

/**
 * my-vendor service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::my-vendor.my-vendor');
