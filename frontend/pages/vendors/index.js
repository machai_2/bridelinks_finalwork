import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Rating from '@mui/material/Rating';
import Image from 'next/image';
import Router, { useRouter } from 'next/router';
import React, { useState, useEffect } from 'react';
import { PrimaryButton } from '../../components/buttons';
import { CollapseSite } from '../../components/Collapse';
import { SortVendorDropdown } from '../../components/dropdowns/sortVendorDropdown';
import Footer from '../../components/footer';
import Header from '../../components/header';
import { InputFilter, InputSearchVendors } from '../../components/inputs';
import SliderPrice from '../../components/sliderPrice';
import { fetchAPI } from '../../lib/api';
import styles from './style.module.scss';
import ReactPaginate from 'react-paginate';
import Link from 'next/link';
import { IoIosHeart, IoIosHeartEmpty } from "react-icons/io";
import { useSession } from 'next-auth/react';
import useSWR from 'swr'
import { SpinnerCircularFixed } from 'spinners-react';

function Vendors({ allvendors, allcategories }) {

  const router = useRouter();
  const { data: session } = useSession();

  const fetcher = (...args) => fetch(args).then((res) => res.json())
  const { data: wedding, error: weddingError } = useSWR(session ? `http://localhost:1337/api/weddings?user_id=${session.id}` : null,
    fetcher)

  const [selectedCategory, setSelectedCategory] = useState(null)
  const [selectedCity, setSelectedCity] = useState(null)
  const [selectedRating, setSelectedRating] = useState(0)
  const [selectLike, setSelectLike] = useState(false)
  const [categoriesToShow, setcategoriesToShow] = useState(6);
  const [results, setResults] = useState(allvendors)
  const [perPage, setPerPage] = useState(12);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [isLoading, setLoading] = useState(false);
  const [favorites, setFavorites] = useState([]);

  const [startPrice, setStartPrice] = useState(0);
  const [endPrice, setEndPrice] = useState(3000);

  let content = null;

  let unique_categories = allcategories.map(i => i.attributes.name)
  let unique_cities = allvendors && allvendors?.filter((element, index, array) => array.indexOf(element) === index).map(i => i.attributes.location.split(',')[1].trim())

  const startLoading = () => setLoading(true);
  const stopLoading = () => setLoading(false);

  const showMore = () => {
    categoriesToShow === 6 ? setcategoriesToShow(unique_categories.length) : setcategoriesToShow(6);
  };


  const handlePageClick = (event) => {
    let page = event.selected;
    setCurrentPage(page)
  }


  const addLiked = (e) => {
    let temp = favorites;
    let added = true;
    temp.map((item, index) => {
      if (item === e.item.attributes.id_link) {
        temp.splice(index, 1)
        added = false;
      }
    })
    if (added) {
      temp.push(e.item.attributes.id_link);
    }
    setFavorites([...temp])
  }


  const applyFilters = () => {
    let updatedList = allvendors;
    if (selectedCategory) {
      updatedList = updatedList.filter(
        (item) => {
          return item.attributes.category_nl_id.data.attributes.name == selectedCategory
        }
      );
    }

    // if (selectedPrice) {
    // updatedList = updatedList.filter(
    //   (item) => {
    //     return item.attributes.price== selectedPrice
    //   }
    // );
    // }
    if (selectedRating) {
      console.log('SELECTED RATING ' + selectedRating)
      updatedList = updatedList.filter(
        (item) => {
          return item.attributes.rating === selectedRating
        }
      );
    }
    if (selectedCity) {
      updatedList = updatedList.filter(
        (item) => {
          return item.attributes.location.split(',').pop().trim() === selectedCity
        }
      );
    }
    setResults(updatedList);
    setTotalPages(Math.floor(updatedList.length / perPage));
  };

  useEffect(() => {
    if (router.isReady) {
      applyFilters();
    }
  }, [selectedCategory, selectedCity, selectedRating, router.isReady]);

  useEffect(() => {
    if (router.isReady) {
      if (router.query.category) {
        setSelectedCategory(router.query.category)
        applyFilters()
      }
      if (router.query.location) {
        setSelectedCity(router.query.location)
        applyFilters()
      }
    }
  }, [router.isReady]);

  useEffect(() => {
    console.log('the favorites ' + favorites)
    favorites.map((item, index) => (
      saveFavorite(item)

    ))
  }, [favorites])

  useEffect(() => {
    const getFavorites = JSON.parse(localStorage.getItem('favorites') || '0')
    if (getFavorites !== 0) {
      setFavorites([getFavorites])
    }
  }, [])

  const saveFavorite = async (link) => {
    const objectData = {
      favorite: true,
      wedding_id: wedding.data[0].id,
      id_link: link
    }
    const response = await fetch(`http://localhost:1337/api/my-vendors?filters[id_link]=${link}`)
    let data = await response.json();

    if (data.data.length) {
      const response = await fetch('http://localhost:1337/api/my-vendors/' + data.data[0].id, {
        method: 'PUT',
        mode: 'cors',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ data: objectData })
      })
      console.log(response);
    } else {
      try {
        const response = await fetch('http://localhost:1337/api/my-vendors/', {
          method: 'POST',
          mode: 'cors',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ data: objectData })
        })
        console.log(response);
      } catch (error) {
        console.log(error)
      }
    }
  }


  useEffect(() => {
    Router.events.on('routeChangeStart', startLoading);
    Router.events.on('routeChangeComplete', stopLoading);
    return () => {
      Router.events.off('routeChangeStart', startLoading);
      Router.events.off('routeChangeComplete', stopLoading);
    }
  })

  if (isLoading)
    content = <SpinnerCircularFixed color='#af4720' secondaryColor={'rgba(0,0,0,0.1)'} />;
  else {
    content = (

      results.slice(currentPage * perPage, (currentPage + 1) * perPage).map((item, index) => (
        <div className={styles.card_item} key={index}>
          <div className={styles.icons}>
            <div className={styles.star}>
              <FontAwesomeIcon icon="fa-solid fa-star" />
              {item.attributes.rating} {favorites.includes(item.attributes.id_link)}
            </div>
            {favorites.includes(item.attributes.id_link) ?
              <div className={styles.favorite}>
                <IoIosHeart size={25} onClick={() => addLiked({ item, index })}></IoIosHeart>
              </div>
              : (
                <div className={styles.favorite}>
                  <IoIosHeartEmpty size={25} onClick={() => addLiked({ item, index })}></IoIosHeartEmpty>
                </div>
              )
            }
          </div>
          <Link href={`/vendors/${item.id}`}>
            <a>
              <div className={styles.card_image}>
                <img
                  src={item.attributes.image_profile != "/newsletter/cover-huwelijk-be.jpg" ? `https://www.mariage.be/${item.attributes.image_profile}` : 'https://res.cloudinary.com/dmj6tqnkw/image/upload/v1661716622/Bridelinks/BRIDELINKS_white_byfilg.jpg'}
                  alt="cover huwelijk"
                  width={240}
                  height={200}
                />
              </div>

              <div className={styles.card_detail}>
                <p>{item.attributes.category_nl_id.data.attributes.name}</p>
                <p className={styles.vendor_name}>{item.attributes.name[0].toUpperCase() + item.attributes.name.slice(1).toLowerCase()}</p>
                <div className={styles.location}>
                  <FontAwesomeIcon icon="fa-solid fa-location-dot" />
                  <p>
                    <span className={styles.city}>{item.city}</span>{item.attributes.location ? item.attributes.location.split(',').pop() : ''} | Starting price € {item.attributes.price}.00
                  </p>
                </div>
              </div>
            </a>
          </Link>
        </div>
      )))

  }

  return (
    <div>
      <div className={styles.container}>
        <Header />
        <div className={styles.banner}>
          <div>
            <Image
              src="/Bridelinks/bannerVendors_bbimc1.png"
              alt="wedding cake"
              width={1820}
              layout="responsive"
              height={160}
              priority={true}
              objectFit="cover"
            />
          </div>
          <div className={styles.banner_content}>
            <div className={styles.left}>
              <h1>Vendors</h1>
              <p>Find vendors near your</p>
            </div>
            <div className={styles.right}>
              <form id={styles.form_vendors}>
                <InputSearchVendors list={unique_categories} onchange={(event, value) => setSelectedCategory(value)} value={selectedCategory}>Category vendors</InputSearchVendors>
                <InputSearchVendors list={unique_cities} onchange={(event, value) => setSelectedCity(value)} value={selectedCity}>Category vendors</InputSearchVendors>
                <PrimaryButton>Search</PrimaryButton>
              </form>
            </div>
          </div>
        </div>
        <main className={styles.main}>
          <div className={styles.filter_left}>
            <CollapseSite name="Categories of vendors">
              {unique_categories?.slice(0, categoriesToShow).map((categorie, index) =>
              (
                <div className={styles.menuItem} key={index}>
                  <input type='button' key={index} value={categorie} onClick={(e) => setSelectedCategory(e.target.value)} />
                </div>
              ))}
              <div>
                <button className={styles.more} onClick={showMore}>{categoriesToShow > 6 ? "- show less" : "+ show more"}</button>
              </div>
            </CollapseSite>
            <CollapseSite name="Location">
              <InputFilter type="text" list={unique_cities} placeholder="Search your city, zipcode,.." onchange={(event, value) => setSelectedCity(value)} />
            </CollapseSite>

            <CollapseSite name="Rating">
              <Rating sx={{ color: '#AF4720' }} name="simple-controlled" onChange={(event, value) => { setSelectedRating(value); }} />
            </CollapseSite>
            {/* <CollapseSite name="Price">
              <div className={styles.priceSlider}>
                <SliderPrice/>

              </div>
            </CollapseSite> */}
          </div>
          <div className={styles.content}>
            <div className={styles.filter_top}>
              <span>{results.length} results</span>

            </div>
            <div className={styles.cards}>
              {content}
            </div>
          </div>
        </main>
        <ReactPaginate
          previousLabel={<div className={styles.next_button}>previous</div>}
          nextLabel={<div className={styles.next_button}>Next<FontAwesomeIcon icon="fa-solid fa-chevron-right" /> </div>}
          breakLabel="..."
          breakClassName={styles.breakPage}
          pageClassName={styles.button_page}
          activeClassName={styles.activePage}
          containerClassName={styles.pages}
          subContainerClassName={styles.button_page}
          initialPage={currentPage - 1}
          pageCount={totalPages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
        />

      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export async function getServerSideProps() {

  const [allvendors, allcategories] = await Promise.all([
    fetchAPI(`/vendor-nls?pagination[start]=0&pagination[limit]=100&populate=*`),
    fetchAPI(`/category-nls?populate=*`),
  ]);
  return {
    props:
    {
      allvendors: allvendors.data,
      allcategories: allcategories.data
    }
  }
}


export default Vendors;
