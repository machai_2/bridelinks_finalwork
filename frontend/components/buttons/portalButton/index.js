import styles from './style.module.scss';

export function PortalButton(props ) {
  return <button type="submit" className={styles.portal_btn} onClick={props.onClick}>{props.children}</button>;
}
